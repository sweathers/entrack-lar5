@extends('_layouts.view-layout') 

@section('title') Team Relationship @stop
@section('content-header') Team Relationship @stop

@section('header-nav')
   
	<li><a class="btn-menu index" href="/public/team-relationships">Back to Team Relationship List</a></li>
	<li><a class="btn-menu edit" href="/public/team-relationships/{{$teamRelationship->team_relationship_id}}/edit" 
       data-id="{{$teamRelationship->team_relationship_id}}"> Edit</a></li> 
 

<!--  <li><a href="#" class="btn-menu delete"
 	
						data-href="/public/team-relationships"
						data-successurl="/public/team-relationships"
 
						data-id="{{$teamRelationship->team_relationship_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$teamRelationship->team_relationship_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a></li> 
							-->
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop

@section('content')

@include('team-relationships/_partials/_view_elements')

	



		             

@stop @section('bottom_style')
@stop @section('bottom_script') @stop
