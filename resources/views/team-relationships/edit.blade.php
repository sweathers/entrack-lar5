@extends('_layouts.edit-layout') 

@section('title') Team Relationship @stop
@section('content-header')Edit Team Relationship @stop

@section('header-nav')
<li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li>
<!-- <li><a class="btn-menu index" href="/public/team-relationships">Back to Team Relationship List</a></li>  -->

@stop

@section('content')
		{!! Form::model($teamRelationship, 
			array('method'=> 'PUT', 
				'route' => array('team-relationships.update', 
				                        $teamRelationship->team_relationship_id ))) !!}
		{!! Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('team-relationships.show')) ?  route('team-relationships.show', array($teamRelationship->team_relationship_id)) : '' )) ) !!}
@include('team-relationships/_partials/_crud_form')


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}                  

@stop @section('bottom_style')
@stop @section('bottom_script')  @stop
