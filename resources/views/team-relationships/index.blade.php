@extends('_layouts.index-layout') 
@section('title') Team Relationship @stop
@section('content-header')  Team Relationship  @stop

@section('header-nav')   <li><a class="btn-menu create" href="/public/team-relationships/create">Create New Team Relationship</a></li>	
 @stop

@section('content')

          <ul class="link-list">
@foreach ($teamRelationships as $index => $row)
            <li>
 						  						 	<div class="NAME team_relationship_name">
											<a href="/public/team-relationships/{{$row->team_relationship_id}}">{{$row->team_relationship_name}}</a> 
					 
				</div>
			  												<!--  <div class="CODE relationship_code">{{$row->relationship_code}}</div>   -->
   												<!--  <div class="INT score">{{$row->score}}</div>   -->
   												<!--  <div class="SORT sort_order">{{$row->sort_order}}</div>   -->
                 
				    <a class="coverall" href="/public/team-relationships/{{$row->team_relationship_id}}"></a> 
   
            </li>
@endforeach
		  </ul>

@stop @section('bottom_style') 
@stop @section('bottom_script') @stop