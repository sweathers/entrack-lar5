<div class="form-group">
   	
	
    <div class="col-md-6">
    	{!! Form::label('team_relationship_name', 'Startup/Relationship Name') !!} 
    	{!! Form::text( 'team_relationship_name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_team_relationship_name" class="text-danger">{!! $errors->first('team_relationship_name', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('relationship_code', 'Type Code') !!} 
    	{!! Form::text( 'relationship_code', null, ['class' => 'form-control']) !!}
    	<p      id="msg_relationship_code" class="text-danger">{!! $errors->first('relationship_code', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('score', 'Score') !!} 
    	{!! Form::text( 'score', null, ['class' => 'form-control']) !!}
    	<p      id="msg_score" class="text-danger">{!! $errors->first('score', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('sort_order', 'Sort Order') !!} 
    	{!! Form::text( 'sort_order', null, ['class' => 'form-control']) !!}
    	<p      id="msg_sort_order" class="text-danger">{!! $errors->first('sort_order', ':message') !!}</p>
    </div>
 
</div>