@extends('admin._layouts.edit-layout') 

@section('title') Assumption @stop
@section('content-header') Edit Assumption @stop


@section('content')
		{{ Form::model($assumption, 
			array('method'=> 'PUT', 
				'route' => array('admin.assumptions.update', 
				                        $assumption->assumption_id ))) }}
		{{ Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('admin.assumptions.show')) ?  route('admin.assumptions.show', array($assumption->assumption_id)) : '' )) ) }}
@include('admin/assumptions/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}                  

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script')  @stop
