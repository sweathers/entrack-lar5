<div class="form-group">
   	
	
    <div class="col-md-6">
    	{!! Form::label('assumption_name', 'Assumption Name') !!} 
    	{!! Form::text( 'assumption_name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_assumption_name" class="text-danger">{!! $errors->first('assumption_name', ':message') !!}</p>
    </div>
      <div class="col-md-6">
    	{!! Form::label( 'startup_id', 'Startup') !!} 
    	{!! Form::select('startup_id', $startupList, null, ['class' => 'form-control']) !!}
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('assumption_description', 'Assumption Description') !!} 
    	{!! Form::textarea( 'assumption_description', null, ['class' => 'form-control']) !!}
    	<p      id="msg_assumption_description" class="text-danger">{!! $errors->first('assumption_description', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('test', 'Test') !!} 
    	{!! Form::textarea( 'test', null, ['class' => 'form-control']) !!}
    	<p      id="msg_test" class="text-danger">{!! $errors->first('test', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('result', 'Result') !!} 
    	{!! Form::textarea( 'result', null, ['class' => 'form-control']) !!}
    	<p      id="msg_result" class="text-danger">{!! $errors->first('result', ':message') !!}</p>
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'is_validated', 'Is Validated') !!} 
    	{!! Form::select('is_validated', array('0' => 'no', '1' => 'yes'), null, ['class' => 'form-control']) !!}
    	<p       id="msg_is_validated" class="text-danger">{!! $errors->first('is_validated', ':message') !!} </p>
    </div>			
  					
    <div class="col-md-6">
    	{!! Form::label( 'cause_pivot', 'Cause Pivot') !!} 
    	{!! Form::select('cause_pivot', array('0' => 'no', '1' => 'yes'), null, ['class' => 'form-control']) !!}
    	<p       id="msg_cause_pivot" class="text-danger">{!! $errors->first('cause_pivot', ':message') !!} </p>
    </div>			
  	
	
    <div class="col-md-6">
    	{!! Form::label('pivot_description', 'Pivot Description') !!} 
    	{!! Form::textarea( 'pivot_description', null, ['class' => 'form-control']) !!}
    	<p      id="msg_pivot_description" class="text-danger">{!! $errors->first('pivot_description', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('sort_order', 'Sort Order') !!} 
    	{!! Form::text( 'sort_order', null, ['class' => 'form-control']) !!}
    	<p      id="msg_sort_order" class="text-danger">{!! $errors->first('sort_order', ':message') !!}</p>
    </div>
 
</div>