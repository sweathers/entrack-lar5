@extends('admin._layouts.index-layout') 

@section('title') Assumption @stop

@section('content-header')  Assumption  @stop

@section('create-link')   <a href="/public/admin/assumptions/create" class="create" >Create New Assumption</a>	
 @stop

@section('content')
		<table class="table table-hover table-striped crud-list-table">
			<thead>
				<tr>
					<th class="crud-action-col"><div
							class="th-inner ">Action</div>
						<div class="fht-cell"></div></th>
      						<th><div
							class="th-inner sortable">Assumption Name</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Startup</div>
						<div class="fht-cell"></div></th>																	
                     						<th><div
							class="th-inner sortable">Sort Order</div>
						<div class="fht-cell"></div></th>																	
   						
				</tr>
			</thead>
			<tbody>
			@foreach ($assumptions as $index => $row)	
				<tr>
					<td class="crud-action-col">
						<a href="/public/admin/assumptions/{{$row->assumption_id}}/edit" class="edit" data-id="{{$row->assumption_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
						<a href="#" class="delete"
						data-href="/public/admin/assumptions"
						data-successurl="/public/admin/assumptions"
						data-index="{{ $index +1 }}" data-id="{{$row->assumption_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$row->assumption_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>
					</td>
					<td >{{$row->assumption_name}}</td>				
					<td >{{$row->startup_name}}</td>
					<td >{{$row->sort_order}}</td>					
				</tr>
			@endforeach
			</tbody>
		</table>

@stop @section('bottom_style') 
@stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop