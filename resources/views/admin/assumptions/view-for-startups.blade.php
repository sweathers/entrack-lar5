@extends('admin._layouts.view-layout') 

@section('title') Assumption @stop
@section('content-header') Assumption @stop


@section('content')

		<div class="form-group">
			<a class="btn btn-default" href="{{ URL::previous() }}" >Back</a>
			<a class="btn btn-default" href="/public/admin/assumptions/{{$assumption->assumption_id}}/edit" class="edit" data-id="{{$assumption->assumption_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
			<!--  <a href="#" class="btn btn-default delete"
						data-href="/public/admin/assumptions"
						data-successurl="/public/admin/assumptions"
						data-id="{{$assumption->assumption_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$assumption->assumption_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>  -->
		</div>

@include('admin/assumptions/_partials/_view_elements')

	



		             

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop
