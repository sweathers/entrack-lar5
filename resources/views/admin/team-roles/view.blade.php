@extends('admin._layouts.view-layout') 

@section('title') Team Role @stop
@section('content-header') Team Role @stop


@section('content')

		<div class="form-group">
			<a class="btn btn-default" href="{{ URL::previous() }}" >Back</a>
			<a class="btn btn-default" href="/public/admin/team-roles/{{$teamRole->team_role_id}}/edit" class="edit" data-id="{{$teamRole->team_role_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
			<!--  <a href="#" class="btn btn-default delete"
						data-href="/public/admin/team-roles"
						data-successurl="/public/admin/team-roles"
						data-id="{{$teamRole->team_role_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$teamRole->team_role_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>  -->
		</div>

@include('admin/team-roles/_partials/_view_elements')

	



		             

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop
