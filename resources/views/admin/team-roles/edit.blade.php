@extends('admin._layouts.edit-layout') 

@section('title') Team Role @stop
@section('content-header') Edit Team Role @stop


@section('content')
		{{ Form::model($teamRole, 
			array('method'=> 'PUT', 
				'route' => array('admin.team-roles.update', 
				                        $teamRole->team_role_id ))) }}
		{{ Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('admin.team-roles.show')) ?  route('admin.team-roles.show', array($teamRole->team_role_id)) : '' )) ) }}
@include('admin/team-roles/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}                  

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script')  @stop
