@extends('admin._layouts.index-layout') 

@section('title') Team Role @stop

@section('content-header')  Team Role  @stop

@section('create-link')   <a href="/public/admin/team-roles/create" class="create" >Create New Team Role</a>	
 @stop

@section('content')
		<table class="table table-hover table-striped crud-list-table">
			<thead>
				<tr>
					<th class="crud-action-col"><div
							class="th-inner ">Action</div>
						<div class="fht-cell"></div></th>
      						<th><div
							class="th-inner sortable">Team Role Name</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Type Code</div>
						<div class="fht-cell"></div></th>																	
             						<th><div
							class="th-inner sortable">Sort Order</div>
						<div class="fht-cell"></div></th>																	
   						
				</tr>
			</thead>
			<tbody>
			@foreach ($teamRoles as $index => $row)	
				<tr>
					<td class="crud-action-col">
						<a href="/public/admin/team-roles/{{$row->team_role_id}}/edit" class="edit" data-id="{{$row->team_role_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
						<a href="#" class="delete"
						data-href="/public/admin/team-roles"
						data-successurl="/public/admin/team-roles"
						data-index="{{ $index +1 }}" data-id="{{$row->team_role_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$row->team_role_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>
					</td>
					<td >{{$row->team_role_name}}</td>				
					<td >{{$row->team_role_code}}</td>				
					<td >{{$row->sort_order}}</td>					
				</tr>
			@endforeach
			</tbody>
		</table>

@stop @section('bottom_style') 
@stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop