@extends('admin._layouts.index-layout') 

@section('title') Team Member @stop

@section('content-header')  Team Member  @stop

@section('create-link')   <a href="/public/admin/team-members/create" class="create" >Create New Team Member</a>	
 @stop

@section('content')
		<table class="table table-hover table-striped crud-list-table">
			<thead>
				<tr>
					<th class="crud-action-col"><div
							class="th-inner ">Action</div>
						<div class="fht-cell"></div></th>
      						<th><div
							class="th-inner sortable">Team Member Name</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Startup</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Team Role</div>
						<div class="fht-cell"></div></th>																	
   						<th><div
							class="th-inner sortable">Team Relationship</div>
						<div class="fht-cell"></div></th>																	
                 						<th><div
							class="th-inner sortable">Hours Per Month</div>
						<div class="fht-cell"></div></th>																	
         						<th><div
							class="th-inner sortable">Sort Order</div>
						<div class="fht-cell"></div></th>																	
   						
				</tr>
			</thead>
			<tbody>
			@foreach ($teamMembers as $index => $row)	
				<tr>
					<td class="crud-action-col">
						<a href="/public/admin/team-members/{{$row->team_member_id}}/edit" class="edit" data-id="{{$row->team_member_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
						<a href="#" class="delete"
						data-href="/public/admin/team-members"
						data-successurl="/public/admin/team-members"
						data-index="{{ $index +1 }}" data-id="{{$row->team_member_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$row->team_member_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>
  					</td>
					<td >{{$row->team_member_name}}</td>				
					<td >{{$row->startup_name}}</td>
					<td >{{$row->team_role_name}}</td>
					<td >{{$row->team_relationship_name}}</td>
					<td >{{$row->hours_per_month}}</td>					
					<td >{{$row->sort_order}}</td>					
				</tr>
			@endforeach
			</tbody>
		</table>

@stop @section('bottom_style') 
@stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop