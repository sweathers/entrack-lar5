@extends('admin._layouts.view-layout') 

@section('title') Team Member @stop
@section('content-header') Team Member @stop


@section('content')

		<div class="form-group">
			<a class="btn btn-default" href="{{ URL::previous() }}" >Back</a>
			<a class="btn btn-default" href="/public/admin/team-members/{{$teamMember->team_member_id}}/edit" class="edit" data-id="{{$teamMember->team_member_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
			<!--  <a href="#" class="btn btn-default delete"
						data-href="/public/admin/team-members"
						data-successurl="/public/admin/team-members"
						data-id="{{$teamMember->team_member_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$teamMember->team_member_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>  -->
		</div>

@include('admin/team-members/_partials/_view_elements')

	

		<div class="form-group">	
			<div class="col-md-6">
				<dt>Project</dt>
				<dd>
					<ul> 				
@foreach ($teamMember->projectTeamMembers as $project )  
						<li>{{ $project->project_name }}</li>
@endforeach 					
					</ul>
				</dd>
        	</div>				 
  
		</div>


		             

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop
