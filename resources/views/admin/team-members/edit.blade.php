@extends('admin._layouts.edit-layout') 

@section('title') Team Member @stop
@section('content-header') Edit Team Member @stop


@section('content')
		{{ Form::model($teamMember, 
			array('method'=> 'PUT', 
				'route' => array('admin.team-members.update', 
				                        $teamMember->team_member_id ))) }}
		{{ Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('admin.team-members.show')) ?  route('admin.team-members.show', array($teamMember->team_member_id)) : '' )) ) }}
@include('admin/team-members/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])

		<div class="form-group">	
			<div class="col-md-4 crud-multiselect-container">
        		{{ Form::label('projects_selected', 'Project') }} 
        		{{ Form::hidden('projects_selected_count', '$projects_selected_count') }} 
        		{{ Form::select('projects_selected', $project_options, $projects_selected, ['class'=>'crud-multiselect-small', 'multiple'=>'multiple', 'name' => 'projects_selected[]']) }}
        	</div>				 
  
		</div>

		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}                  

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script') {{ HTML::script('js/crud-multiselect.js') }}  @stop
