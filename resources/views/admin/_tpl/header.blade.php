    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	{{ HTML::style('/img/favicon.png',['rel' => 'shortcut icon','type' => 'image/png']) }}

	<title>@yield('html-title','EnTrack - Tracking Startup Success')</title>
    {{-- HTML::style('/packages/bootstrap/css/bootstrap.min.css') --}}
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>

	{{ HTML::style('/css/site.css') }}
	{{ HTML::style('/css/wireframe.css') }}
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
	@yield('top_style')
	@yield('top_script')

