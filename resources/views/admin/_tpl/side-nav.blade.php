		<!--  Side Bar Nav -->
        <div id="sidebar">
          <ul class="nav-list"> 
@if(Auth::user()->role_id == 2) {{-- 'default' users  --}} 
      <li class=""><a href="/public/startups">Startup Profile</a></li>
   @if((isset($startup)) && ($startup->startup_id > 0))
      <li class="level2 "><a href="/public/startups/{{$startup->startup_id}}/team-members">Team Members</a></li>
      <li class="level2 "><a href="/public/startups/{{$startup->startup_id}}/assumptions">Assumptions</a></li>
      <li class="level2 "><a href="/public/startups/{{$startup->startup_id}}/projects">Projects</a></li>
      <li class="level2 "><a href="/public/startups/{{$startup->startup_id}}/startup-milestones">Completed Milestones</a></li>
   @endif
      
      <li class=""><a href="/public/milestones">Milestones</a></li>
      <li class=""><a href="/public/team-roles">Team Roles</a></li>
      <li class=""><a href="/public/project-statuses">Project Status Codes</a></li>
      
@endif  
      
         	</ul>
         <br/>
         <ul class="nav-list">
           <!-- <li><a href="/public/users/{{Auth::user()->user_id}}/edit"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>Change Password</a></li>  --> 
          </ul>
        </div>
