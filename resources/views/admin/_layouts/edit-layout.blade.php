<!DOCTYPE html>
<html lang="en">
  <head>
@include('_tpl.header')
@yield('header_addons')
  </head>
  <body class="edit-screen">
  @include('_tpl.header-top-nav-bar')
    <div id="page-container">
      <div id="content-container">
        <div id="content-header">
		   <h2>@yield('content-header')</h2>
        @if (isset($message))  
				<!-- Validation Message -->
				<div class="msg-danger">
				    {{ $message }}
				</div>
        @endif
		@if(count($errors) >= 1)
        		<div class="alert alert-error">
                	<div class="error">{{ $errors->first('error', ':message') }}</div>
            	</div>
        @endif
        @if (isset($success))
        		<div class="alert alert-success">
                    <div>{{ $success }}</div>
            	</div>        
        @endif
	
        	</div>
            <div class="content-section-container">
         		@yield('content')      
      		</div>
        </div>
        @include('_tpl.side-nav')
    </div>
    @include('_tpl.footer')
      
@yield('bottom_style')
    {{ HTML::script('/packages/jquery/js/jquery.min.js') }}
    {{-- HTML::script('/packages/bootstrap/js/bootstrap.min.js') --}}
    {{ HTML::script('/js/site.js') }}

<!-- MULTISELECT -->
{{ HTML::script('packages/bootstrap/plugin/multiselect/bootstrap-multiselect.js') }}
{{ HTML::style('packages/bootstrap/plugin/multiselect/bootstrap-multiselect.css') }}
@yield('bottom_script')

  </body>
</html>
