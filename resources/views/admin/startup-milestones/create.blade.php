@extends('admin._layouts.create-layout') 

@section('title') Startup Milestone @stop

@section('content-header')  Create Startup Milestone  @stop

@section('content')
		{{  Form::model($startupMilestone,
				array('method'=> 'POST', 'class' => 'form-horizontal form-bordered', 
						'route' => array('admin.startup-milestones.store'))) }}
		
@include('admin.startup-milestones/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])
		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}

@stop  @section('bottom_style') 
@stop  @section('bottom_plugin_script')
@stop @section('bottom_script') @stop