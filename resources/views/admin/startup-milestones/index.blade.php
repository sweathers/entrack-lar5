@extends('admin._layouts.index-layout') 

@section('title') Startup Milestone @stop

@section('content-header')  Startup Milestone  @stop

@section('create-link')   <a href="/public/admin/startup-milestones/create" class="create" >Create New Startup Milestone</a>	
 @stop

@section('content')
		<table class="table table-hover table-striped crud-list-table">
			<thead>
				<tr>
					<th class="crud-action-col"><div
							class="th-inner ">Action</div>
						<div class="fht-cell"></div></th>
      						<th><div
							class="th-inner sortable">Startup</div>
						<div class="fht-cell"></div></th>																	
   						<th><div
							class="th-inner sortable">Completed Milestone</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Date Completed</div>
						<div class="fht-cell"></div></th>																	
           						
				</tr>
			</thead>
			<tbody>
			@foreach ($startupMilestones as $index => $row)	
				<tr>
					<td class="crud-action-col">
						<a href="/public/admin/startup-milestones/{{$row->xref_s_m_id}}/edit" class="edit" data-id="{{$row->xref_s_m_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
						<a href="#" class="delete"
						data-href="/public/admin/startup-milestones"
						data-successurl="/public/admin/startup-milestones"
						data-index="{{ $index +1 }}" data-id="{{$row->xref_s_m_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$row->name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>
					</td>
					<td >{{$row->startup_name}}</td>
					<td >{{$row->milestone_name}}</td>
					<td >{{$row->date_completed}}</td>					
				</tr>
			@endforeach
			</tbody>
		</table>

@stop @section('bottom_style') 
@stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop