<div class="form-group">
   					
    <div class="col-md-6">
    	{!! Form::label( 'startup_id', 'Startup') !!} 
    	{!! Form::select('startup_id', array('0' => 'Select ...') + $startupList->toArray(), null, ['class' => 'form-control']) !!}
    	<p       id="msg_startup_id" class="text-danger">{!! $errors->first('startup_id', ':message') !!} </p>
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'milestone_id', 'Completed Milestone') !!} 
    	{!! Form::select('milestone_id', array('0' => 'Select ...') + $milestoneList->toArray(), null, ['class' => 'form-control']) !!}
    	<p       id="msg_milestone_id" class="text-danger">{!! $errors->first('milestone_id', ':message') !!} </p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('date_completed', 'Date Completed') !!} 
    	{!! Form::text( 'date_completed', null, ['class' => 'form-control']) !!}
    	<p      id="msg_date_completed" class="text-danger">{!! $errors->first('date_completed', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('completed_descr', 'Completed Descr') !!} 
    	{!! Form::textarea( 'completed_descr', null, ['class' => 'form-control']) !!}
    	<p      id="msg_completed_descr" class="text-danger">{!! $errors->first('completed_descr', ':message') !!}</p>
    </div>
   
</div>