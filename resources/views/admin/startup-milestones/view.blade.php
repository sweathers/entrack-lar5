@extends('admin._layouts.view-layout') 

@section('title') Startup Milestone @stop
@section('content-header') Startup Milestone @stop


@section('content')

		<div class="form-group">
			<a class="btn btn-default" href="{{ URL::previous() }}" >Back</a>
			<a class="btn btn-default" href="/public/admin/startup-milestones/{{$startupMilestone->xref_s_m_id}}/edit" class="edit" data-id="{{$startupMilestone->xref_s_m_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
			<!--  <a href="#" class="btn btn-default delete"
						data-href="/public/admin/startup-milestones"
						data-successurl="/public/admin/startup-milestones"
						data-id="{{$startupMilestone->xref_s_m_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$startupMilestone->name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>  -->
		</div>

@include('admin/startup-milestones/_partials/_view_elements')

	



		             

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop
