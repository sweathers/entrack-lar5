@extends('admin._layouts.edit-layout') 

@section('title') Startup Milestone @stop
@section('content-header') Edit Startup Milestone @stop


@section('content')
		{{ Form::model($startupMilestone, 
			array('method'=> 'PUT', 
				'route' => array('admin.startup-milestones.update', 
				                        $startupMilestone->xref_s_m_id ))) }}
		{{ Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('admin.startup-milestones.show')) ?  route('admin.startup-milestones.show', array($startupMilestone->xref_s_m_id)) : '' )) ) }}
@include('admin/startup-milestones/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}                  

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script')  @stop
