@extends('admin._layouts.view-layout') 

@section('title') Project @stop
@section('content-header') Project @stop


@section('content')

		<div class="form-group">
			<a class="btn btn-default" href="{{ URL::previous() }}" >Back</a>
			<a class="btn btn-default" href="/public/admin/projects/{{$project->project_id}}/edit" class="edit" data-id="{{$project->project_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
			<!--  <a href="#" class="btn btn-default delete"
						data-href="/public/admin/projects"
						data-successurl="/public/admin/projects"
						data-id="{{$project->project_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$project->project_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>  -->
		</div>

@include('admin/projects/_partials/_view_elements')

	

		<div class="form-group">	
			<div class="col-md-6">
				<dt>Team Member</dt>
				<dd>
					<ul> 				
@foreach ($project->projectTeamMembers as $teamMember )  
						<li>{{ $teamMember->team_member_name }}</li>
@endforeach 					
					</ul>
				</dd>
        	</div>				 
  
		</div>


		             

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop
