@extends('admin._layouts.create-layout') 

@section('title') Project @stop

@section('content-header') 	
Create Project for Startup "{{ $startup->startup_name or '' }}"
 @stop

@section('content')
			
		{{ Form::model($project,
				array('method'=> 'POST', 'class' => 'form-horizontal form-bordered', 
						'route' => array('admin.startups.projects.store', $startup->startup_id ))) }}
		
@include('admin.projects/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])
		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}

@stop  @section('bottom_style') 
@stop  @section('bottom_plugin_script')
@stop @section('bottom_script') @stop