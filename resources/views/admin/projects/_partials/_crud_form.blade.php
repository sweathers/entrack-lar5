<div class="form-group">
   	
	
    <div class="col-md-6">
    	{!! Form::label('project_name', 'Project Name') !!} 
    	{!! Form::text( 'project_name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_project_name" class="text-danger">{!! $errors->first('project_name', ':message') !!}</p>
    </div>
      <div class="col-md-6">
    	{!! Form::label( 'startup_id', 'Startup') !!} 
    	{!! Form::select('startup_id', $startupList, null, ['class' => 'form-control']) !!}
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'project_status_id', 'Project Status') !!} 
    	{!! Form::select('project_status_id', array('0' => 'Select ...') + $projectStatusList->toArray(), null, ['class' => 'form-control']) !!}
    	<p       id="msg_project_status_id" class="text-danger">{!! $errors->first('project_status_id', ':message') !!} </p>
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'assumption_id', 'Assumption to Validate') !!} 
    	{!! Form::select('assumption_id', array('0' => 'Select ...') + $assumptionList->toArray(), null, ['class' => 'form-control']) !!}
    	<p       id="msg_assumption_id" class="text-danger">{!! $errors->first('assumption_id', ':message') !!} </p>
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'milestone_id', 'Milestone Working Toward') !!} 
    	{!! Form::select('milestone_id', array('0' => 'Select ...') + $milestoneList->toArray(), null, ['class' => 'form-control']) !!}
    	<p       id="msg_milestone_id" class="text-danger">{!! $errors->first('milestone_id', ':message') !!} </p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('project_description', 'Project Description') !!} 
    	{!! Form::textarea( 'project_description', null, ['class' => 'form-control']) !!}
    	<p      id="msg_project_description" class="text-danger">{!! $errors->first('project_description', ':message') !!}</p>
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'responsible_team_member_id', 'Responsible Team Member') !!} 
    	{!! Form::select('responsible_team_member_id', array('0' => 'Select ...') + $teamMemberList->toArray(), null, ['class' => 'form-control']) !!}
    	<p       id="msg_responsible_team_member_id" class="text-danger">{!! $errors->first('responsible_team_member_id', ':message') !!} </p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('priority', 'Priority') !!} 
    	{!! Form::text( 'priority', null, ['class' => 'form-control']) !!}
    	<p      id="msg_priority" class="text-danger">{!! $errors->first('priority', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('budget_per_month', 'Budget Per Month') !!} 
    	{!! Form::text( 'budget_per_month', null, ['class' => 'form-control']) !!}
    	<p      id="msg_budget_per_month" class="text-danger">{!! $errors->first('budget_per_month', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('manhours_per_month', 'Manhours Per Month') !!} 
    	{!! Form::text( 'manhours_per_month', null, ['class' => 'form-control']) !!}
    	<p      id="msg_manhours_per_month" class="text-danger">{!! $errors->first('manhours_per_month', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('estimated_month', 'Estimated Month') !!} 
    	{!! Form::text( 'estimated_month', null, ['class' => 'form-control']) !!}
    	<p      id="msg_estimated_month" class="text-danger">{!! $errors->first('estimated_month', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('sort_order', 'Sort Order') !!} 
    	{!! Form::text( 'sort_order', null, ['class' => 'form-control']) !!}
    	<p      id="msg_sort_order" class="text-danger">{!! $errors->first('sort_order', ':message') !!}</p>
    </div>
 
</div>