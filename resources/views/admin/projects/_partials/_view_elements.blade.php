<dl>
   	
	

    	<dt>Project Name</dt><!--  $project->project_name -->   
    	<dd>{{ $project->project_name }}&nbsp;</dd>

  
    	<dt>Startup</dt><!--  $project->startup_id -->    	
		<dd>{{ $project->startup->startup_name }}</dd>
    	
  					
        <dt>Project Status</dt><!--  $project->project_status_id --> 	
		<dd>{{ $project->projectStatus->project_status_name  or "&nbsp;"  }}</dd>
		
		

  					
        <dt>Assumption to Validate</dt><!--  $project->assumption_id --> 	
		<dd>{{ $project->assumption->assumption_name  or "&nbsp;"  }}</dd>
		
		

  					
        <dt>Milestone Working Toward</dt><!--  $project->milestone_id --> 	
		<dd>{{ $project->milestone->milestone_name  or "&nbsp;"  }}</dd>
		
		

  	
	
    	<dt>Project Description</dt><!--  $project->project_description -->   
    	<dd><p>{{ $project->project_description }}&nbsp;</p></dd>

  					
        <dt>Responsible Team Member</dt><!--  $project->responsible_team_member_id --> 	
		<dd>{{ $project->responsibleTeamMember->team_member_name  or "&nbsp;"  }}</dd>
		
		

  	
	

    	<dt>Priority</dt><!--  $project->priority -->   
    	<dd>{{ $project->priority }}&nbsp;</dd>

  	
	

    	<dt>Budget Per Month</dt><!--  $project->budget_per_month -->   
    	<dd>{{ $project->budget_per_month }}&nbsp;</dd>

  	
	

    	<dt>Manhours Per Month</dt><!--  $project->manhours_per_month -->   
    	<dd>{{ $project->manhours_per_month }}&nbsp;</dd>

  	
	

    	<dt>Estimated Month</dt><!--  $project->estimated_month -->   
    	<dd>{{ $project->estimated_month }}&nbsp;</dd>

  	
	

    	<dt>Sort Order</dt><!--  $project->sort_order -->   
    	<dd>{{ $project->sort_order }}&nbsp;</dd>

 	</dl>
