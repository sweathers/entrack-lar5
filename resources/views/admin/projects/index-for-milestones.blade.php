@extends('admin._layouts.index-layout') 

@section('title') Project @stop

@section('content-header') 	
	Project for Milestone "{{ $milestone->milestone_name or '' }}"
 @stop

@section('create-link')  <a href="/public/admin/milestones/{{$milestone->milestone_id or 0}}/projects/create" class="create" >Create New Project</a>
 @stop

@section('content')
		<table class="table table-hover table-striped crud-list-table">
			<thead>
				<tr>
					<th class="crud-action-col"><div
							class="th-inner ">Action</div>
						<div class="fht-cell"></div></th>
      						<th><div
							class="th-inner sortable">Project Name</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Startup</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Project Status</div>
						<div class="fht-cell"></div></th>																	
                     						<th><div
							class="th-inner sortable">Budget Per Month</div>
						<div class="fht-cell"></div></th>																	
   						<th><div
							class="th-inner sortable">Manhours Per Month</div>
						<div class="fht-cell"></div></th>																	
   						<th><div
							class="th-inner sortable">Estimated Month</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Sort Order</div>
						<div class="fht-cell"></div></th>																	
   						
				</tr>
			</thead>
			<tbody>
			@foreach ($projects as $index => $row)	
				<tr>
					<td class="crud-action-col">
					
						<a href="/public/admin/projects/{{$row->project_id}}/edit?successUrl={{ urlencode( Request::path() ) }}" class="edit" data-id="{{$row->project_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
						<a href="#" class="delete"
						data-href="/public/admin/projects"
						data-successurl="/public/admin/projects"
						data-index="{{ $index +1 }}" data-id="{{$row->project_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$row->project_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>
  					</td>
					<td >{{$row->project_name}}</td>				
					<td >{{$row->startup_name}}</td>
					<td >{{$row->project_status_name}}</td>
					<td >{{$row->budget_per_month}}</td>					
					<td >{{$row->manhours_per_month}}</td>					
					<td >{{$row->estimated_month}}</td>					
					<td >{{$row->sort_order}}</td>					
				</tr>
			@endforeach
			</tbody>
		</table>

@stop @section('bottom_style') 
@stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop