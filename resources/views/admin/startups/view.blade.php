@extends('admin._layouts.view-layout') 

@section('title') Startup @stop
@section('content-header') Startup @stop


@section('content')

		<div class="form-group">
			<a class="btn btn-default" href="{{ URL::previous() }}" >Back</a>
			<a class="btn btn-default" href="/public/admin/startups/{{$startup->startup_id}}/edit" class="edit" data-id="{{$startup->startup_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
			<!--  <a href="#" class="btn btn-default delete"
						data-href="/public/admin/startups"
						data-successurl="/public/admin/startups"
						data-id="{{$startup->startup_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$startup->startup_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>  -->
			<a class="btn btn-default" href="/public/admin/startups/{{$startup->startup_id}}/team-members" ><i class="glyphicon glyphicon-list"></i> Team Member</a>	
			<a class="btn btn-default" href="/public/admin/startups/{{$startup->startup_id}}/assumptions" ><i class="glyphicon glyphicon-list"></i> Assumption</a>	
			<a class="btn btn-default" href="/public/admin/startups/{{$startup->startup_id}}/projects" ><i class="glyphicon glyphicon-list"></i> Project</a>	
		</div>

@include('admin/startups/_partials/_view_elements')

	

		<div class="form-group">	
			<div class="col-md-6">
				<dt>Completed Milestone</dt>
				<dd>
					<ul> 				
@foreach ($startup->startupMilestones as $milestone )  
						<li>{{ $milestone->milestone_name }}</li>
@endforeach 					
					</ul>
				</dd>
        	</div>				 
  
		</div>

		<div class="form-group">
 
			<div class="col-md-6">
				<dt>Team Members</dt>
				<dd>
					<ul>				
@foreach ($startup->startupTeamMembers as $teamMember )  
						<li><a href="/public/admin/startups/{{$startup->startup_id}}/team-members/{{$teamMember->team_member_id}}">{{ $teamMember->team_member_name }}</a></li>
@endforeach 					
					</ul>
				</dd>
        	</div>	 
  
			<div class="col-md-6">
				<dt>Assumptions</dt>
				<dd>
					<ul>				
@foreach ($startup->startupAssumptions as $assumption )  
						<li><a href="/public/admin/startups/{{$startup->startup_id}}/assumptions/{{$assumption->assumption_id}}">{{ $assumption->assumption_name }}</a></li>
@endforeach 					
					</ul>
				</dd>
        	</div>	 
  
			<div class="col-md-6">
				<dt>Projects</dt>
				<dd>
					<ul>				
@foreach ($startup->startupProjects as $project )  
						<li><a href="/public/admin/startups/{{$startup->startup_id}}/projects/{{$project->project_id}}">{{ $project->project_name }}</a></li>
@endforeach 					
					</ul>
				</dd>
        	</div>	 
 		</div>

		             

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop
