@extends('admin._layouts.index-layout') 

@section('title') Startup @stop

@section('content-header')  Startup  @stop

@section('create-link')   <a href="/public/admin/startups/create" class="create" >Create New Startup</a>	
 @stop

@section('content')
		<table class="table table-hover table-striped crud-list-table">
			<thead>
				<tr>
					<th class="crud-action-col"><div
							class="th-inner ">Action</div>
						<div class="fht-cell"></div></th>
      						<th><div
							class="th-inner sortable">Startup Name</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Location Place</div>
						<div class="fht-cell"></div></th>																	
                         						<th><div
							class="th-inner sortable">Sort Order</div>
						<div class="fht-cell"></div></th>																	
   						
				</tr>
			</thead>
			<tbody>
			@foreach ($startups as $index => $row)	
				<tr>
					<td class="crud-action-col">
						<a href="/public/admin/startups/{{$row->startup_id}}/edit" class="edit" data-id="{{$row->startup_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
						<a href="#" class="delete"
						data-href="/public/admin/startups"
						data-successurl="/public/admin/startups"
						data-index="{{ $index +1 }}" data-id="{{$row->startup_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$row->startup_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>
						<a href="/public/admin/startups/{{$row->startup_id}}/team-members" ><i class="glyphicon glyphicon-list"></i> Team Member</a>	
						<a href="/public/admin/startups/{{$row->startup_id}}/assumptions" ><i class="glyphicon glyphicon-list"></i> Assumption</a>	
						<a href="/public/admin/startups/{{$row->startup_id}}/projects" ><i class="glyphicon glyphicon-list"></i> Project</a>	
						<a href="/public/admin/startups/{{$row->startup_id}}/startup-milestones" ><i class="glyphicon glyphicon-list"></i> Startup Milestone</a>	
  					</td>
					<td >{{$row->startup_name}}</td>				
					<td >{{$row->geo_place_name}}</td>
					<td >{{$row->sort_order}}</td>					
				</tr>
			@endforeach
			</tbody>
		</table>

@stop @section('bottom_style') 
@stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop