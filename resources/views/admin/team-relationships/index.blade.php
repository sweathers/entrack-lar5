@extends('admin._layouts.index-layout') 

@section('title') Team Relationship @stop

@section('content-header')  Team Relationship  @stop

@section('create-link')   <a href="/public/admin/team-relationships/create" class="create" >Create New Team Relationship</a>	
 @stop

@section('content')
		<table class="table table-hover table-striped crud-list-table">
			<thead>
				<tr>
					<th class="crud-action-col"><div
							class="th-inner ">Action</div>
						<div class="fht-cell"></div></th>
      						<th><div
							class="th-inner sortable">Startup/Relationship Name</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Type Code</div>
						<div class="fht-cell"></div></th>																	
         						<th><div
							class="th-inner sortable">Sort Order</div>
						<div class="fht-cell"></div></th>																	
   						
				</tr>
			</thead>
			<tbody>
			@foreach ($teamRelationships as $index => $row)	
				<tr>
					<td class="crud-action-col">
						<a href="/public/admin/team-relationships/{{$row->team_relationship_id}}/edit" class="edit" data-id="{{$row->team_relationship_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
						<a href="#" class="delete"
						data-href="/public/admin/team-relationships"
						data-successurl="/public/admin/team-relationships"
						data-index="{{ $index +1 }}" data-id="{{$row->team_relationship_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$row->team_relationship_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>
					</td>
					<td >{{$row->team_relationship_name}}</td>				
					<td >{{$row->relationship_code}}</td>				
					<td >{{$row->sort_order}}</td>					
				</tr>
			@endforeach
			</tbody>
		</table>

@stop @section('bottom_style') 
@stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop