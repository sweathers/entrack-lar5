@extends('admin._layouts.edit-layout') 

@section('title') Team Relationship @stop
@section('content-header') Edit Team Relationship @stop


@section('content')
		{{ Form::model($teamRelationship, 
			array('method'=> 'PUT', 
				'route' => array('admin.team-relationships.update', 
				                        $teamRelationship->team_relationship_id ))) }}
		{{ Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('admin.team-relationships.show')) ?  route('admin.team-relationships.show', array($teamRelationship->team_relationship_id)) : '' )) ) }}
@include('admin/team-relationships/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}                  

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script')  @stop
