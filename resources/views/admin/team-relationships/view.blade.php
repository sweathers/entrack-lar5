@extends('admin._layouts.view-layout') 

@section('title') Team Relationship @stop
@section('content-header') Team Relationship @stop


@section('content')

		<div class="form-group">
			<a class="btn btn-default" href="{{ URL::previous() }}" >Back</a>
			<a class="btn btn-default" href="/public/admin/team-relationships/{{$teamRelationship->team_relationship_id}}/edit" class="edit" data-id="{{$teamRelationship->team_relationship_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
			<!--  <a href="#" class="btn btn-default delete"
						data-href="/public/admin/team-relationships"
						data-successurl="/public/admin/team-relationships"
						data-id="{{$teamRelationship->team_relationship_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$teamRelationship->team_relationship_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>  -->
		</div>

@include('admin/team-relationships/_partials/_view_elements')

	



		             

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop
