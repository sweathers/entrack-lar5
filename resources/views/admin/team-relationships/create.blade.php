@extends('admin._layouts.create-layout') 

@section('title') Team Relationship @stop

@section('content-header')  Create Team Relationship  @stop

@section('content')
		{{  Form::model($teamRelationship,
				array('method'=> 'POST', 'class' => 'form-horizontal form-bordered', 
						'route' => array('admin.team-relationships.store'))) }}
		
@include('admin.team-relationships/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])
		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}

@stop  @section('bottom_style') 
@stop  @section('bottom_plugin_script')
@stop @section('bottom_script') @stop