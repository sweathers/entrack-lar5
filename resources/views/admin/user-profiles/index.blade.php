@extends('admin._layouts.index-layout') 

@section('title') User Profile @stop

@section('content-header')  User Profile  @stop

@section('create-link')   <a href="/public/admin/user-profiles/create" class="create" >Create New User Profile</a>	
 @stop

@section('content')
		<table class="table table-hover table-striped crud-list-table">
			<thead>
				<tr>
					<th class="crud-action-col"><div
							class="th-inner ">Action</div>
						<div class="fht-cell"></div></th>
      						<th><div
							class="th-inner sortable">Username</div>
						<div class="fht-cell"></div></th>																	
                 						
				</tr>
			</thead>
			<tbody>
			@foreach ($userProfiles as $index => $row)	
				<tr>
					<td class="crud-action-col">
						<a href="/public/admin/user-profiles/{{$row->id}}/edit" class="edit" data-id="{{$row->id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
						<a href="#" class="delete"
						data-href="/public/admin/user-profiles"
						data-successurl="/public/admin/user-profiles"
						data-index="{{ $index +1 }}" data-id="{{$row->id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$row->name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>
					</td>
					<td >{{$row->name}}</td>				
				</tr>
			@endforeach
			</tbody>
		</table>

@stop @section('bottom_style') 
@stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop