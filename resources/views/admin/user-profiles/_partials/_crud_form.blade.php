<div class="form-group">
   	
	
    <div class="col-md-6">
    	{!! Form::label('name', 'Username') !!} 
    	{!! Form::text( 'name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_name" class="text-danger">{!! $errors->first('name', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('email', 'email') !!} 
    	{!! Form::text( 'email', null, ['class' => 'form-control']) !!}
    	<p      id="msg_email" class="text-danger">{!! $errors->first('email', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('first_name', 'First Name') !!} 
    	{!! Form::text( 'first_name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_first_name" class="text-danger">{!! $errors->first('first_name', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('last_name', 'Last Name') !!} 
    	{!! Form::text( 'last_name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_last_name" class="text-danger">{!! $errors->first('last_name', ':message') !!}</p>
    </div>
   
</div>