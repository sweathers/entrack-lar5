@extends('admin._layouts.edit-layout') 

@section('title') User Profile @stop
@section('content-header') Edit User Profile @stop


@section('content')
		{{ Form::model($userProfile, 
			array('method'=> 'PUT', 
				'route' => array('admin.user-profiles.update', 
				                        $userProfile->id ))) }}
		{{ Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('admin.user-profiles.show')) ?  route('admin.user-profiles.show', array($userProfile->id)) : '' )) ) }}
@include('admin/user-profiles/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}                  

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script')  @stop
