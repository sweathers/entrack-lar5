@extends('admin._layouts.index-layout') 

@section('title') Project Status @stop

@section('content-header')  Project Status  @stop

@section('create-link')   <a href="/public/admin/project-statuses/create" class="create" >Create New Project Status</a>	
 @stop

@section('content')
		<table class="table table-hover table-striped crud-list-table">
			<thead>
				<tr>
					<th class="crud-action-col"><div
							class="th-inner ">Action</div>
						<div class="fht-cell"></div></th>
      						<th><div
							class="th-inner sortable">Project Status Name</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Type Code</div>
						<div class="fht-cell"></div></th>																	
     						<th><div
							class="th-inner sortable">Sort Order</div>
						<div class="fht-cell"></div></th>																	
   						
				</tr>
			</thead>
			<tbody>
			@foreach ($projectStatuses as $index => $row)	
				<tr>
					<td class="crud-action-col">
						<a href="/public/admin/project-statuses/{{$row->project_status_id}}/edit" class="edit" data-id="{{$row->project_status_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
						<a href="#" class="delete"
						data-href="/public/admin/project-statuses"
						data-successurl="/public/admin/project-statuses"
						data-index="{{ $index +1 }}" data-id="{{$row->project_status_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$row->project_status_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>
					</td>
					<td >{{$row->project_status_name}}</td>				
					<td >{{$row->project_status_code}}</td>				
					<td >{{$row->sort_order}}</td>					
				</tr>
			@endforeach
			</tbody>
		</table>

@stop @section('bottom_style') 
@stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop