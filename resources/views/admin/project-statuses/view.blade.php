@extends('admin._layouts.view-layout') 

@section('title') Project Status @stop
@section('content-header') Project Status @stop


@section('content')

		<div class="form-group">
			<a class="btn btn-default" href="{{ URL::previous() }}" >Back</a>
			<a class="btn btn-default" href="/public/admin/project-statuses/{{$projectStatus->project_status_id}}/edit" class="edit" data-id="{{$projectStatus->project_status_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
			<!--  <a href="#" class="btn btn-default delete"
						data-href="/public/admin/project-statuses"
						data-successurl="/public/admin/project-statuses"
						data-id="{{$projectStatus->project_status_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$projectStatus->project_status_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>  -->
		</div>

@include('admin/project-statuses/_partials/_view_elements')

	



		             

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop
