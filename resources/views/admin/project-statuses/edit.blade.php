@extends('admin._layouts.edit-layout') 

@section('title') Project Status @stop
@section('content-header') Edit Project Status @stop


@section('content')
		{{ Form::model($projectStatus, 
			array('method'=> 'PUT', 
				'route' => array('admin.project-statuses.update', 
				                        $projectStatus->project_status_id ))) }}
		{{ Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('admin.project-statuses.show')) ?  route('admin.project-statuses.show', array($projectStatus->project_status_id)) : '' )) ) }}
@include('admin/project-statuses/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}                  

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script')  @stop
