@extends('admin._layouts.create-layout') 

@section('title') Project Status @stop

@section('content-header')  Create Project Status  @stop

@section('content')
		{{  Form::model($projectStatus,
				array('method'=> 'POST', 'class' => 'form-horizontal form-bordered', 
						'route' => array('admin.project-statuses.store'))) }}
		
@include('admin.project-statuses/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])
		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{{ Form::submit('Save') }}
		</div>
		{{ Form::close() }}

@stop  @section('bottom_style') 
@stop  @section('bottom_plugin_script')
@stop @section('bottom_script') @stop