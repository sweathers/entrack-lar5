@extends('admin._layouts.view-layout') 

@section('title') Milestone @stop
@section('content-header') Milestone @stop


@section('content')

		<div class="form-group">
			<a class="btn btn-default" href="{{ URL::previous() }}" >Back</a>
			<a class="btn btn-default" href="/public/admin/milestones/{{$milestone->milestone_id}}/edit" class="edit" data-id="{{$milestone->milestone_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
			<!--  <a href="#" class="btn btn-default delete"
						data-href="/public/admin/milestones"
						data-successurl="/public/admin/milestones"
						data-id="{{$milestone->milestone_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$milestone->milestone_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>  -->
		</div>

@include('admin/milestones/_partials/_view_elements')

	

		<div class="form-group">	
			<div class="col-md-6">
				<dt>Startup</dt>
				<dd>
					<ul> 				
@foreach ($milestone->startupMilestones as $startup )  
						<li>{{ $startup->startup_name }}</li>
@endforeach 					
					</ul>
				</dd>
        	</div>				 
  
		</div>


		             

@stop @section('bottom_style') @stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop
