@extends('admin._layouts.index-layout') 

@section('title') Milestone @stop

@section('content-header')  Milestone  @stop

@section('create-link')   <a href="/public/admin/milestones/create" class="create" >Create New Milestone</a>	
 @stop

@section('content')
		<table class="table table-hover table-striped crud-list-table">
			<thead>
				<tr>
					<th class="crud-action-col"><div
							class="th-inner ">Action</div>
						<div class="fht-cell"></div></th>
      						<th><div
							class="th-inner sortable">Milestone Name</div>
						<div class="fht-cell"></div></th>																	
                   						<th><div
							class="th-inner sortable">Sort Order</div>
						<div class="fht-cell"></div></th>																	
   						
				</tr>
			</thead>
			<tbody>
			@foreach ($milestones as $index => $row)	
				<tr>
					<td class="crud-action-col">
						<a href="/public/admin/milestones/{{$row->milestone_id}}/edit" class="edit" data-id="{{$row->milestone_id}}"><i class="glyphicon glyphicon-edit"></i> Edit</a> 
						<a href="#" class="delete"
						data-href="/public/admin/milestones"
						data-successurl="/public/admin/milestones"
						data-index="{{ $index +1 }}" data-id="{{$row->milestone_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$row->milestone_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a>
						<a href="/public/admin/milestones/{{$row->milestone_id}}/startup-milestones" ><i class="glyphicon glyphicon-list"></i> Startup Milestone</a>	
  					</td>
					<td >{{$row->milestone_name}}</td>				
					<td >{{$row->sort_order}}</td>					
				</tr>
			@endforeach
			</tbody>
		</table>

@stop @section('bottom_style') 
@stop @section('bottom_plugin_script')
@stop @section('bottom_script') @stop