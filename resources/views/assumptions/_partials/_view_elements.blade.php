<dl>
   	
	

    	<dt>Assumption Name</dt><!--  $assumption->assumption_name -->   
    	<dd>{{ $assumption->assumption_name }}&nbsp;</dd>

  
    	<dt>Startup</dt><!--  $assumption->startup_id -->    	
		<dd>{{ $assumption->startup->startup_name }}</dd>
    	
  	
	
    	<dt>Assumption Description</dt><!--  $assumption->assumption_description -->   
    	<dd><p>{{ $assumption->assumption_description }}&nbsp;</p></dd>

  	
	
    	<dt>Test</dt><!--  $assumption->test -->   
    	<dd><p>{{ $assumption->test }}&nbsp;</p></dd>

  	
	
    	<dt>Result</dt><!--  $assumption->result -->   
    	<dd><p>{{ $assumption->result }}&nbsp;</p></dd>

  					

    	<dt>Is Validated</dt><!--  $assumption->is_validated -->    	
    	<dd>{{ ($assumption->is_validated > 0) ? 'yes' : 'no' }}</dd>
			
  					

    	<dt>Cause Pivot</dt><!--  $assumption->cause_pivot -->    	
    	<dd>{{ ($assumption->cause_pivot > 0) ? 'yes' : 'no' }}</dd>
			
  	
	
    	<dt>Pivot Description</dt><!--  $assumption->pivot_description -->   
    	<dd><p>{{ $assumption->pivot_description }}&nbsp;</p></dd>

  	
	

    	<dt>Sort Order</dt><!--  $assumption->sort_order -->   
    	<dd>{{ $assumption->sort_order }}&nbsp;</dd>

 	</dl>
