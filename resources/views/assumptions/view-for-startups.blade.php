@extends('_layouts.view-layout') 

@section('title') Assumption @stop
@section('content-header') Assumption @stop

@section('header-nav')
  <li><a class="btn-menu index" href="/public/startups/{{$startup->startup_id or 0}}/assumptions">Back to Startup-Assumption List</a><li>
<li><a class="btn-menu edit" href="/public/assumptions/{{$assumption->assumption_id}}/edit?successUrl={{((Route::getRoutes()->hasNamedRoute('startups.assumptions.show')) ? route('startups.assumptions.show', array($startup->startup_id, $assumption->assumption_id)) : '')}}" 
       data-id="{{$assumption->assumption_id}}"> Edit</a></li> 
 

<!--  <li><a href="#" class="btn-menu delete"
						data-href="/public/startups/{{$startup->startup_id or 0}}/assumptions"
						data-successurl="/public/startups/{{$startup->startup_id or 0}}/assumptions"
 
						data-id="{{$assumption->assumption_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$assumption->assumption_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a></li> 
							-->
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop

@section('content')

@include('assumptions/_partials/_view_elements')

	



		             

@stop @section('bottom_style')
@stop @section('bottom_script') @stop
