@extends('_layouts.edit-layout') 

@section('title') Assumption @stop
@section('content-header')Edit Assumption @stop

@section('header-nav')
<li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li>
<!-- <li><a class="btn-menu index" href="/public/assumptions">Back to Assumption List</a></li>  -->

@stop

@section('content')
		{!! Form::model($assumption, 
			array('method'=> 'PUT', 
				'route' => array('assumptions.update', 
				                        $assumption->assumption_id ))) !!}
		{!! Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('assumptions.show')) ?  route('assumptions.show', array($assumption->assumption_id)) : '' )) ) !!}
@include('assumptions/_partials/_crud_form')


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}                  

@stop @section('bottom_style')
@stop @section('bottom_script')  @stop
