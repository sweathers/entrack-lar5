@extends('_layouts.view-layout') 

@section('title') Project @stop
@section('content-header') Project @stop

@section('header-nav')
  <li><a class="btn-menu index" href="/public/startups/{{$startup->startup_id or 0}}/projects">Back to Startup-Project List</a><li>
<li><a class="btn-menu edit" href="/public/projects/{{$project->project_id}}/edit?successUrl={{((Route::getRoutes()->hasNamedRoute('startups.projects.show')) ? route('startups.projects.show', array($startup->startup_id, $project->project_id)) : '')}}" 
       data-id="{{$project->project_id}}"> Edit</a></li> 
 

<!--  <li><a href="#" class="btn-menu delete"
						data-href="/public/startups/{{$startup->startup_id or 0}}/projects"
						data-successurl="/public/startups/{{$startup->startup_id or 0}}/projects"
 
						data-id="{{$project->project_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$project->project_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a></li> 
							-->
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop

@section('content')

@include('projects/_partials/_view_elements')

	

		<div class="form-group">	
			<dl>
				<dt>Team Members</dt>
				<dd>
					<ul> 				
@foreach ($project->projectTeamMembers as $teamMember )  
						<li>{{ $teamMember->team_member_name }}</li>
@endforeach 					
					</ul>
				</dd>
        	</dl>				 
  
		</div>


		             

@stop @section('bottom_style')
@stop @section('bottom_script') @stop
