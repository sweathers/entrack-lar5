@extends('_layouts.edit-layout') 

@section('title') Project @stop
@section('content-header')Edit Project @stop

@section('header-nav')
<li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li>
<!-- <li><a class="btn-menu index" href="/public/projects">Back to Project List</a></li>  -->

@stop

@section('content')
		{!! Form::model($project, 
			array('method'=> 'PUT', 
				'route' => array('projects.update', 
				                        $project->project_id ))) !!}
		{!! Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('projects.show')) ?  route('projects.show', array($project->project_id)) : '' )) ) !!}
@include('projects/_partials/_crud_form')

		<div class="form-group">	
			<div class="col-md-4 crud-multiselect-container">
        		{!! Form::label('team_members_selected', 'Team Member') !!} 
        		{!! Form::hidden('team_members_selected_count', '$team_members_selected_count') !!} 
        		{!! Form::select('team_members_selected', $team_member_options, $team_members_selected, ['class'=>'crud-multiselect-small', 'multiple'=>'multiple', 'name' => 'team_members_selected[]']) !!}
        	</div>				 
  
		</div>

		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}                  

@stop @section('bottom_style')
@stop @section('bottom_script') <script src="/public/packages/bootstrap/plugin/multiselect/bootstrap-multiselect.js"></script>
  @stop
