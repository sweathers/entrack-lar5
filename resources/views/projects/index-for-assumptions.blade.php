@extends('_layouts.index-layout') 
@section('title') Project @stop
@section('content-header') 	
	Project for Assumption "{{ $assumption->assumption_name or '' }}"
 @stop

@section('header-nav')  <li><a class="btn-menu create" href="/public/assumptions/{{$assumption->assumption_id or 0}}/projects/create">Create New Project</a></li>
 @stop

@section('content')

          <ul class="link-list">
@foreach ($projects as $index => $row)
            <li>
 						  						 	<div class="NAME project_name">
										
						<a href="/public/assumptions/{{$assumption->assumption_id or 0}}/projects/{{$row->project_id}}">{{$row->project_name}}</a> 
					 
				</div>
			  												<!-- <div class="PARENT startup_id">{{$row->startup_name}}</div>   -->
				   												<!-- <div class="TYPETABLE project_status_id">{{$row->project_status_name}}</div>   -->
				   												<!-- <div class="OTHERTABLEID assumption_id">{{$row->assumption_name}}</div>   -->
				   												<!-- <div class="OTHERTABLEID milestone_id">{{$row->milestone_name}}</div>   -->
				   						  												<!-- <div class="OTHERTABLEID responsible_team_member_id">{{$row->team_member_name}}</div>   -->
				   												<!--  <div class="INT priority">{{$row->priority}}</div>   -->
   												<!--  <div class="DECIMAL budget_per_month">{{$row->budget_per_month}}</div>   -->
   												<!--  <div class="DECIMAL manhours_per_month">{{$row->manhours_per_month}}</div>   -->
   												<!--  <div class="DECIMAL estimated_month">{{$row->estimated_month}}</div>   -->
   												<!--  <div class="SORT sort_order">{{$row->sort_order}}</div>   -->
                 
					
					<a class="coverall" href="/public/assumptions/{{$assumption->assumption_id or 0}}/projects/{{$row->project_id}}">  </a> 
   
            </li>
@endforeach
		  </ul>

@stop @section('bottom_style') 
@stop @section('bottom_script') @stop