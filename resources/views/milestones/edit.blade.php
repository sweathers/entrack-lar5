@extends('_layouts.edit-layout') 

@section('title') Milestone @stop
@section('content-header')Edit Milestone @stop

@section('header-nav')
<li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li>
<!-- <li><a class="btn-menu index" href="/public/milestones">Back to Milestone List</a></li>  -->

@stop

@section('content')
		{!! Form::model($milestone, 
			array('method'=> 'PUT', 
				'route' => array('milestones.update', 
				                        $milestone->milestone_id ))) !!}
		{!! Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('milestones.show')) ?  route('milestones.show', array($milestone->milestone_id)) : '' )) ) !!}
@include('milestones/_partials/_crud_form')

		<div class="form-group">	
			<div class="col-md-4 crud-multiselect-container">
        		{!! Form::label('startups_selected', 'Startup') !!} 
        		{!! Form::hidden('startups_selected_count', '$startups_selected_count') !!} 
        		{!! Form::select('startups_selected', $startup_options, $startups_selected, ['class'=>'crud-multiselect-small', 'multiple'=>'multiple', 'name' => 'startups_selected[]']) !!}
        	</div>				 
  
		</div>

		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}                  

@stop @section('bottom_style')
@stop @section('bottom_script') <script src="/public/packages/bootstrap/plugin/multiselect/bootstrap-multiselect.js"></script>
  @stop
