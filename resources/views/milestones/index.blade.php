@extends('_layouts.index-layout') 
@section('title') Milestone @stop
@section('content-header')  Milestone  @stop

@section('header-nav')   <li><a class="btn-menu create" href="/public/milestones/create">Create New Milestone</a></li>	
 @stop

@section('content')

          <ul class="link-list">
@foreach ($milestones as $index => $row)
            <li>
 						  						 	<div class="NAME milestone_name">
											<a href="/public/milestones/{{$row->milestone_id}}">{{$row->milestone_name}}</a> 
					 
				</div>
			  						  						  						  						  												<!--  <div class="INT score">{{$row->score}}</div>   -->
   												<!--  <div class="SORT sort_order">{{$row->sort_order}}</div>   -->
                 
				    <a class="coverall" href="/public/milestones/{{$row->milestone_id}}"></a> 
   
            </li>
@endforeach
		  </ul>

@stop @section('bottom_style') 
@stop @section('bottom_script') @stop