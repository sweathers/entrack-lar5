@extends('_layouts.view-layout') 

@section('title') Milestone @stop
@section('content-header') Milestone @stop

@section('header-nav')
   
	<li><a class="btn-menu index" href="/public/milestones">Back to Milestone List</a></li>
	<li><a class="btn-menu edit" href="/public/milestones/{{$milestone->milestone_id}}/edit" 
       data-id="{{$milestone->milestone_id}}"> Edit</a></li> 
 

<!--  <li><a href="#" class="btn-menu delete"
 	
						data-href="/public/milestones"
						data-successurl="/public/milestones"
 
						data-id="{{$milestone->milestone_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$milestone->milestone_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a></li> 
							-->
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop

@section('content')

@include('milestones/_partials/_view_elements')

	

		<div class="form-group">	
			<dl>
				<dt>Startups</dt>
				<dd>
					<ul> 				
@foreach ($milestone->startupMilestones as $startup )  
						<li>{{ $startup->startup_name }}</li>
@endforeach 					
					</ul>
				</dd>
        	</dl>				 
  
		</div>


		             

@stop @section('bottom_style')
@stop @section('bottom_script') @stop
