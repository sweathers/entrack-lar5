<dl>
   	
	

    	<dt>Milestone Name</dt><!--  $milestone->milestone_name -->   
    	<dd>{{ $milestone->milestone_name }}&nbsp;</dd>

  	
	
    	<dt>Description</dt><!--  $milestone->description -->   
    	<dd><p>{{ $milestone->description }}&nbsp;</p></dd>

  	
	
    	<dt>Helpful Information</dt><!--  $milestone->helpful_information -->   
    	<dd><p>{{ $milestone->helpful_information }}&nbsp;</p></dd>

  	
	
    	<dt>Helpful Person</dt><!--  $milestone->helpful_person -->   
    	<dd><p>{{ $milestone->helpful_person }}&nbsp;</p></dd>

  	
	
    	<dt>Complete Descr</dt><!--  $milestone->complete_descr -->   
    	<dd><p>{{ $milestone->complete_descr }}&nbsp;</p></dd>

  	
	

    	<dt>Score</dt><!--  $milestone->score -->   
    	<dd>{{ $milestone->score }}&nbsp;</dd>

  	
	

    	<dt>Sort Order</dt><!--  $milestone->sort_order -->   
    	<dd>{{ $milestone->sort_order }}&nbsp;</dd>

 	</dl>
