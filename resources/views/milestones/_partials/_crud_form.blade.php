<div class="form-group">
   	
	
    <div class="col-md-6">
    	{!! Form::label('milestone_name', 'Milestone Name') !!} 
    	{!! Form::text( 'milestone_name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_milestone_name" class="text-danger">{!! $errors->first('milestone_name', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('description', 'Description') !!} 
    	{!! Form::textarea( 'description', null, ['class' => 'form-control']) !!}
    	<p      id="msg_description" class="text-danger">{!! $errors->first('description', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('helpful_information', 'Helpful Information') !!} 
    	{!! Form::textarea( 'helpful_information', null, ['class' => 'form-control']) !!}
    	<p      id="msg_helpful_information" class="text-danger">{!! $errors->first('helpful_information', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('helpful_person', 'Helpful Person') !!} 
    	{!! Form::textarea( 'helpful_person', null, ['class' => 'form-control']) !!}
    	<p      id="msg_helpful_person" class="text-danger">{!! $errors->first('helpful_person', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('complete_descr', 'Complete Descr') !!} 
    	{!! Form::textarea( 'complete_descr', null, ['class' => 'form-control']) !!}
    	<p      id="msg_complete_descr" class="text-danger">{!! $errors->first('complete_descr', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('score', 'Score') !!} 
    	{!! Form::text( 'score', null, ['class' => 'form-control']) !!}
    	<p      id="msg_score" class="text-danger">{!! $errors->first('score', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('sort_order', 'Sort Order') !!} 
    	{!! Form::text( 'sort_order', null, ['class' => 'form-control']) !!}
    	<p      id="msg_sort_order" class="text-danger">{!! $errors->first('sort_order', ':message') !!}</p>
    </div>
 
</div>