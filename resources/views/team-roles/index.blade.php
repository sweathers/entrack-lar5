@extends('_layouts.index-layout') 
@section('title') Team Role @stop
@section('content-header')  Team Role  @stop

@section('header-nav')   <li><a class="btn-menu create" href="/public/team-roles/create">Create New Team Role</a></li>	
 @stop

@section('content')

          <ul class="link-list">
@foreach ($teamRoles as $index => $row)
            <li>
 						  						 	<div class="NAME team_role_name">
											<a href="/public/team-roles/{{$row->team_role_id}}">{{$row->team_role_name}}</a> 
					 
				</div>
			  												<!--  <div class="CODE team_role_code">{{$row->team_role_code}}</div>   -->
   												<!--  <div class="BOOLEAN is_default">{{$row->is_default}}</div>   -->
   												<!--  <div class="INT score">{{$row->score}}</div>   -->
   												<!--  <div class="SORT sort_order">{{$row->sort_order}}</div>   -->
                 
				    <a class="coverall" href="/public/team-roles/{{$row->team_role_id}}"></a> 
   
            </li>
@endforeach
		  </ul>

@stop @section('bottom_style') 
@stop @section('bottom_script') @stop