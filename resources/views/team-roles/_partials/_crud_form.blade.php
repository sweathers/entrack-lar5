<div class="form-group">
   	
	
    <div class="col-md-6">
    	{!! Form::label('team_role_name', 'Team Role Name') !!} 
    	{!! Form::text( 'team_role_name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_team_role_name" class="text-danger">{!! $errors->first('team_role_name', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('team_role_code', 'Type Code') !!} 
    	{!! Form::text( 'team_role_code', null, ['class' => 'form-control']) !!}
    	<p      id="msg_team_role_code" class="text-danger">{!! $errors->first('team_role_code', ':message') !!}</p>
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'is_default', 'Is Default') !!} 
    	{!! Form::select('is_default', array('0' => 'no', '1' => 'yes'), null, ['class' => 'form-control']) !!}
    	<p       id="msg_is_default" class="text-danger">{!! $errors->first('is_default', ':message') !!} </p>
    </div>			
  	
	
    <div class="col-md-6">
    	{!! Form::label('score', 'Score') !!} 
    	{!! Form::text( 'score', null, ['class' => 'form-control']) !!}
    	<p      id="msg_score" class="text-danger">{!! $errors->first('score', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('sort_order', 'Sort Order') !!} 
    	{!! Form::text( 'sort_order', null, ['class' => 'form-control']) !!}
    	<p      id="msg_sort_order" class="text-danger">{!! $errors->first('sort_order', ':message') !!}</p>
    </div>
 
</div>