@extends('_layouts.view-layout') 

@section('title') Team Role @stop
@section('content-header') Team Role @stop

@section('header-nav')
   
	<li><a class="btn-menu index" href="/public/team-roles">Back to Team Role List</a></li>
	<li><a class="btn-menu edit" href="/public/team-roles/{{$teamRole->team_role_id}}/edit" 
       data-id="{{$teamRole->team_role_id}}"> Edit</a></li> 
 

<!--  <li><a href="#" class="btn-menu delete"
 	
						data-href="/public/team-roles"
						data-successurl="/public/team-roles"
 
						data-id="{{$teamRole->team_role_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$teamRole->team_role_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a></li> 
							-->
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop

@section('content')

@include('team-roles/_partials/_view_elements')

	



		             

@stop @section('bottom_style')
@stop @section('bottom_script') @stop
