@extends('_layouts.edit-layout') 

@section('title') Team Role @stop
@section('content-header')Edit Team Role @stop

@section('header-nav')
<li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li>
<!-- <li><a class="btn-menu index" href="/public/team-roles">Back to Team Role List</a></li>  -->

@stop

@section('content')
		{!! Form::model($teamRole, 
			array('method'=> 'PUT', 
				'route' => array('team-roles.update', 
				                        $teamRole->team_role_id ))) !!}
		{!! Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('team-roles.show')) ?  route('team-roles.show', array($teamRole->team_role_id)) : '' )) ) !!}
@include('team-roles/_partials/_crud_form')


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}                  

@stop @section('bottom_style')
@stop @section('bottom_script')  @stop
