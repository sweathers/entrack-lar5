    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" media="all" href="/public/img/favicon.png">

	<title>@yield('html-title','EnTrack - Tracking Startup Success')</title>
    <!--  <link media="all" type="text/css" rel="stylesheet" href="/public/packages/bootstrap/css/bootstrap.min.css">   -->  
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>

	<link media="all" type="text/css" rel="stylesheet" href="/public/css/site.css">
	<link media="all" type="text/css" rel="stylesheet" href="/public/css/wireframe.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
	@yield('top_style')
	@yield('top_script')

