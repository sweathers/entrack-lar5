<!DOCTYPE html>
<html lang="en">
  <head>
@include('_tpl.header')
@yield('header_addons')
  </head>
  <body class="edit-screen">
  @include('_tpl.header-top-nav-bar')
    <div id="page-container">
      <div id="content-container">
        <div id="content-header">
		   <h2>@yield('content-header')</h2>
        @if (isset($message))  
				<!-- Validation Message -->
				<div class="msg-danger">
				    {{ $message }}
				</div>
        @endif
		@if(count($errors) >= 1)
				<div class="alert alert-error">
		        @foreach ($errors->keys() as $errorKey )
                	<div class="error">{{$errorKey}} - {{$errors->first($errorKey, ':message') }}</div>
        		@endforeach
                	<div class="error">{{ $errors->first('error', ':message') }}</div>
            	</div>
        @endif
        @if (isset($success))
        		<div class="alert alert-success">
                    <div>{{ $success }}</div>
            	</div>        
        @endif
	
        	</div>
            <div class="content-section-container">
         		@yield('content')      
      		</div>
        </div>
        @include('_tpl.side-nav')
    </div>
    @include('_tpl.footer')
           
@yield('bottom_style')
    <script src="/public/packages/jquery/js/jquery.min.js"></script>
    <!--  <script src="/public/packages/bootstrap/js/bootstrap.min.js"></script>  -->
    <script src="/public/js/site.js"></script>

<!-- MULTISELECT -->
    <script src="/public/packages/bootstrap/plugin/multiselect/bootstrap-multiselect.js"></script>
    <link media="all" type="text/css" rel="stylesheet" href="/public/packages/bootstrap/plugin/multiselect/bootstrap-multiselect.css">
@yield('bottom_script')

  </body>
</html>
