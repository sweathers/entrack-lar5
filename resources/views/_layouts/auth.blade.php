<!DOCTYPE html>
<html lang="en">
  @include('_tpl.header')
  </head>
<body class="auth-screen" >
    <div id="page-container">
      <div id="content-container">
  		
		<div id="content-header">
			<!-- <img src="/public/img/myproto-logo.png" id="login-logo" alt="MyProto - Helping Startups Build" />   -->
			<h1>EnTrack - Tracking Startup Success</h1>
		   <h2>@yield('content-header')</h2>
        @if (isset($message))  
				<!-- Validation Message -->
				<div class="msg-danger">
				    {{ $message }}
				</div>
        @endif
		@if(count($errors) >= 1)
        		<div class="alert alert-error">
                	<div class="error">{{ $errors->first('error', ':message') }}</div>
            	</div>
        @endif
        @if (isset($success))
        		<div class="alert alert-success">
                    <div>{{ $success }}</div>
            	</div>        
        @endif
		
        </div>
        <div class="content-section-container">
         		@yield('content')      
      	</div>
     </div>        
  </div>
  @include('_tpl.footer')
</body>
</html>
