<!DOCTYPE html>
<html lang="en">
  <head>
@include('_tpl.header')
@yield('header_addons')
  </head>
  <body class="view-screen">
  @include('_tpl.header-top-nav-bar')
    <div id="page-container">
      <div id="content-container">
        <div id="content-header">
		   <h2>@yield('content-header')</h2>
			@if (isset($message))  
				<!-- Validation Message -->
				<div class="msg-danger">
				    {{ $message }}
				</div>
        	@endif
        </div>
        <div class="content-section-container">
         	   @yield('content')      
      	</div>
      </div>
      @include('_tpl.side-nav')
    </div>
      @include('_tpl.footer')

  </div>

@yield('bottom_style')
    <script src="/public/packages/jquery/js/jquery.min.js"></script>
    <!--  <script src="/public/packages/bootstrap/js/bootstrap.min.js"></script>  -->
    <script src="/public/js/site.js"></script>

@yield('bottom_plugin_script')

  </body>
</html>
