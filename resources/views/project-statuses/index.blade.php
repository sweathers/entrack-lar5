@extends('_layouts.index-layout') 
@section('title') Project Status @stop
@section('content-header')  Project Status  @stop

@section('header-nav')   <li><a class="btn-menu create" href="/public/project-statuses/create">Create New Project Status</a></li>	
 @stop

@section('content')

          <ul class="link-list">
@foreach ($projectStatuses as $index => $row)
            <li>
 						  						 	<div class="NAME project_status_name">
											<a href="/public/project-statuses/{{$row->project_status_id}}">{{$row->project_status_name}}</a> 
					 
				</div>
			  												<!--  <div class="CODE project_status_code">{{$row->project_status_code}}</div>   -->
   												<!--  <div class="SORT sort_order">{{$row->sort_order}}</div>   -->
                 
				    <a class="coverall" href="/public/project-statuses/{{$row->project_status_id}}"></a> 
   
            </li>
@endforeach
		  </ul>

@stop @section('bottom_style') 
@stop @section('bottom_script') @stop