@extends('_layouts.edit-layout') 

@section('title') Project Status @stop
@section('content-header')Edit Project Status @stop

@section('header-nav')
<li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li>
<!-- <li><a class="btn-menu index" href="/public/project-statuses">Back to Project Status List</a></li>  -->

@stop

@section('content')
		{!! Form::model($projectStatus, 
			array('method'=> 'PUT', 
				'route' => array('project-statuses.update', 
				                        $projectStatus->project_status_id ))) !!}
		{!! Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('project-statuses.show')) ?  route('project-statuses.show', array($projectStatus->project_status_id)) : '' )) ) !!}
@include('project-statuses/_partials/_crud_form')


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}                  

@stop @section('bottom_style')
@stop @section('bottom_script')  @stop
