<div class="form-group">
   	
	
    <div class="col-md-6">
    	{!! Form::label('project_status_name', 'Project Status Name') !!} 
    	{!! Form::text( 'project_status_name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_project_status_name" class="text-danger">{!! $errors->first('project_status_name', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('project_status_code', 'Type Code') !!} 
    	{!! Form::text( 'project_status_code', null, ['class' => 'form-control']) !!}
    	<p      id="msg_project_status_code" class="text-danger">{!! $errors->first('project_status_code', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('sort_order', 'Sort Order') !!} 
    	{!! Form::text( 'sort_order', null, ['class' => 'form-control']) !!}
    	<p      id="msg_sort_order" class="text-danger">{!! $errors->first('sort_order', ':message') !!}</p>
    </div>
 
</div>