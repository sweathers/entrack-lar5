@extends('_layouts.view-layout') 

@section('title') Project Status @stop
@section('content-header') Project Status @stop

@section('header-nav')
   
	<li><a class="btn-menu index" href="/public/project-statuses">Back to Project Status List</a></li>
	<li><a class="btn-menu edit" href="/public/project-statuses/{{$projectStatus->project_status_id}}/edit" 
       data-id="{{$projectStatus->project_status_id}}"> Edit</a></li> 
 

<!--  <li><a href="#" class="btn-menu delete"
 	
						data-href="/public/project-statuses"
						data-successurl="/public/project-statuses"
 
						data-id="{{$projectStatus->project_status_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$projectStatus->project_status_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a></li> 
							-->
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop

@section('content')

@include('project-statuses/_partials/_view_elements')

	



		             

@stop @section('bottom_style')
@stop @section('bottom_script') @stop
