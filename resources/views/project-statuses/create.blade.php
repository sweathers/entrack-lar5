@extends('_layouts.edit-layout') 

@section('title') Project Status @stop

@section('content-header')  Create Project Status   @stop

@section('header-nav')   <li><a class="btn-menu index" href="/public/project-statuses">Back to Project Status List</a></li>	
 
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop


@section('content')

		{!!  Form::model($projectStatus,
				array('method'=> 'POST', 'class' => 'form-horizontal form-bordered', 
						'route' => array('project-statuses.store'))) !!}
		
@include('project-statuses/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])
		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}

@stop  @section('bottom_style') 
@stop @section('bottom_script') @stop