@extends('_layouts.index-layout') 
@section('title') Startup Milestone @stop
@section('content-header') 	
	Startup Milestone for Startup "{{ $startup->startup_name or '' }}"
 @stop

@section('header-nav')  <li><a class="btn-menu create" href="/public/startups/{{$startup->startup_id or 0}}/startup-milestones/create">Create New Startup Milestone</a></li>
 @stop

@section('content')

          <ul class="link-list">
@foreach ($startupMilestones as $index => $row)
            <li>
 						  												<!-- <div class="XREFID startup_id">{{$row->startup_name}}</div>   -->
				   							<div class="NAME XREFID milestone_id"><a href="/public/startups/{{$startup->startup_id or 0}}/startup-milestones/{{$row->xref_s_m_id}}">{{$row->milestone_name}}</a></div> 
			  												<!--  <div class="DATETIME date_completed">{{$row->date_completed}}</div>   -->
   						  												<!--  <div class="ISACTIVE is_active">{{$row->is_active}}</div>   -->
                 
					
					<a class="coverall" href="/public/startups/{{$startup->startup_id or 0}}/startup-milestones/{{$row->xref_s_m_id}}"></a> 
   
            </li>
@endforeach
		  </ul>

@stop @section('bottom_style') 
@stop @section('bottom_script') @stop