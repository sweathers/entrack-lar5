@extends('_layouts.edit-layout') 

@section('title') Startup Milestone @stop
@section('content-header')Edit Startup Milestone @stop

@section('header-nav')
<li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li>
<!-- <li><a class="btn-menu index" href="/public/startup-milestones">Back to Startup Milestone List</a></li>  -->

@stop

@section('content')
		{!! Form::model($startupMilestone, 
			array('method'=> 'PUT', 
				'route' => array('startup-milestones.update', 
				                        $startupMilestone->xref_s_m_id ))) !!}
		{!! Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('startup-milestones.show')) ?  route('startup-milestones.show', array($startupMilestone->xref_s_m_id)) : '' )) ) !!}
@include('startup-milestones/_partials/_crud_form')


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}                  

@stop @section('bottom_style')
@stop @section('bottom_script')  @stop
