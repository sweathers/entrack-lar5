@extends('_layouts.view-layout') 

@section('title') Startup Milestone @stop
@section('content-header') Startup Milestone @stop

@section('header-nav')
  <li><a class="btn-menu index" href="/public/startups/{{$startup->startup_id or 0}}/startup-milestones">Back to Startup-Startup Milestone List</a><li>
<li><a class="btn-menu edit" href="/public/startup-milestones/{{$startupMilestone->xref_s_m_id}}/edit?successUrl={{((Route::getRoutes()->hasNamedRoute('startups.startup-milestones.show')) ? route('startups.startup-milestones.show', array($startup->startup_id, $startupMilestone->xref_s_m_id)) : '')}}" 
       data-id="{{$startupMilestone->xref_s_m_id}}"> Edit</a></li> 
 

<!--  <li><a href="#" class="btn-menu delete"
						data-href="/public/startups/{{$startup->startup_id or 0}}/startup-milestones"
						data-successurl="/public/startups/{{$startup->startup_id or 0}}/startup-milestones"
 
						data-id="{{$startupMilestone->xref_s_m_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$startupMilestone->name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a></li> 
							-->
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop

@section('content')

@include('startup-milestones/_partials/_view_elements')

<hr />
@include('milestones/_partials/_view_elements')
 	



		             

@stop @section('bottom_style')
@stop @section('bottom_script') @stop
