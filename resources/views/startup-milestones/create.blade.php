@extends('_layouts.edit-layout') 

@section('title') Startup Milestone @stop

@section('content-header')  Create Startup Milestone   @stop

@section('header-nav')   <li><a class="btn-menu index" href="/public/startup-milestones">Back to Startup Milestone List</a></li>	
 
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop


@section('content')

		{!!  Form::model($startupMilestone,
				array('method'=> 'POST', 'class' => 'form-horizontal form-bordered', 
						'route' => array('startup-milestones.store'))) !!}
		
@include('startup-milestones/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])
		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}

@stop  @section('bottom_style') 
@stop @section('bottom_script') @stop