@extends('_layouts.view-layout') 

@section('title') User Profile @stop
@section('content-header') User Profile @stop

@section('header-nav')
   
	<li><a class="btn-menu index" href="/public/user-profiles">Back to User Profile List</a></li>
	<li><a class="btn-menu edit" href="/public/user-profiles/{{$userProfile->id}}/edit" 
       data-id="{{$userProfile->id}}"> Edit</a></li> 
 

<!--  <li><a href="#" class="btn-menu delete"
 	
						data-href="/public/user-profiles"
						data-successurl="/public/user-profiles"
 
						data-id="{{$userProfile->id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$userProfile->name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a></li> 
							-->
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop

@section('content')

@include('user-profiles/_partials/_view_elements')

	



		             

@stop @section('bottom_style')
@stop @section('bottom_script') @stop
