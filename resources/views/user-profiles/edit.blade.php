@extends('_layouts.edit-layout') 

@section('title') User Profile @stop
@section('content-header')Edit User Profile @stop

@section('header-nav')
<li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li>
<!-- <li><a class="btn-menu index" href="/public/user-profiles">Back to User Profile List</a></li>  -->

@stop

@section('content')
		{!! Form::model($userProfile, 
			array('method'=> 'PUT', 
				'route' => array('user-profiles.update', 
				                        $userProfile->id ))) !!}
		{!! Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('user-profiles.show')) ?  route('user-profiles.show', array($userProfile->id)) : '' )) ) !!}
@include('user-profiles/_partials/_crud_form')


		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}                  

@stop @section('bottom_style')
@stop @section('bottom_script')  @stop
