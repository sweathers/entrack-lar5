@extends('_layouts.index-layout') 
@section('title') Startup @stop
@section('content-header')  Startup  @stop

@section('header-nav')   <li><a class="btn-menu create" href="/public/startups/create">Create New Startup</a></li>	
 @stop

@section('content')

          <ul class="link-list">
@foreach ($startups as $index => $row)
            <li>
 						  						 	<div class="NAME startup_name">
											<a href="/public/startups/{{$row->startup_id}}">{{$row->startup_name}}</a> 
					 
				</div>
			  												<!-- <div class="TYPETABLE location_place_id">{{$row->locationPlace_name}}</div>   -->
				   												<!-- <div class="OWNER user_id">{{$row->userProfile_name}}</div>   -->
				   												<!--  <div class="DATETIME date_started">{{$row->date_started}}</div>   -->
   						  						  												<!--  <div class="DECIMAL capital_raised">{{$row->capital_raised}}</div>   -->
   												<!--  <div class="SORT sort_order">{{$row->sort_order}}</div>   -->
   												<div class="TEXT milestone_names"> {{$row->milestone_names}} </div>
                 
				    <a class="coverall" href="/public/startups/{{$row->startup_id}}"></a> 
   
            </li>
@endforeach
		  </ul>

@stop @section('bottom_style') 
@stop @section('bottom_script') @stop