@extends('_layouts.edit-layout') 

@section('title') Startup @stop
@section('content-header')Edit Startup @stop

@section('header-nav')
<li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li>
<!-- <li><a class="btn-menu index" href="/public/startups">Back to Startup List</a></li>  -->

@stop

@section('content')
		{!! Form::model($startup, 
			array('method'=> 'PUT', 
				'route' => array('startups.update', 
				                        $startup->startup_id ))) !!}
		{!! Form::hidden( 'success_url', ((!empty($successUrl))? urldecode($successUrl) : ((Route::getRoutes()->hasNamedRoute('startups.show')) ?  route('startups.show', array($startup->startup_id)) : '' )) ) !!}
@include('startups/_partials/_crud_form')

		<div class="form-group">	
			<div class="col-md-4 crud-multiselect-container">
        		{!! Form::label('milestones_selected', 'Completed Milestone') !!} 
        		{!! Form::hidden('milestones_selected_count', '$milestones_selected_count') !!} 
        		{!! Form::select('milestones_selected', $milestone_options, $milestones_selected, ['class'=>'crud-multiselect-small', 'multiple'=>'multiple', 'name' => 'milestones_selected[]']) !!}
        	</div>				 
  
		</div>

		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}                  

@stop @section('bottom_style')
@stop @section('bottom_script') <script src="/public/packages/bootstrap/plugin/multiselect/bootstrap-multiselect.js"></script>
  @stop
