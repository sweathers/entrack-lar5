<div class="form-group">
   	
	
    <div class="col-md-6">
    	{!! Form::label('startup_name', 'Startup Name') !!} 
    	{!! Form::text( 'startup_name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_startup_name" class="text-danger">{!! $errors->first('startup_name', ':message') !!}</p>
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'location_place_id', 'Location Place') !!} 
    	{!! Form::select('location_place_id', array('0' => 'Select ...') + $locationPlaceList->toArray(), null, ['class' => 'form-control']) !!}
    	<p       id="msg_location_place_id" class="text-danger">{!! $errors->first('location_place_id', ':message') !!} </p>
    </div>
      <div class="col-md-6">
    	{!! Form::label( 'user_id', 'User') !!} 
    	{!! Form::select('user_id', $userProfileList, null, ['class' => 'form-control']) !!}
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('date_started', 'Date Started') !!} 
    	{!! Form::text( 'date_started', null, ['class' => 'form-control']) !!}
    	<p      id="msg_date_started" class="text-danger">{!! $errors->first('date_started', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('about', 'About') !!} 
    	{!! Form::textarea( 'about', null, ['class' => 'form-control']) !!}
    	<p      id="msg_about" class="text-danger">{!! $errors->first('about', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('target_industry', 'Target Industry') !!} 
    	{!! Form::text( 'target_industry', null, ['class' => 'form-control']) !!}
    	<p      id="msg_target_industry" class="text-danger">{!! $errors->first('target_industry', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('capital_raised', 'Capital Already Raised') !!} 
    	{!! Form::text( 'capital_raised', null, ['class' => 'form-control']) !!}
    	<p      id="msg_capital_raised" class="text-danger">{!! $errors->first('capital_raised', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('sort_order', 'Sort Order') !!} 
    	{!! Form::text( 'sort_order', null, ['class' => 'form-control']) !!}
    	<p      id="msg_sort_order" class="text-danger">{!! $errors->first('sort_order', ':message') !!}</p>
    </div>
 
</div>