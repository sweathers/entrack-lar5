@extends('_layouts.view-layout') 

@section('title') Startup @stop
@section('content-header') Startup @stop

@section('header-nav')
   
	<li><a class="btn-menu index" href="/public/startups">Back to Startup List</a></li>
	<li><a class="btn-menu edit" href="/public/startups/{{$startup->startup_id}}/edit" 
       data-id="{{$startup->startup_id}}"> Edit</a></li> 
 

<!--  <li><a href="#" class="btn-menu delete"
 	
						data-href="/public/startups"
						data-successurl="/public/startups"
 
						data-id="{{$startup->startup_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$startup->startup_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a></li> 
							-->
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop

@section('content')

@include('startups/_partials/_view_elements')

	

		<div class="form-group">	
			<dl>
				<dt>Completed Milestones</dt>
				<dd>
					<ul> 				
@foreach ($startup->startupMilestones as $milestone )  
						<li>{{ $milestone->milestone_name }}</li>
@endforeach 					
					</ul>
				</dd>
        	</dl>				 
  
		</div>

	
		<div class="form-group">
 
			<dl>
				<dt><a href="/public/startups/{{$startup->startup_id}}/team-members" >Team Members</a></dt>
				<dd>
					<ul>				
@foreach ($startup->startupTeamMembers as $teamMember )  
						<li><a href="/public/startups/{{$startup->startup_id}}/team-members/{{$teamMember->team_member_id}}">{{ $teamMember->team_member_name }}</a></li>
@endforeach 					
					</ul>
				</dd>
        	</dl>	 
  
			<dl>
				<dt><a href="/public/startups/{{$startup->startup_id}}/assumptions" >Assumptions</a></dt>
				<dd>
					<ul>				
@foreach ($startup->startupAssumptions as $assumption )  
						<li><a href="/public/startups/{{$startup->startup_id}}/assumptions/{{$assumption->assumption_id}}">{{ $assumption->assumption_name }}</a></li>
@endforeach 					
					</ul>
				</dd>
        	</dl>	 
  
			<dl>
				<dt><a href="/public/startups/{{$startup->startup_id}}/projects" >Projects</a></dt>
				<dd>
					<ul>				
@foreach ($startup->startupProjects as $project )  
						<li><a href="/public/startups/{{$startup->startup_id}}/projects/{{$project->project_id}}">{{ $project->project_name }}</a></li>
@endforeach 					
					</ul>
				</dd>
        	</dl>	 
 		</div>

		             

@stop @section('bottom_style')
@stop @section('bottom_script') @stop
