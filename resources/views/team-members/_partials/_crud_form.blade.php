<div class="form-group">
   	
	
    <div class="col-md-6">
    	{!! Form::label('team_member_name', 'Team Member Name') !!} 
    	{!! Form::text( 'team_member_name', null, ['class' => 'form-control']) !!}
    	<p      id="msg_team_member_name" class="text-danger">{!! $errors->first('team_member_name', ':message') !!}</p>
    </div>
      <div class="col-md-6">
    	{!! Form::label( 'startup_id', 'Startup') !!} 
    	{!! Form::select('startup_id', $startupList, null, ['class' => 'form-control']) !!}
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'team_role_id', 'Team Role') !!} 
    	{!! Form::select('team_role_id', array('0' => 'Select ...') + $teamRoleList->toArray(), null, ['class' => 'form-control']) !!}
    	<p       id="msg_team_role_id" class="text-danger">{!! $errors->first('team_role_id', ':message') !!} </p>
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'team_relationship_id', 'Team Relationship') !!} 
    	{!! Form::select('team_relationship_id', array('0' => 'Select ...') + $teamRelationshipList->toArray(), null, ['class' => 'form-control']) !!}
    	<p       id="msg_team_relationship_id" class="text-danger">{!! $errors->first('team_relationship_id', ':message') !!} </p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('title', 'Title') !!} 
    	{!! Form::text( 'title', null, ['class' => 'form-control']) !!}
    	<p      id="msg_title" class="text-danger">{!! $errors->first('title', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('contact_email', 'Contact Email') !!} 
    	{!! Form::text( 'contact_email', null, ['class' => 'form-control']) !!}
    	<p      id="msg_contact_email" class="text-danger">{!! $errors->first('contact_email', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('contact_phone', 'Contact Phone') !!} 
    	{!! Form::text( 'contact_phone', null, ['class' => 'form-control']) !!}
    	<p      id="msg_contact_phone" class="text-danger">{!! $errors->first('contact_phone', ':message') !!}</p>
    </div>
  	
	
    <div class="col-md-6">
    	{!! Form::label('hours_per_month', 'Hours Per Month') !!} 
    	{!! Form::text( 'hours_per_month', null, ['class' => 'form-control']) !!}
    	<p      id="msg_hours_per_month" class="text-danger">{!! $errors->first('hours_per_month', ':message') !!}</p>
    </div>
  					
    <div class="col-md-6">
    	{!! Form::label( 'has_equity', 'Has Equity') !!} 
    	{!! Form::select('has_equity', array('0' => 'no', '1' => 'yes'), null, ['class' => 'form-control']) !!}
    	<p       id="msg_has_equity" class="text-danger">{!! $errors->first('has_equity', ':message') !!} </p>
    </div>			
  	
	
    <div class="col-md-6">
    	{!! Form::label('sort_order', 'Sort Order') !!} 
    	{!! Form::text( 'sort_order', null, ['class' => 'form-control']) !!}
    	<p      id="msg_sort_order" class="text-danger">{!! $errors->first('sort_order', ':message') !!}</p>
    </div>
 
</div>