<dl>
   	
	

    	<dt>Team Member Name</dt><!--  $teamMember->team_member_name -->   
    	<dd>{{ $teamMember->team_member_name }}&nbsp;</dd>

  
    	<dt>Startup</dt><!--  $teamMember->startup_id -->    	
		<dd>{{ $teamMember->startup->startup_name }}</dd>
    	
  					
        <dt>Team Role</dt><!--  $teamMember->team_role_id --> 	
		<dd>{{ $teamMember->teamRole->team_role_name  or "&nbsp;"  }}</dd>
		
		

  					
        <dt>Team Relationship</dt><!--  $teamMember->team_relationship_id --> 	
		<dd>{{ $teamMember->teamRelationship->team_relationship_name  or "&nbsp;"  }}</dd>
		
		

  	
	

    	<dt>Title</dt><!--  $teamMember->title -->   
    	<dd>{{ $teamMember->title }}&nbsp;</dd>

  	
	

    	<dt>Contact Email</dt><!--  $teamMember->contact_email -->   
    	<dd>{{ $teamMember->contact_email }}&nbsp;</dd>

  	
	

    	<dt>Contact Phone</dt><!--  $teamMember->contact_phone -->   
    	<dd>{{ $teamMember->contact_phone }}&nbsp;</dd>

  	
	

    	<dt>Hours Per Month</dt><!--  $teamMember->hours_per_month -->   
    	<dd>{{ $teamMember->hours_per_month }}&nbsp;</dd>

  					

    	<dt>Has Equity</dt><!--  $teamMember->has_equity -->    	
    	<dd>{{ ($teamMember->has_equity > 0) ? 'yes' : 'no' }}</dd>
			
  	
	

    	<dt>Sort Order</dt><!--  $teamMember->sort_order -->   
    	<dd>{{ $teamMember->sort_order }}&nbsp;</dd>

 	</dl>
