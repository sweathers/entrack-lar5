@extends('_layouts.view-layout') 

@section('title') Team Member @stop
@section('content-header') Team Member @stop

@section('header-nav')
  <li><a class="btn-menu index" href="/public/startups/{{$startup->startup_id or 0}}/team-members">Back to Startup-Team Member List</a><li>
<li><a class="btn-menu edit" href="/public/team-members/{{$teamMember->team_member_id}}/edit?successUrl={{((Route::getRoutes()->hasNamedRoute('startups.team-members.show')) ? route('startups.team-members.show', array($startup->startup_id, $teamMember->team_member_id)) : '')}}" 
       data-id="{{$teamMember->team_member_id}}"> Edit</a></li> 
 

<!--  <li><a href="#" class="btn-menu delete"
						data-href="/public/startups/{{$startup->startup_id or 0}}/team-members"
						data-successurl="/public/startups/{{$startup->startup_id or 0}}/team-members"
 
						data-id="{{$teamMember->team_member_id}}" data-toggle="modal"
						data-target=".delete-modal" data-title="Confirm Delete"
						data-body="Are you sure you want to delete <b>{{$teamMember->team_member_name}}</b>? "><i
							class="glyphicon glyphicon-remove"></i> Delete </a></li> 
							-->
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop

@section('content')

@include('team-members/_partials/_view_elements')

	

		<div class="form-group">	
			<dl>
				<dt>Projects</dt>
				<dd>
					<ul> 				
@foreach ($teamMember->projectTeamMembers as $project )  
						<li>{{ $project->project_name }}</li>
@endforeach 					
					</ul>
				</dd>
        	</dl>				 
  
		</div>


		             

@stop @section('bottom_style')
@stop @section('bottom_script') @stop
