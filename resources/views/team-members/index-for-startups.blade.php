@extends('_layouts.index-layout') 
@section('title') Team Member @stop
@section('content-header') 	
	Team Member for Startup "{{ $startup->startup_name or '' }}"
 @stop

@section('header-nav')  <li><a class="btn-menu create" href="/public/startups/{{$startup->startup_id or 0}}/team-members/create">Create New Team Member</a></li>
 @stop

@section('content')

          <ul class="link-list">
@foreach ($teamMembers as $index => $row)
            <li>
 						  						 	<div class="NAME team_member_name">
										
						<a href="/public/startups/{{$startup->startup_id or 0}}/team-members/{{$row->team_member_id}}">{{$row->team_member_name}}</a> 
					 
				</div>
			  												<!-- <div class="PARENT startup_id">{{$row->startup_name}}</div>   -->
				   												<!-- <div class="TYPETABLE team_role_id">{{$row->teamRole_name}}</div>   -->
				   												<!-- <div class="TYPETABLE team_relationship_id">{{$row->teamRelationship_name}}</div>   -->
				   												<!--  <div class="TEXT_64 title">{{$row->title}}</div>   -->
   												<!--  <div class="TEXT_128 contact_email">{{$row->contact_email}}</div>   -->
   												<!--  <div class="TEXT_32 contact_phone">{{$row->contact_phone}}</div>   -->
   												<!--  <div class="DECIMAL hours_per_month">{{$row->hours_per_month}}</div>   -->
   												<!--  <div class="BOOLEAN has_equity">{{$row->has_equity}}</div>   -->
   												<!--  <div class="SORT sort_order">{{$row->sort_order}}</div>   -->
                 
					
					<a class="coverall" href="/public/startups/{{$startup->startup_id or 0}}/team-members/{{$row->team_member_id}}"></a> 
   
            </li>
@endforeach
		  </ul>

@stop @section('bottom_style') 
@stop @section('bottom_script') @stop