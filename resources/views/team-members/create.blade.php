@extends('_layouts.edit-layout') 

@section('title') Team Member @stop

@section('content-header')  Create Team Member   @stop

@section('header-nav')   <li><a class="btn-menu index" href="/public/team-members">Back to Team Member List</a></li>	
 
<!--  <li><a class="btn-menu back" href="{{ URL::previous() }}" class="btn btn-default">Back</a></li> -->
@stop


@section('content')

		{!!  Form::model($teamMember,
				array('method'=> 'POST', 'class' => 'form-horizontal form-bordered', 
						'route' => array('team-members.store'))) !!}
		
@include('team-members/_partials/_crud_form', ['form-class' => 'form-horizontal form-bordered'])
		<div class="crud-form-buttons" >
			<a href="{{ URL::previous() }}" class="btn btn-default">Close</a>
			{!! Form::submit('Save') !!}
		</div>
		{!! Form::close() !!}

@stop  @section('bottom_style') 
@stop @section('bottom_script') @stop