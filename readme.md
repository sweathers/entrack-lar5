# The MyProto EnTrack Demo

EnTrack (pronounced "On Track" and short for ENTRepreneur TRACKer) is a demo project to test the CRUD functionality of a database driven website with complex database relationships.  

## Installation
1.  PreReqs - *AMP environment (dev.entrack.local), composer, bitbucket.org account, unix shell (CYGWIN recommended for windows)
2.  Checkout this reposity into the Apache Docroot Folder.  Switch to a development branch.
3.  chmod 755 bootstrap/cache; chmod 755 storage; mkdir vendor; touch vendor/autoload.php
4.  Run 'composer install' to install vendor files not included in repository
4b.  If 4a fails, run 'composer install --no-scripts' 
5.  Create DB and Import project data
6.  create .env file in docroot
7.  run 'php artisan migrate'
8.  Test http://dev.entrack.local/public/ 

## License
 
EnTrack and the underlying templates are copyrighted 2016 by MyProto Technology, LLC and may only be used with the written permission of MyProto.