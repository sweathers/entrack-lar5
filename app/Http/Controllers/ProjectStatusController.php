<?php

/** 
 * Controller for Project Status 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\ProjectStatus;


class ProjectStatusController extends Controller {
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * ProjectStatus @ index
	 * Display a list of ProjectStatuses and links for CRUD actions
	 *
	 * @param int $parentId (optional, Used by helper method for filtering by a referenced table. )
	 * @param string $parentSlug (optional, Used by helper method for filtering by a referenced table.)
	 * @return Response
	 */
	public function index($refId=0, $refSlug='')
	{
		$projectStatuses = ProjectStatus::authed( $this->user_id, $this->user_is_admin, 'view' )
		    
		   ->orderBy('sort_order', 'asc') 
		   ->get();
		$data['projectStatuses']=array();
			
		//REFERENCED RESOURCE NAMES
		foreach ($projectStatuses as $row)
		{				
			//get OTM names


			//get MTM names
			$data['projectStatuses'][]=$row;	
		} /* end foreach row */
	

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'project-statuses.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
		return view('project-statuses.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 * ProjectStatus @ create
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($refId=0, $refSlug='')
	{
		$data = array();
		$projectStatus = new ProjectStatus; //MODEL for default values and form model binding
		
		//REFERENCED RESOURCE SELECT LIST
				
		
		
		$data['projectStatus'] = $projectStatus; 

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'project-statuses.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('project-statuses.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);	
	}


	/**
	 * ProjectStatus @ store
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($refId=0, $refSlug='')
	{

		$validator = Validator::make(Input::all(), ProjectStatus::$insertRules);

		if($validator->passes()) {
				$projectStatus = new ProjectStatus;
				$projectStatus->fill(Input::all());
			
					
			//SET DEFAULT VALUES ON CREATE
			if (! $projectStatus->sort_order) $projectStatus->sort_order = 20;
						
			//SET CODE VALUE FROM NAME
			if (! $projectStatus->project_status_code) {
				$str=$projectStatus->project_status_name;
				//sanitize the string to all lower with spaces and no other non-alpha-num
				$str = preg_replace('/[\_\-\t\n\r\0\x0b\!\@\#\$\%\^\&\*\+\=\ ]+/', ' ', $str);
				$str = preg_replace('/[^a-zA-Z0-9\ ]*/', '', $str);
				$str = preg_replace('/([a-z])([A-Z0-9])([a-z])/', '$1 $2$3', $str);
				$str = trim(strtolower($str));
				$str = str_singular($str);
				$str_snake_case = preg_replace('/[\ ]+/', '_', $str);
				//DEFAULT VALUE FOR CODE
				$projectStatus->project_status_code = substr($str_snake_case, 0, 16) . "";
			}
			

			if($projectStatus->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.project-statuses.index')->with('message', 'Project Status has been created!');				
				} elseif ($this->is_api_url ) {
				    $data['error'] =false;
					$data['message'] = 'Project Status has been created!';
					$data['url'] = URL::route("api.((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '')project-statuses.index");							

					return json_encode($data);
				}	
				return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'project-statuses.index', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->with('message', 'Project Status has been created!');					
			}

		}
		$data['error']   = true;
		$data['message']  = 'Please fill required fields below!';
		$data['errors'] = $validator->messages();

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . ((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'project-statuses.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'project-statuses.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
	}
	

	/**
	 * ProjectStatus @ show
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id, $refId=0, $refSlug='')
	{
		$data = array();

		//ACCESS CONTROL 
		$projectStatus = ProjectStatus::where('project_status_id', $id)->authed( $this->user_id, $this->user_is_admin, 'show' )
		    
		   ->first();
	
		if (empty($projectStatus)){ return Redirect::route('project-statuses.index')->with('message', 'You are not logged in as the owner of this Project Status.'); }
			
		$data['projectStatus'] = $projectStatus;

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'project-statuses.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('project-statuses.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 *  ProjectStatus @ edit
	 *  Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$projectStatus = ProjectStatus::where('project_status_id', $id)
		    
		   ->first();

		if (empty($projectStatus)){ return Redirect::route('project-statuses.index')->with('message', 'You are not logged in as the owner of this Project Status.'); }
		
		$data['projectStatus'] = $projectStatus;

		//REFERENCED RESOURCE SELECT LIST

		//MTM OPTIONS
		//add parent IDs to where clause where appropriate

  		//SET SuccessURL if manually set in url
  		$data['successUrl'] = Input::get('successUrl', '');
  	
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'project-statuses.edit', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('project-statuses.edit', $data);
	}

	/**
	 * ProjectStatus @ update
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		$projectStatus = ProjectStatus::where('project_status_id', $id)
		    
		   ->first();
		
		if (empty($projectStatus)){ return Redirect::route('project-statuses.index')->with('message', 'You are not logged in as the owner of this Project Status.'); }
	
		$validator = Validator::make(Input::all(), ProjectStatus::$updateRules);

		if($validator->passes()) {
			$projectStatus->fill(Input::all());

			if($projectStatus->save()) {
			
				/**
				 * MTM UPDATES 
				 * for simple xrefs delete and insert
				 * for xrefs tables with data, toggle the is_active flag and insert if doesn't exist
				 *
				 **/ 


				//REDIRECT TO INDEX PAGE
				$success_url = Input::get('success_url', '');
								
				if ($this->is_admin_url ){
					if (!empty ($success_url)) { return Redirect::to($success_url); }
					return Redirect::route('admin.project-statuses.index')->with('messge', 'Project Status has been updated.');				
				} elseif ($this->is_api_url ) {
					$data = ['error' => false,
						'message' => 'Project Status has been updated!',
						'url' => URL::route("api.project-statuses.index")							
						];
					if (!empty ($success_url)) {$data['url'] = $success_url; }
					return json_encode($data);
				}
				if (!empty ($success_url)) { return Redirect::to($success_url); } 
				return Redirect::route('project-statuses.show', array($id))->with('message', 'Project Status has been updated!');
			}
		}

		//RETURN ERRORS
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route('admin.project-statuses.edit', $id)
				->withInput()
				->withErrors($validator)
				->with('message', 'There were validation errors.');			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}

		return Redirect::route('project-statuses.edit', $id)
			->withInput()
			->withErrors($validator)
			->with('message', 'There were validation errors.');

	}


	/**
	 * ProjectStatus @ destroy
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$projectStatus = ProjectStatus::where('project_status_id', $id)->first();
		if (empty($projectStatus)){ return Redirect::route('project-statuses.index')->with('message', 'You are not logged in as the owner of this Project Status.'); }
		
		  // MTM delete xref rows: 

		/** 
		 * // CHILDREN delete  Where child model = "Jr" 
		 * 
		 * 	$projectStatusJrs = ProjectStatusJr::where('project_status_id', $id)->get();
		 *	foreach ($projectStatusJrs as $child) {
		 *	  //recursivly delete decendants
		 *	  //$grandchildren = GrandChild::where('parent_id', $child->projectStatus_jr_id)->delete();
		 *
		 *	  $child->delete();
		 *  }
		 */

		$projectStatus->delete();
		
		//Delete with popup modal and json api response, because DELETE method only available via forms submital.  

		$data = ['error' => false,
				'message' => "Deleted Project Status(" . $id . ")"
			];
		if ($this->is_admin_url ){
			$data['url']= URL::route("admin.project-statuses.index");
		} elseif ($this->is_api_url ) {
			$data['url']= URL::route("api.project-statuses.index");
		} else {
			$data['url']= URL::route("project-statuses.index");
		}
		//return json string if the views use an ajax call to delete.
		return json_encode($data);
		//return Redirect::route($data['url'])->with('message', 'Project Status  deleted');
		
	}

	

 /**
 * OTM TABLE INDEX functions (excluding parent, owner - use Parent controller;  and type tables - not usually necessary )
 *
 */
 

}