<?php


/**
 * Parent-Child Controller for Startup Team Member 
 * Controls access by parentId to Index, Create, and Store Methods.  
 * All other methods are controlled by the standard controller. 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */
 
 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\TeamMember;

use App\Models\Startup;
use App\Models\TeamRole;
use App\Models\TeamRelationship;
use App\Models\ProjectTeamMember;
use App\Models\Project;
 
class StartupTeamMemberController extends Controller {	
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * Display a list and provide links for CRUD actions
	 * usage: GET RESOURCE
	 *
	 * @return Response
	 */
	public function index($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->first(); 
		$teamMembers = TeamMember::where('startup_id',  $startup->startup_id )
		   ->with('teamRole', 'teamRelationship', 'projectTeamMembers') 
		   ->orderBy('sort_order', 'asc')
		   ->get();

		$data['teamMembers']=array();
				
		//REFERENCED RESOURCE NAMES
		foreach ($teamMembers as $row)
		{		
			//get OTM names
			$row->startup_name = (( $row->startup) ? $row->startup->startup_name : '' );
						$row->teamRole_name = (( $row->teamRole) ? $row->teamRole->team_role_name : '' );
			$row->teamRelationship_name = (( $row->teamRelationship) ? $row->teamRelationship->team_relationship_name : '' );

			//get MTM names
			$project_list=$row->projectTeamMembers()->select('tbl_project.project_name')->take(5)->lists('project_name');
			$row->project_names =  $project_list->implode(', ');	
			
			
			$data['teamMembers'][]=$row;	
		} /* end foreach row */

		$data['parentName'] = $startup->startup_name;
		
		$data['startup'] = $startup;
			
		if ($this->is_admin_url ){
		return View::make(self::$admin_dir . '.' . 'team-members.index-for-startups', $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
 
		return view('team-members.index-for-startups', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin )->first(); 

		$data = array();
		$teamMember = new TeamMember; //MODEL for default values and form model binding
		
		
		//REFERENCED RESOURCE SELECT LIST
		$data['startupList'] = Startup::where('startup_id', $parentId)->lists('startup_name', 'startup_id');
		$data['teamRoleList'] = TeamRole::orderBy('sort_order', 'asc')->lists('team_role_name', 'team_role_id');
		$data['teamRelationshipList'] = TeamRelationship::orderBy('sort_order', 'asc')->lists('team_relationship_name', 'team_relationship_id');
		$data['parentName'] = $startup->startup_name;
		$data['startup'] = $startup;

				$teamMember->fill($startup->toArray()); //inherit as much as possible from the parent
		$data['teamMember'] = $teamMember; 
		
		if ($this->is_admin_url ){
		return view(self::$admin_dir . '.' . 'team-members.create-for-startups', $data);
 
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-members.create-for-startups', $data);

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin )->first(); 
	
		$validator = Validator::make(Input::all(), TeamMember::$insertRules);

		if($validator->passes()) {

			$teamMember = new TeamMember;
			$teamMember->fill(Input::all());
			
	
			//FILL GUARDED PARENT ID
			$teamMember->startup_id = $parentId;
	
			
			//SET DEFAULT VALUES ON CREATE
			if (! $teamMember->sort_order) $teamMember->sort_order = 20;

			
			if($teamMember->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.startups.team-members.index', $startup->startup_id )->with('data', 'Team Member has been created!');
			
				} elseif ($this->is_api_url ) {
				    $data = ['error' => false,
						'message' => 'Team Member has been created!',
						'url' => URL::route("api.startups.team-members.index", [$startup->startup_id])
								
						];
					return json_encode($data);
				}	
		return Redirect::route('startups.team-members.index', $startup->startup_id )->with('data', 'Team Member has been created!');
	
				
			}

		}
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . 'startups.team-members.create', [$startup->startup_id])->withInput(Input::all())->withErrors($validator);
				
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}		
		return Redirect::route('startups.team-members.create', [$startup->startup_id])->withInput(Input::all())->withErrors($validator);

	}

	/**
	 * Show by ParentId 
	 * redirects to the standard controller
	 *
	 */
	public function show($parentId, $id)
	{
		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin, 'show')->first(); 
		

		//$teamMember = $startup->startupTeamMembers()->where('team_member_id', $id)
		$teamMember = TeamMember::where('team_member_id', $id)->where('startup_id',  $startup->startup_id )
		   ->with('teamRole', 'teamRelationship', 'projectTeamMembers') 
		   ->first();
		
		if (empty($teamMember)){ return Redirect::route('team-members.index')->with('message', 'You are not logged in as the owner of this Team Member.'); }
		
		$data['teamMember'] = $teamMember;
		$data['startup'] = $startup;


		if ($this->is_admin_url ){ 
		return view(self::$admin_dir . '.' . 'team-members.view-for-startups', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-members.view-for-startups', $data);
	}

	
	/**
	 * Edit by ParentId 
	 * redirects to the standard controller
	 *
	 */
	public function edit($parentId, $id)
	{
		//REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.team-members.edit"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.team-members.edit', [$id] );
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route('team-members.edit', [$id]);	
	}

	/**
	 * Update the specified resource in storage by parentId
	 * Redirects to the standard controller
	 *
	 */
	public function update($parentId, $id)
	{
	    //REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.team-members.edit"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.team-members.edit', [$id]);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route('team-members.edit', [$id]);
	}

	/**
	 * Remove the specified resource from storage by parentId
	 * Redirects to the standard controller
	 *
	 */
	public function destroy($parentId, $id)
	{	
		//REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.team-members.destroy"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.team-members.destroy', [$id]);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		
		return Redirect::route('team-members.destroy', [$id]);
	}

	
	/**
	 * For MTM Relationships Display a list of related objects and provide links to CRUD actions
	 * 
	 *
	 * @return  Response
	 */
 	
	public function projectsIndex($parentId)
	{
		$teamMember = TeamMember::where('team_member_id', $parentId)->first();
		$projects = $teamMember->projectTeamMembers()
		  ->get();

		$data=array();
		$data['teamMember'] = $teamMember;
		$data['projects'] = $projects;
		
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'projects.index-for-team-members', $data);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
	}
	return view('projects.index-for-team-members', $data);
	}
	
	
}