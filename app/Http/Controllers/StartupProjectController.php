<?php


/**
 * Parent-Child Controller for Startup Project 
 * Controls access by parentId to Index, Create, and Store Methods.  
 * All other methods are controlled by the standard controller. 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */
 
 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\Project;

use App\Models\Startup;
use App\Models\ProjectStatus;
use App\Models\Assumption;
use App\Models\Milestone;
use App\Models\TeamMember;
use App\Models\ProjectTeamMember;
 
class StartupProjectController extends Controller {	
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * Display a list and provide links for CRUD actions
	 * usage: GET RESOURCE
	 *
	 * @return Response
	 */
	public function index($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->first(); 
		$projects = Project::where('startup_id',  $startup->startup_id )
		   ->with('projectStatus', 'assumption', 'milestone', 'responsibleTeamMember', 'projectTeamMembers') 
		   ->orderBy('sort_order', 'asc')
		   ->get();

		$data['projects']=array();
				
		//REFERENCED RESOURCE NAMES
		foreach ($projects as $row)
		{		
			//get OTM names
			$row->startup_name = (( $row->startup) ? $row->startup->startup_name : '' );
						$row->projectStatus_name = (( $row->projectStatus) ? $row->projectStatus->project_status_name : '' );
			$row->assumption_name = (( $row->assumption) ? $row->assumption->assumption_name : '' );
			$row->milestone_name = (( $row->milestone) ? $row->milestone->milestone_name : '' );
			$row->teamMember_name = (( $row->responsibleTeamMember) ? $row->responsibleTeamMember->team_member_name : '' );

			//get MTM names
			$team_member_list=$row->projectTeamMembers()->select('tbl_team_member.team_member_name')->take(5)->lists('team_member_name');
			$row->team_member_names =  $team_member_list->implode(', ');	
			
			
			$data['projects'][]=$row;	
		} /* end foreach row */

		$data['parentName'] = $startup->startup_name;
		
		$data['startup'] = $startup;
			
		if ($this->is_admin_url ){
		return View::make(self::$admin_dir . '.' . 'projects.index-for-startups', $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
 
		return view('projects.index-for-startups', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin )->first(); 

		$data = array();
		$project = new Project; //MODEL for default values and form model binding
		
		
		//REFERENCED RESOURCE SELECT LIST
		$data['startupList'] = Startup::where('startup_id', $parentId)->lists('startup_name', 'startup_id');
		$data['projectStatusList'] = ProjectStatus::orderBy('sort_order', 'asc')->lists('project_status_name', 'project_status_id');
		$data['assumptionList'] = Assumption::orderBy('sort_order', 'asc')->lists('assumption_name', 'assumption_id');
		$data['milestoneList'] = Milestone::orderBy('sort_order', 'asc')->lists('milestone_name', 'milestone_id');
		$data['teamMemberList'] = TeamMember::orderBy('sort_order', 'asc')->lists('team_member_name', 'team_member_id');
		$data['parentName'] = $startup->startup_name;
		$data['startup'] = $startup;

				$project->fill($startup->toArray()); //inherit as much as possible from the parent
		$data['project'] = $project; 
		
		if ($this->is_admin_url ){
		return view(self::$admin_dir . '.' . 'projects.create-for-startups', $data);
 
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('projects.create-for-startups', $data);

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin )->first(); 
	
		$validator = Validator::make(Input::all(), Project::$insertRules);

		if($validator->passes()) {

			$project = new Project;
			$project->fill(Input::all());
			
	
			//FILL GUARDED PARENT ID
			$project->startup_id = $parentId;
	
			
			//SET DEFAULT VALUES ON CREATE
			if (! $project->sort_order) $project->sort_order = 20;

			
			if($project->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.startups.projects.index', $startup->startup_id )->with('data', 'Project has been created!');
			
				} elseif ($this->is_api_url ) {
				    $data = ['error' => false,
						'message' => 'Project has been created!',
						'url' => URL::route("api.startups.projects.index", [$startup->startup_id])
								
						];
					return json_encode($data);
				}	
		return Redirect::route('startups.projects.index', $startup->startup_id )->with('data', 'Project has been created!');
	
				
			}

		}
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . 'startups.projects.create', [$startup->startup_id])->withInput(Input::all())->withErrors($validator);
				
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}		
		return Redirect::route('startups.projects.create', [$startup->startup_id])->withInput(Input::all())->withErrors($validator);

	}

	/**
	 * Show by ParentId 
	 * redirects to the standard controller
	 *
	 */
	public function show($parentId, $id)
	{
		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin, 'show')->first(); 
		

		//$project = $startup->startupProjects()->where('project_id', $id)
		$project = Project::where('project_id', $id)->where('startup_id',  $startup->startup_id )
		   ->with('projectStatus', 'assumption', 'milestone', 'responsibleTeamMember', 'projectTeamMembers') 
		   ->first();
		
		if (empty($project)){ return Redirect::route('projects.index')->with('message', 'You are not logged in as the owner of this Project.'); }
		
		$data['project'] = $project;
		$data['startup'] = $startup;


		if ($this->is_admin_url ){ 
		return view(self::$admin_dir . '.' . 'projects.view-for-startups', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('projects.view-for-startups', $data);
	}

	
	/**
	 * Edit by ParentId 
	 * redirects to the standard controller
	 *
	 */
	public function edit($parentId, $id)
	{
		//REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.projects.edit"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.projects.edit', [$id] );
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route('projects.edit', [$id]);	
	}

	/**
	 * Update the specified resource in storage by parentId
	 * Redirects to the standard controller
	 *
	 */
	public function update($parentId, $id)
	{
	    //REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.projects.edit"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.projects.edit', [$id]);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route('projects.edit', [$id]);
	}

	/**
	 * Remove the specified resource from storage by parentId
	 * Redirects to the standard controller
	 *
	 */
	public function destroy($parentId, $id)
	{	
		//REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.projects.destroy"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.projects.destroy', [$id]);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		
		return Redirect::route('projects.destroy', [$id]);
	}

	
	/**
	 * For MTM Relationships Display a list of related objects and provide links to CRUD actions
	 * 
	 *
	 * @return  Response
	 */
 	
	public function teamMembersIndex($parentId)
	{
		$project = Project::where('project_id', $parentId)->first();
		$teamMembers = $project->projectTeamMembers()
		  ->get();

		$data=array();
		$data['project'] = $project;
		$data['teamMembers'] = $teamMembers;
		
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-members.index-for-projects', $data);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
	}
	return view('team-members.index-for-projects', $data);
	}
	
	
}