<?php

/** 
 * Controller for Team Member 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\TeamMember;

use App\Models\Startup;
use App\Models\TeamRole;
use App\Models\TeamRelationship;
use App\Models\ProjectTeamMember;
use App\Models\Project;

class TeamMemberController extends Controller {
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * TeamMember @ index
	 * Display a list of TeamMembers and links for CRUD actions
	 *
	 * @param int $parentId (optional, Used by helper method for filtering by a referenced table. )
	 * @param string $parentSlug (optional, Used by helper method for filtering by a referenced table.)
	 * @return Response
	 */
	public function index($refId=0, $refSlug='')
	{
		$teamMembers = TeamMember::authed( $this->user_id, $this->user_is_admin, 'view' )
		   ->with('startup', 'teamRole', 'teamRelationship', 'projectTeamMembers') 
		   ->orderBy('sort_order', 'asc') 
		   ->get();
		$data['teamMembers']=array();
			
		//REFERENCED RESOURCE NAMES
		foreach ($teamMembers as $row)
		{				
			//get OTM names
			$row->startup_name = (( $row->startup) ? $row->startup->startup_name : '' );
						$row->teamRole_name = (( $row->teamRole) ? $row->teamRole->team_role_name : '' );
			$row->teamRelationship_name = (( $row->teamRelationship) ? $row->teamRelationship->team_relationship_name : '' );


			//get MTM names
			$project_list=$row->projectTeamMembers()->with('projects')->take(5)->lists('project_name');
			$row->project_names =  $project_list->implode(', ');	
			$data['teamMembers'][]=$row;	
		} /* end foreach row */
	

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-members.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
		return view('team-members.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 * TeamMember @ create
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($refId=0, $refSlug='')
	{
		$data = array();
		$teamMember = new TeamMember; //MODEL for default values and form model binding
		
		//REFERENCED RESOURCE SELECT LIST
			
			$data['startupList'] = Startup::orderBy('sort_order', 'asc')->lists('startup_name', 'startup_id');
			
			$data['teamRoleList'] = TeamRole::orderBy('sort_order', 'asc')->lists('team_role_name', 'team_role_id');
			
			$data['teamRelationshipList'] = TeamRelationship::orderBy('sort_order', 'asc')->lists('team_relationship_name', 'team_relationship_id');
				
		
		
		$data['teamMember'] = $teamMember; 

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-members.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-members.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);	
	}


	/**
	 * TeamMember @ store
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($refId=0, $refSlug='')
	{

		$validator = Validator::make(Input::all(), TeamMember::$insertRules);

		if($validator->passes()) {
				$teamMember = new TeamMember;
				$teamMember->fill(Input::all());
			
			
			//FILL GUARDED PARENT ID
			if(($this->user_is_admin) && (Input::has('startup_id'))){ $teamMember->startup_id= Input::get('startup_id'); } 
			//$teamMember->startup_id = $parentId;
					
			//SET DEFAULT VALUES ON CREATE
			if (! $teamMember->sort_order) $teamMember->sort_order = 20;
			

			if($teamMember->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.team-members.index')->with('message', 'Team Member has been created!');				
				} elseif ($this->is_api_url ) {
				    $data['error'] =false;
					$data['message'] = 'Team Member has been created!';
					$data['url'] = URL::route("api.((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '')team-members.index");							

					return json_encode($data);
				}	
				return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'team-members.index', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->with('message', 'Team Member has been created!');					
			}

		}
		$data['error']   = true;
		$data['message']  = 'Please fill required fields below!';
		$data['errors'] = $validator->messages();

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . ((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'team-members.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'team-members.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
	}
	

	/**
	 * TeamMember @ show
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id, $refId=0, $refSlug='')
	{
		$data = array();

		//ACCESS CONTROL 
		$teamMember = TeamMember::where('team_member_id', $id)->authed( $this->user_id, $this->user_is_admin, 'show' )
		   ->with('startup', 'teamRole', 'teamRelationship', 'projectTeamMembers') 
		   ->first();
	
		if (empty($teamMember)){ return Redirect::route('team-members.index')->with('message', 'You are not logged in as the owner of this Team Member.'); }
			
		$data['teamMember'] = $teamMember;

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-members.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-members.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 *  TeamMember @ edit
	 *  Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$teamMember = TeamMember::where('team_member_id', $id)
		   ->with('startup', 'teamRole', 'teamRelationship', 'projectTeamMembers') 
		   ->first();

		if (empty($teamMember)){ return Redirect::route('team-members.index')->with('message', 'You are not logged in as the owner of this Team Member.'); }
		
		$data['teamMember'] = $teamMember;

		//REFERENCED RESOURCE SELECT LIST
		$data['startupList'] = Startup::where('startup_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('startup_name', 'startup_id');
		$data['teamRoleList'] = TeamRole::where('team_role_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('team_role_name', 'team_role_id');
		$data['teamRelationshipList'] = TeamRelationship::where('team_relationship_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('team_relationship_name', 'team_relationship_id');

		//MTM OPTIONS
		//add parent IDs to where clause where appropriate
		//projects 
		$data['projects_selected'] = $teamMember->projectTeamMembers()->pluck('xref_project_team_member.project_id')->toArray();
 		$data['projects_selected_count'] = count($data['projects_selected']);
		$data['project_options'] = Project::whereIn('startup_id',[0, $teamMember->startup_id])->orderBy('sort_order' , 'ASC')->lists('project_name', 'project_id');
					
  		//SET SuccessURL if manually set in url
  		$data['successUrl'] = Input::get('successUrl', '');
  	
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-members.edit', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-members.edit', $data);
	}

	/**
	 * TeamMember @ update
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		$teamMember = TeamMember::where('team_member_id', $id)
		   ->with('startup', 'teamRole', 'teamRelationship', 'projectTeamMembers') 
		   ->first();
		
		if (empty($teamMember)){ return Redirect::route('team-members.index')->with('message', 'You are not logged in as the owner of this Team Member.'); }
	
		$validator = Validator::make(Input::all(), TeamMember::$updateRules);

		if($validator->passes()) {
			$teamMember->fill(Input::all());

			//FILL GUARDED PARENT ID
			if(($this->user_is_admin) && (Input::has('startup_id'))){ $teamMember->startup_id = Input::get('startup_id'); }
			if($teamMember->save()) {
			
				/**
				 * MTM UPDATES 
				 * for simple xrefs delete and insert
				 * for xrefs tables with data, toggle the is_active flag and insert if doesn't exist
				 *
				 **/ 


	//UPDATE XREF DATA TABLE - projects 
				if(  Input::has('projects_selected_count') ) {
					$projects=array();
				 	//DELETE DATA IN simple xref table
				 		ProjectTeamMember::where('team_member_id', '=', $teamMember->team_member_id)->delete();
				 	//INSERT DATA to xref table
				 		$projects_selected_input = Input::get('projects_selected', array());
				 		if (is_string($projects_selected_input)) {
				 			$projects_selected_input = explode(',', $projects_selected_input);
				  		}
				  		foreach ($projects_selected_input as $that_xref_id) {
				 			$projects[] = array('team_member_id' => $teamMember->team_member_id, 'project_id' => $that_xref_id);
				  		}
				  	//return json_encode($projects);
				  	ProjectTeamMember::insert($projects);
				}

				//REDIRECT TO INDEX PAGE
				$success_url = Input::get('success_url', '');
								
				if ($this->is_admin_url ){
					if (!empty ($success_url)) { return Redirect::to($success_url); }
					return Redirect::route('admin.team-members.index')->with('messge', 'Team Member has been updated.');				
				} elseif ($this->is_api_url ) {
					$data = ['error' => false,
						'message' => 'Team Member has been updated!',
						'url' => URL::route("api.team-members.index")							
						];
					if (!empty ($success_url)) {$data['url'] = $success_url; }
					return json_encode($data);
				}
				if (!empty ($success_url)) { return Redirect::to($success_url); } 
				return Redirect::route('team-members.show', array($id))->with('message', 'Team Member has been updated!');
			}
		}

		//RETURN ERRORS
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route('admin.team-members.edit', $id)
				->withInput()
				->withErrors($validator)
				->with('message', 'There were validation errors.');			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}

		return Redirect::route('team-members.edit', $id)
			->withInput()
			->withErrors($validator)
			->with('message', 'There were validation errors.');

	}


	/**
	 * TeamMember @ destroy
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$teamMember = TeamMember::where('team_member_id', $id)->first();
		if (empty($teamMember)){ return Redirect::route('team-members.index')->with('message', 'You are not logged in as the owner of this Team Member.'); }
		
		  // MTM delete xref rows: 
			ProjectTeamMember::where('team_member_id', '=', $teamMember->team_member_id)->delete();

		/** 
		 * // CHILDREN delete  Where child model = "Jr" 
		 * 
		 * 	$teamMemberJrs = TeamMemberJr::where('team_member_id', $id)->get();
		 *	foreach ($teamMemberJrs as $child) {
		 *	  //recursivly delete decendants
		 *	  //$grandchildren = GrandChild::where('parent_id', $child->teamMember_jr_id)->delete();
		 *
		 *	  $child->delete();
		 *  }
		 */

		$teamMember->delete();
		
		//Delete with popup modal and json api response, because DELETE method only available via forms submital.  

		$data = ['error' => false,
				'message' => "Deleted Team Member(" . $id . ")"
			];
		if ($this->is_admin_url ){
			$data['url']= URL::route("admin.team-members.index");
		} elseif ($this->is_api_url ) {
			$data['url']= URL::route("api.team-members.index");
		} else {
			$data['url']= URL::route("team-members.index");
		}
		//return json string if the views use an ajax call to delete.
		return json_encode($data);
		//return Redirect::route($data['url'])->with('message', 'Team Member  deleted');
		
	}

	
/**
 * MTM Relationships INDEX and SHOW functions
 * 
 *
 */
	
	public function projectsIndex($parentId)
	{
		$teamMember = TeamMember::where('team_member_id', $parentId)->authed( $this->user_id, $this->user_is_admin, 'show' )->first();

		$projects = $teamMember->projectTeamMembers()
			->get();

		$data=array();
		$data['teamMember'] = $teamMember;
		$data['projects'] = $projects;
		
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'projects.index-for-team-members', $data);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
	}
	return view('projects.index-for-team-members', $data);
	}

	public function projectsShow($parentId, $id)
	{
		//ACCESS CONTROL
		$teamMember = TeamMember::where('team_member_id', $parentId)->authed( $this->user_id, $this->user_is_admin, 'show')->first(); 
		
		//$project = Project::where('project_id', $id)->where('team_member_id',  $teamMember->team_member_id )->first();
		$project= $teamMember->projectTeamMembers()->where('tbl_project.project_id', $id)->first();
		
		if (empty($project)){ return Redirect::back()->with('message', 'You are not authorized to see this record for Project.'); }
		
		$data['teamMember'] = $teamMember;
		$data['project'] = $project;
		

		if ($this->is_admin_url ){ 
		return view(self::$admin_dir . '.' . 'projects.view-for-team-members', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('projects.view-for-team-members', $data);
	}
	

 /**
 * OTM TABLE INDEX functions (excluding parent, owner - use Parent controller;  and type tables - not usually necessary )
 *
 */
 

}