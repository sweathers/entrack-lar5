<?php


/**
 * Parent-Child Controller for Startup Startup Milestone 
 * Controls access by parentId to Index, Create, and Store Methods.  
 * All other methods are controlled by the standard controller. 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */
 
 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\StartupMilestone;

use App\Models\Startup;
use App\Models\Milestone;
 
class StartupStartupMilestoneController extends Controller {	
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * Display a list and provide links for CRUD actions
	 * usage: GET RESOURCE
	 *
	 * @return Response
	 */
	public function index($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->first(); 
		$startupMilestones = StartupMilestone::where('startup_id',  $startup->startup_id )
		   ->where( 'is_active', '>', 0 ) 
		   ->with('milestone') 
		   ->get();

		$data['startupMilestones']=array();
				
		//REFERENCED RESOURCE NAMES
		foreach ($startupMilestones as $row)
		{		
			//get OTM names
			$row->startup_name = (( $row->startup) ? $row->startup->startup_name : '' );
						$row->milestone_name = (( $row->milestone) ? $row->milestone->milestone_name : '' );
					

			//get MTM names
			
			
			$data['startupMilestones'][]=$row;	
		} /* end foreach row */

		$data['parentName'] = $startup->startup_name;
		
		$data['startup'] = $startup;
			
		if ($this->is_admin_url ){
		return View::make(self::$admin_dir . '.' . 'startup-milestones.index-for-startups', $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
 
		return view('startup-milestones.index-for-startups', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin )->first(); 

		$data = array();
		$startupMilestone = new StartupMilestone; //MODEL for default values and form model binding
		
		
		//REFERENCED RESOURCE SELECT LIST
		$data['startupList'] = Startup::orderBy('sort_order', 'asc')->lists('startup_name', 'startup_id');
		$data['milestoneList'] = Milestone::orderBy('sort_order', 'asc')->lists('milestone_name', 'milestone_id');
		$data['parentName'] = $startup->startup_name;
		$data['startup'] = $startup;

				$startupMilestone->fill($startup->toArray()); //inherit as much as possible from the parent
		$data['startupMilestone'] = $startupMilestone; 
		
		if ($this->is_admin_url ){
		return view(self::$admin_dir . '.' . 'startup-milestones.create-for-startups', $data);
 
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('startup-milestones.create-for-startups', $data);

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin )->first(); 
	
		$validator = Validator::make(Input::all(), StartupMilestone::$insertRules);

		if($validator->passes()) {

			$startupMilestone = new StartupMilestone;
			$startupMilestone->fill(Input::all());
			
			//SET GUARDED XREF ID
			$startupMilestone->startup_id = $startup->startup_id;
	
			
			//SET DEFAULT VALUES ON CREATE
			if (! $startupMilestone->is_active) $startupMilestone->is_active = 1;

			
			if($startupMilestone->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.startups.startup-milestones.index', $startup->startup_id )->with('data', 'Startup Milestone has been created!');
			
				} elseif ($this->is_api_url ) {
				    $data = ['error' => false,
						'message' => 'Startup Milestone has been created!',
						'url' => URL::route("api.startups.startup-milestones.index", [$startup->startup_id])
								
						];
					return json_encode($data);
				}	
		return Redirect::route('startups.startup-milestones.index', $startup->startup_id )->with('data', 'Startup Milestone has been created!');
	
				
			}

		}
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . 'startups.startup-milestones.create', [$startup->startup_id])->withInput(Input::all())->withErrors($validator);
				
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}		
		return Redirect::route('startups.startup-milestones.create', [$startup->startup_id])->withInput(Input::all())->withErrors($validator);

	}

	/**
	 * Show by ParentId 
	 * redirects to the standard controller
	 *
	 */
	public function show($parentId, $id)
	{
		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin, 'show')->first(); 
		

		//$startupMilestone = $startup->startupStartupMilestones()->where('xref_s_m_id', $id)
		$startupMilestone = StartupMilestone::where('xref_s_m_id', $id)->where('startup_id',  $startup->startup_id )
		   ->with('milestone') 
		   ->first();
		
		if (empty($startupMilestone)){ return Redirect::route('startup-milestones.index')->with('message', 'You are not logged in as the owner of this Startup Milestone.'); }
		
		$data['startupMilestone'] = $startupMilestone;
		$data['startup'] = $startup;

		//REFERENCED OBJECT  
		$data['milestone'] = Milestone::where('milestone_id', $startupMilestone->milestone_id)->first();

		if ($this->is_admin_url ){ 
		return view(self::$admin_dir . '.' . 'startup-milestones.view-for-startups', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('startup-milestones.view-for-startups', $data);
	}

	
	/**
	 * Edit by ParentId 
	 * redirects to the standard controller
	 *
	 */
	public function edit($parentId, $id)
	{
		//REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.startup-milestones.edit"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.startup-milestones.edit', [$id] );
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route('startup-milestones.edit', [$id]);	
	}

	/**
	 * Update the specified resource in storage by parentId
	 * Redirects to the standard controller
	 *
	 */
	public function update($parentId, $id)
	{
	    //REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.startup-milestones.edit"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.startup-milestones.edit', [$id]);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route('startup-milestones.edit', [$id]);
	}

	/**
	 * Remove the specified resource from storage by parentId
	 * Redirects to the standard controller
	 *
	 */
	public function destroy($parentId, $id)
	{	
		//REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.startup-milestones.destroy"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.startup-milestones.destroy', [$id]);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		
		return Redirect::route('startup-milestones.destroy', [$id]);
	}

	
	
}