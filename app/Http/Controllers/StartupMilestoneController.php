<?php

/** 
 * Controller for Startup Milestone 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\StartupMilestone;

use App\Models\Startup;
use App\Models\Milestone;

class StartupMilestoneController extends Controller {
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * StartupMilestone @ index
	 * Display a list of StartupMilestones and links for CRUD actions
	 *
	 * @param int $parentId (optional, Used by helper method for filtering by a referenced table. )
	 * @param string $parentSlug (optional, Used by helper method for filtering by a referenced table.)
	 * @return Response
	 */
	public function index($refId=0, $refSlug='')
	{
		$startupMilestones = StartupMilestone::authed( $this->user_id, $this->user_is_admin, 'view' )
		   ->with('startup', 'milestone') 
		    
		   ->get();
		$data['startupMilestones']=array();
			
		//REFERENCED RESOURCE NAMES
		foreach ($startupMilestones as $row)
		{				
			//get OTM names
			$row->startup_name = (( $row->startup) ? $row->startup->startup_name : '' );
						$row->milestone_name = (( $row->milestone) ? $row->milestone->milestone_name : '' );
			

			//get MTM names
			$data['startupMilestones'][]=$row;	
		} /* end foreach row */
	

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'startup-milestones.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
		return view('startup-milestones.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 * StartupMilestone @ create
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($refId=0, $refSlug='')
	{
		$data = array();
		$startupMilestone = new StartupMilestone; //MODEL for default values and form model binding
		
		//REFERENCED RESOURCE SELECT LIST
			
			$data['startupList'] = Startup::orderBy('sort_order', 'asc')->lists('startup_name', 'startup_id');
			
			$data['milestoneList'] = Milestone::orderBy('sort_order', 'asc')->lists('milestone_name', 'milestone_id');
				
		
		
		$data['startupMilestone'] = $startupMilestone; 

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'startup-milestones.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('startup-milestones.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);	
	}


	/**
	 * StartupMilestone @ store
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($refId=0, $refSlug='')
	{

		$validator = Validator::make(Input::all(), StartupMilestone::$insertRules);

		if($validator->passes()) {
				$startupMilestone = new StartupMilestone;
				$startupMilestone->fill(Input::all());
			
					
			//SET DEFAULT VALUES ON CREATE
			if (! $startupMilestone->is_active) $startupMilestone->is_active = 1;
			

			if($startupMilestone->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.startup-milestones.index')->with('message', 'Startup Milestone has been created!');				
				} elseif ($this->is_api_url ) {
				    $data['error'] =false;
					$data['message'] = 'Startup Milestone has been created!';
					$data['url'] = URL::route("api.((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '')startup-milestones.index");							

					return json_encode($data);
				}	
				return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'startup-milestones.index', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->with('message', 'Startup Milestone has been created!');					
			}

		}
		$data['error']   = true;
		$data['message']  = 'Please fill required fields below!';
		$data['errors'] = $validator->messages();

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . ((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'startup-milestones.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'startup-milestones.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
	}
	

	/**
	 * StartupMilestone @ show
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id, $refId=0, $refSlug='')
	{
		$data = array();

		//ACCESS CONTROL 
		$startupMilestone = StartupMilestone::where('xref_s_m_id', $id)->authed( $this->user_id, $this->user_is_admin, 'show' )
		   ->with('startup', 'milestone') 
		   ->first();
	
		if (empty($startupMilestone)){ return Redirect::route('startup-milestones.index')->with('message', 'You are not logged in as the owner of this Startup Milestone.'); }
			
		$data['startupMilestone'] = $startupMilestone;

		//REFERENCED OBJECTS (from xref controller)  		
		$data['milestone'] = Milestone::where('milestone_id', $startupMilestone->milestone_id)->first();	
		$data['startup'] = Startup::where('startup_id', $startupMilestone->startup_id)->first();		
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'startup-milestones.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('startup-milestones.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 *  StartupMilestone @ edit
	 *  Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$startupMilestone = StartupMilestone::where('xref_s_m_id', $id)
		   ->with('startup', 'milestone') 
		   ->first();

		if (empty($startupMilestone)){ return Redirect::route('startup-milestones.index')->with('message', 'You are not logged in as the owner of this Startup Milestone.'); }
		
		$data['startupMilestone'] = $startupMilestone;

		//REFERENCED RESOURCE SELECT LIST
		$data['startupList'] = Startup::where('startup_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('startup_name', 'startup_id');
		$data['milestoneList'] = Milestone::where('milestone_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('milestone_name', 'milestone_id');

		//MTM OPTIONS
		//add parent IDs to where clause where appropriate

  		//SET SuccessURL if manually set in url
  		$data['successUrl'] = Input::get('successUrl', '');
  	
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'startup-milestones.edit', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('startup-milestones.edit', $data);
	}

	/**
	 * StartupMilestone @ update
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		$startupMilestone = StartupMilestone::where('xref_s_m_id', $id)
		   ->with('startup', 'milestone') 
		   ->first();
		
		if (empty($startupMilestone)){ return Redirect::route('startup-milestones.index')->with('message', 'You are not logged in as the owner of this Startup Milestone.'); }
	
		$validator = Validator::make(Input::all(), StartupMilestone::$updateRules);

		if($validator->passes()) {
			$startupMilestone->fill(Input::all());

			if($startupMilestone->save()) {
			
				/**
				 * MTM UPDATES 
				 * for simple xrefs delete and insert
				 * for xrefs tables with data, toggle the is_active flag and insert if doesn't exist
				 *
				 **/ 


				//REDIRECT TO INDEX PAGE
				$success_url = Input::get('success_url', '');
								
				if ($this->is_admin_url ){
					if (!empty ($success_url)) { return Redirect::to($success_url); }
					return Redirect::route('admin.startup-milestones.index')->with('messge', 'Startup Milestone has been updated.');				
				} elseif ($this->is_api_url ) {
					$data = ['error' => false,
						'message' => 'Startup Milestone has been updated!',
						'url' => URL::route("api.startup-milestones.index")							
						];
					if (!empty ($success_url)) {$data['url'] = $success_url; }
					return json_encode($data);
				}
				if (!empty ($success_url)) { return Redirect::to($success_url); } 
				return Redirect::route('startup-milestones.show', array($id))->with('message', 'Startup Milestone has been updated!');
			}
		}

		//RETURN ERRORS
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route('admin.startup-milestones.edit', $id)
				->withInput()
				->withErrors($validator)
				->with('message', 'There were validation errors.');			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}

		return Redirect::route('startup-milestones.edit', $id)
			->withInput()
			->withErrors($validator)
			->with('message', 'There were validation errors.');

	}


	/**
	 * StartupMilestone @ destroy
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$startupMilestone = StartupMilestone::where('xref_s_m_id', $id)->first();
		if (empty($startupMilestone)){ return Redirect::route('startup-milestones.index')->with('message', 'You are not logged in as the owner of this Startup Milestone.'); }
		
		  // MTM delete xref rows: 

		/** 
		 * // CHILDREN delete  Where child model = "Jr" 
		 * 
		 * 	$startupMilestoneJrs = StartupMilestoneJr::where('xref_s_m_id', $id)->get();
		 *	foreach ($startupMilestoneJrs as $child) {
		 *	  //recursivly delete decendants
		 *	  //$grandchildren = GrandChild::where('parent_id', $child->startupMilestone_jr_id)->delete();
		 *
		 *	  $child->delete();
		 *  }
		 */

		$startupMilestone->delete();
		
		//Delete with popup modal and json api response, because DELETE method only available via forms submital.  

		$data = ['error' => false,
				'message' => "Deleted Startup Milestone(" . $id . ")"
			];
		if ($this->is_admin_url ){
			$data['url']= URL::route("admin.startup-milestones.index");
		} elseif ($this->is_api_url ) {
			$data['url']= URL::route("api.startup-milestones.index");
		} else {
			$data['url']= URL::route("startup-milestones.index");
		}
		//return json string if the views use an ajax call to delete.
		return json_encode($data);
		//return Redirect::route($data['url'])->with('message', 'Startup Milestone  deleted');
		
	}

	

 /**
 * OTM TABLE INDEX functions (excluding parent, owner - use Parent controller;  and type tables - not usually necessary )
 *
 */
 

}