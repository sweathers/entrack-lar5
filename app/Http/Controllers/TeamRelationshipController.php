<?php

/** 
 * Controller for Team Relationship 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\TeamRelationship;


class TeamRelationshipController extends Controller {
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * TeamRelationship @ index
	 * Display a list of TeamRelationships and links for CRUD actions
	 *
	 * @param int $parentId (optional, Used by helper method for filtering by a referenced table. )
	 * @param string $parentSlug (optional, Used by helper method for filtering by a referenced table.)
	 * @return Response
	 */
	public function index($refId=0, $refSlug='')
	{
		$teamRelationships = TeamRelationship::authed( $this->user_id, $this->user_is_admin, 'view' )
		    
		   ->orderBy('sort_order', 'asc') 
		   ->get();
		$data['teamRelationships']=array();
			
		//REFERENCED RESOURCE NAMES
		foreach ($teamRelationships as $row)
		{				
			//get OTM names


			//get MTM names
			$data['teamRelationships'][]=$row;	
		} /* end foreach row */
	

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-relationships.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
		return view('team-relationships.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 * TeamRelationship @ create
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($refId=0, $refSlug='')
	{
		$data = array();
		$teamRelationship = new TeamRelationship; //MODEL for default values and form model binding
		
		//REFERENCED RESOURCE SELECT LIST
				
		
		
		$data['teamRelationship'] = $teamRelationship; 

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-relationships.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-relationships.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);	
	}


	/**
	 * TeamRelationship @ store
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($refId=0, $refSlug='')
	{

		$validator = Validator::make(Input::all(), TeamRelationship::$insertRules);

		if($validator->passes()) {
				$teamRelationship = new TeamRelationship;
				$teamRelationship->fill(Input::all());
			
					
			//SET DEFAULT VALUES ON CREATE
			if (! $teamRelationship->sort_order) $teamRelationship->sort_order = 20;
						
			//SET CODE VALUE FROM NAME
			if (! $teamRelationship->relationship_code) {
				$str=$teamRelationship->team_relationship_name;
				//sanitize the string to all lower with spaces and no other non-alpha-num
				$str = preg_replace('/[\_\-\t\n\r\0\x0b\!\@\#\$\%\^\&\*\+\=\ ]+/', ' ', $str);
				$str = preg_replace('/[^a-zA-Z0-9\ ]*/', '', $str);
				$str = preg_replace('/([a-z])([A-Z0-9])([a-z])/', '$1 $2$3', $str);
				$str = trim(strtolower($str));
				$str = str_singular($str);
				$str_snake_case = preg_replace('/[\ ]+/', '_', $str);
				//DEFAULT VALUE FOR CODE
				$teamRelationship->relationship_code = substr($str_snake_case, 0, 16) . "";
			}
			

			if($teamRelationship->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.team-relationships.index')->with('message', 'Team Relationship has been created!');				
				} elseif ($this->is_api_url ) {
				    $data['error'] =false;
					$data['message'] = 'Team Relationship has been created!';
					$data['url'] = URL::route("api.((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '')team-relationships.index");							

					return json_encode($data);
				}	
				return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'team-relationships.index', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->with('message', 'Team Relationship has been created!');					
			}

		}
		$data['error']   = true;
		$data['message']  = 'Please fill required fields below!';
		$data['errors'] = $validator->messages();

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . ((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'team-relationships.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'team-relationships.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
	}
	

	/**
	 * TeamRelationship @ show
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id, $refId=0, $refSlug='')
	{
		$data = array();

		//ACCESS CONTROL 
		$teamRelationship = TeamRelationship::where('team_relationship_id', $id)->authed( $this->user_id, $this->user_is_admin, 'show' )
		    
		   ->first();
	
		if (empty($teamRelationship)){ return Redirect::route('team-relationships.index')->with('message', 'You are not logged in as the owner of this Team Relationship.'); }
			
		$data['teamRelationship'] = $teamRelationship;

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-relationships.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-relationships.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 *  TeamRelationship @ edit
	 *  Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$teamRelationship = TeamRelationship::where('team_relationship_id', $id)
		    
		   ->first();

		if (empty($teamRelationship)){ return Redirect::route('team-relationships.index')->with('message', 'You are not logged in as the owner of this Team Relationship.'); }
		
		$data['teamRelationship'] = $teamRelationship;

		//REFERENCED RESOURCE SELECT LIST

		//MTM OPTIONS
		//add parent IDs to where clause where appropriate

  		//SET SuccessURL if manually set in url
  		$data['successUrl'] = Input::get('successUrl', '');
  	
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-relationships.edit', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-relationships.edit', $data);
	}

	/**
	 * TeamRelationship @ update
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		$teamRelationship = TeamRelationship::where('team_relationship_id', $id)
		    
		   ->first();
		
		if (empty($teamRelationship)){ return Redirect::route('team-relationships.index')->with('message', 'You are not logged in as the owner of this Team Relationship.'); }
	
		$validator = Validator::make(Input::all(), TeamRelationship::$updateRules);

		if($validator->passes()) {
			$teamRelationship->fill(Input::all());

			if($teamRelationship->save()) {
			
				/**
				 * MTM UPDATES 
				 * for simple xrefs delete and insert
				 * for xrefs tables with data, toggle the is_active flag and insert if doesn't exist
				 *
				 **/ 


				//REDIRECT TO INDEX PAGE
				$success_url = Input::get('success_url', '');
								
				if ($this->is_admin_url ){
					if (!empty ($success_url)) { return Redirect::to($success_url); }
					return Redirect::route('admin.team-relationships.index')->with('messge', 'Team Relationship has been updated.');				
				} elseif ($this->is_api_url ) {
					$data = ['error' => false,
						'message' => 'Team Relationship has been updated!',
						'url' => URL::route("api.team-relationships.index")							
						];
					if (!empty ($success_url)) {$data['url'] = $success_url; }
					return json_encode($data);
				}
				if (!empty ($success_url)) { return Redirect::to($success_url); } 
				return Redirect::route('team-relationships.show', array($id))->with('message', 'Team Relationship has been updated!');
			}
		}

		//RETURN ERRORS
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route('admin.team-relationships.edit', $id)
				->withInput()
				->withErrors($validator)
				->with('message', 'There were validation errors.');			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}

		return Redirect::route('team-relationships.edit', $id)
			->withInput()
			->withErrors($validator)
			->with('message', 'There were validation errors.');

	}


	/**
	 * TeamRelationship @ destroy
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$teamRelationship = TeamRelationship::where('team_relationship_id', $id)->first();
		if (empty($teamRelationship)){ return Redirect::route('team-relationships.index')->with('message', 'You are not logged in as the owner of this Team Relationship.'); }
		
		  // MTM delete xref rows: 

		/** 
		 * // CHILDREN delete  Where child model = "Jr" 
		 * 
		 * 	$teamRelationshipJrs = TeamRelationshipJr::where('team_relationship_id', $id)->get();
		 *	foreach ($teamRelationshipJrs as $child) {
		 *	  //recursivly delete decendants
		 *	  //$grandchildren = GrandChild::where('parent_id', $child->teamRelationship_jr_id)->delete();
		 *
		 *	  $child->delete();
		 *  }
		 */

		$teamRelationship->delete();
		
		//Delete with popup modal and json api response, because DELETE method only available via forms submital.  

		$data = ['error' => false,
				'message' => "Deleted Team Relationship(" . $id . ")"
			];
		if ($this->is_admin_url ){
			$data['url']= URL::route("admin.team-relationships.index");
		} elseif ($this->is_api_url ) {
			$data['url']= URL::route("api.team-relationships.index");
		} else {
			$data['url']= URL::route("team-relationships.index");
		}
		//return json string if the views use an ajax call to delete.
		return json_encode($data);
		//return Redirect::route($data['url'])->with('message', 'Team Relationship  deleted');
		
	}

	

 /**
 * OTM TABLE INDEX functions (excluding parent, owner - use Parent controller;  and type tables - not usually necessary )
 *
 */
 

}