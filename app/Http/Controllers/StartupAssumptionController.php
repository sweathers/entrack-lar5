<?php


/**
 * Parent-Child Controller for Startup Assumption 
 * Controls access by parentId to Index, Create, and Store Methods.  
 * All other methods are controlled by the standard controller. 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */
 
 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\Assumption;

use App\Models\Startup;
 
class StartupAssumptionController extends Controller {	
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * Display a list and provide links for CRUD actions
	 * usage: GET RESOURCE
	 *
	 * @return Response
	 */
	public function index($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->first(); 
		$assumptions = Assumption::where('startup_id',  $startup->startup_id )
 
		   ->orderBy('sort_order', 'asc')
		   ->get();

		$data['assumptions']=array();
				
		//REFERENCED RESOURCE NAMES
		foreach ($assumptions as $row)
		{		
			//get OTM names
			$row->startup_name = (( $row->startup) ? $row->startup->startup_name : '' );
			
			//get MTM names
			
			
			$data['assumptions'][]=$row;	
		} /* end foreach row */

		$data['parentName'] = $startup->startup_name;
		
		$data['startup'] = $startup;
			
		if ($this->is_admin_url ){
		return View::make(self::$admin_dir . '.' . 'assumptions.index-for-startups', $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
 
		return view('assumptions.index-for-startups', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin )->first(); 

		$data = array();
		$assumption = new Assumption; //MODEL for default values and form model binding
		
		
		//REFERENCED RESOURCE SELECT LIST
		$data['startupList'] = Startup::where('startup_id', $parentId)->lists('startup_name', 'startup_id');
		$data['parentName'] = $startup->startup_name;
		$data['startup'] = $startup;

				$assumption->fill($startup->toArray()); //inherit as much as possible from the parent
		$data['assumption'] = $assumption; 
		
		if ($this->is_admin_url ){
		return view(self::$admin_dir . '.' . 'assumptions.create-for-startups', $data);
 
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('assumptions.create-for-startups', $data);

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store($parentId)
	{

		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin )->first(); 
	
		$validator = Validator::make(Input::all(), Assumption::$insertRules);

		if($validator->passes()) {

			$assumption = new Assumption;
			$assumption->fill(Input::all());
			
	
			//FILL GUARDED PARENT ID
			$assumption->startup_id = $parentId;
	
			
			//SET DEFAULT VALUES ON CREATE
			if (! $assumption->sort_order) $assumption->sort_order = 20;

			
			if($assumption->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.startups.assumptions.index', $startup->startup_id )->with('data', 'Assumption has been created!');
			
				} elseif ($this->is_api_url ) {
				    $data = ['error' => false,
						'message' => 'Assumption has been created!',
						'url' => URL::route("api.startups.assumptions.index", [$startup->startup_id])
								
						];
					return json_encode($data);
				}	
		return Redirect::route('startups.assumptions.index', $startup->startup_id )->with('data', 'Assumption has been created!');
	
				
			}

		}
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . 'startups.assumptions.create', [$startup->startup_id])->withInput(Input::all())->withErrors($validator);
				
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}		
		return Redirect::route('startups.assumptions.create', [$startup->startup_id])->withInput(Input::all())->withErrors($validator);

	}

	/**
	 * Show by ParentId 
	 * redirects to the standard controller
	 *
	 */
	public function show($parentId, $id)
	{
		$startup = Startup::where('startup_id', $parentId)->authed( $this->user_id, $this->user_is_admin, 'show')->first(); 
		

		//$assumption = $startup->startupAssumptions()->where('assumption_id', $id)
		$assumption = Assumption::where('assumption_id', $id)->where('startup_id',  $startup->startup_id )
		    
		   ->first();
		
		if (empty($assumption)){ return Redirect::route('assumptions.index')->with('message', 'You are not logged in as the owner of this Assumption.'); }
		
		$data['assumption'] = $assumption;
		$data['startup'] = $startup;


		if ($this->is_admin_url ){ 
		return view(self::$admin_dir . '.' . 'assumptions.view-for-startups', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('assumptions.view-for-startups', $data);
	}

	
	/**
	 * Edit by ParentId 
	 * redirects to the standard controller
	 *
	 */
	public function edit($parentId, $id)
	{
		//REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.assumptions.edit"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.assumptions.edit', [$id] );
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route('assumptions.edit', [$id]);	
	}

	/**
	 * Update the specified resource in storage by parentId
	 * Redirects to the standard controller
	 *
	 */
	public function update($parentId, $id)
	{
	    //REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.assumptions.edit"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.assumptions.edit', [$id]);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route('assumptions.edit', [$id]);
	}

	/**
	 * Remove the specified resource from storage by parentId
	 * Redirects to the standard controller
	 *
	 */
	public function destroy($parentId, $id)
	{	
		//REDIRECT TO CHILD CONTROLLER
		$data = ['error' => false,
				'message' => 'Action not available for this Parent / Child controller.',
				'url' => URL::route("api.assumptions.destroy"),
		];
		if ($this->is_admin_url ){
			return Redirect::route('admin.assumptions.destroy', [$id]);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		
		return Redirect::route('assumptions.destroy', [$id]);
	}

	
	
}