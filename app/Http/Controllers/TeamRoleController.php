<?php

/** 
 * Controller for Team Role 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\TeamRole;


class TeamRoleController extends Controller {
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * TeamRole @ index
	 * Display a list of TeamRoles and links for CRUD actions
	 *
	 * @param int $parentId (optional, Used by helper method for filtering by a referenced table. )
	 * @param string $parentSlug (optional, Used by helper method for filtering by a referenced table.)
	 * @return Response
	 */
	public function index($refId=0, $refSlug='')
	{
		$teamRoles = TeamRole::authed( $this->user_id, $this->user_is_admin, 'view' )
		    
		   ->orderBy('sort_order', 'asc') 
		   ->get();
		$data['teamRoles']=array();
			
		//REFERENCED RESOURCE NAMES
		foreach ($teamRoles as $row)
		{				
			//get OTM names


			//get MTM names
			$data['teamRoles'][]=$row;	
		} /* end foreach row */
	

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-roles.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
		return view('team-roles.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 * TeamRole @ create
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($refId=0, $refSlug='')
	{
		$data = array();
		$teamRole = new TeamRole; //MODEL for default values and form model binding
		
		//REFERENCED RESOURCE SELECT LIST
				
		
		
		$data['teamRole'] = $teamRole; 

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-roles.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-roles.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);	
	}


	/**
	 * TeamRole @ store
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($refId=0, $refSlug='')
	{

		$validator = Validator::make(Input::all(), TeamRole::$insertRules);

		if($validator->passes()) {
				$teamRole = new TeamRole;
				$teamRole->fill(Input::all());
			
					
			//SET DEFAULT VALUES ON CREATE
			if (! $teamRole->sort_order) $teamRole->sort_order = 20;
						
			//SET CODE VALUE FROM NAME
			if (! $teamRole->team_role_code) {
				$str=$teamRole->team_role_name;
				//sanitize the string to all lower with spaces and no other non-alpha-num
				$str = preg_replace('/[\_\-\t\n\r\0\x0b\!\@\#\$\%\^\&\*\+\=\ ]+/', ' ', $str);
				$str = preg_replace('/[^a-zA-Z0-9\ ]*/', '', $str);
				$str = preg_replace('/([a-z])([A-Z0-9])([a-z])/', '$1 $2$3', $str);
				$str = trim(strtolower($str));
				$str = str_singular($str);
				$str_snake_case = preg_replace('/[\ ]+/', '_', $str);
				//DEFAULT VALUE FOR CODE
				$teamRole->team_role_code = substr($str_snake_case, 0, 16) . "";
			}
			

			if($teamRole->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.team-roles.index')->with('message', 'Team Role has been created!');				
				} elseif ($this->is_api_url ) {
				    $data['error'] =false;
					$data['message'] = 'Team Role has been created!';
					$data['url'] = URL::route("api.((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '')team-roles.index");							

					return json_encode($data);
				}	
				return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'team-roles.index', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->with('message', 'Team Role has been created!');					
			}

		}
		$data['error']   = true;
		$data['message']  = 'Please fill required fields below!';
		$data['errors'] = $validator->messages();

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . ((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'team-roles.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'team-roles.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
	}
	

	/**
	 * TeamRole @ show
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id, $refId=0, $refSlug='')
	{
		$data = array();

		//ACCESS CONTROL 
		$teamRole = TeamRole::where('team_role_id', $id)->authed( $this->user_id, $this->user_is_admin, 'show' )
		    
		   ->first();
	
		if (empty($teamRole)){ return Redirect::route('team-roles.index')->with('message', 'You are not logged in as the owner of this Team Role.'); }
			
		$data['teamRole'] = $teamRole;

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-roles.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-roles.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 *  TeamRole @ edit
	 *  Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$teamRole = TeamRole::where('team_role_id', $id)
		    
		   ->first();

		if (empty($teamRole)){ return Redirect::route('team-roles.index')->with('message', 'You are not logged in as the owner of this Team Role.'); }
		
		$data['teamRole'] = $teamRole;

		//REFERENCED RESOURCE SELECT LIST

		//MTM OPTIONS
		//add parent IDs to where clause where appropriate

  		//SET SuccessURL if manually set in url
  		$data['successUrl'] = Input::get('successUrl', '');
  	
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-roles.edit', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-roles.edit', $data);
	}

	/**
	 * TeamRole @ update
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		$teamRole = TeamRole::where('team_role_id', $id)
		    
		   ->first();
		
		if (empty($teamRole)){ return Redirect::route('team-roles.index')->with('message', 'You are not logged in as the owner of this Team Role.'); }
	
		$validator = Validator::make(Input::all(), TeamRole::$updateRules);

		if($validator->passes()) {
			$teamRole->fill(Input::all());

			if($teamRole->save()) {
			
				/**
				 * MTM UPDATES 
				 * for simple xrefs delete and insert
				 * for xrefs tables with data, toggle the is_active flag and insert if doesn't exist
				 *
				 **/ 


				//REDIRECT TO INDEX PAGE
				$success_url = Input::get('success_url', '');
								
				if ($this->is_admin_url ){
					if (!empty ($success_url)) { return Redirect::to($success_url); }
					return Redirect::route('admin.team-roles.index')->with('messge', 'Team Role has been updated.');				
				} elseif ($this->is_api_url ) {
					$data = ['error' => false,
						'message' => 'Team Role has been updated!',
						'url' => URL::route("api.team-roles.index")							
						];
					if (!empty ($success_url)) {$data['url'] = $success_url; }
					return json_encode($data);
				}
				if (!empty ($success_url)) { return Redirect::to($success_url); } 
				return Redirect::route('team-roles.show', array($id))->with('message', 'Team Role has been updated!');
			}
		}

		//RETURN ERRORS
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route('admin.team-roles.edit', $id)
				->withInput()
				->withErrors($validator)
				->with('message', 'There were validation errors.');			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}

		return Redirect::route('team-roles.edit', $id)
			->withInput()
			->withErrors($validator)
			->with('message', 'There were validation errors.');

	}


	/**
	 * TeamRole @ destroy
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$teamRole = TeamRole::where('team_role_id', $id)->first();
		if (empty($teamRole)){ return Redirect::route('team-roles.index')->with('message', 'You are not logged in as the owner of this Team Role.'); }
		
		  // MTM delete xref rows: 

		/** 
		 * // CHILDREN delete  Where child model = "Jr" 
		 * 
		 * 	$teamRoleJrs = TeamRoleJr::where('team_role_id', $id)->get();
		 *	foreach ($teamRoleJrs as $child) {
		 *	  //recursivly delete decendants
		 *	  //$grandchildren = GrandChild::where('parent_id', $child->teamRole_jr_id)->delete();
		 *
		 *	  $child->delete();
		 *  }
		 */

		$teamRole->delete();
		
		//Delete with popup modal and json api response, because DELETE method only available via forms submital.  

		$data = ['error' => false,
				'message' => "Deleted Team Role(" . $id . ")"
			];
		if ($this->is_admin_url ){
			$data['url']= URL::route("admin.team-roles.index");
		} elseif ($this->is_api_url ) {
			$data['url']= URL::route("api.team-roles.index");
		} else {
			$data['url']= URL::route("team-roles.index");
		}
		//return json string if the views use an ajax call to delete.
		return json_encode($data);
		//return Redirect::route($data['url'])->with('message', 'Team Role  deleted');
		
	}

	

 /**
 * OTM TABLE INDEX functions (excluding parent, owner - use Parent controller;  and type tables - not usually necessary )
 *
 */
 

}