<?php

/** 
 * Controller for Startup 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\Startup;

// use App\User;
use App\Models\LocationPlace;
use App\Models\UserProfile;
use App\Models\StartupMilestone;
use App\Models\Milestone;

class StartupController extends Controller {
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * Startup @ index
	 * Display a list of Startups and links for CRUD actions
	 *
	 * @param int $parentId (optional, Used by helper method for filtering by a referenced table. )
	 * @param string $parentSlug (optional, Used by helper method for filtering by a referenced table.)
	 * @return Response
	 */
	public function index($refId=0, $refSlug='')
	{
		$startups = Startup::authed( $this->user_id, $this->user_is_admin, 'view' )
		   ->with('locationPlace', 'userProfile', 'startupMilestones') 
		   ->orderBy('sort_order', 'asc') 
		   ->get();
		$data['startups']=array();
			
		//REFERENCED RESOURCE NAMES
		foreach ($startups as $row)
		{				
			//get OTM names
			$row->locationPlace_name = (( $row->locationPlace) ? $row->locationPlace->geo_place_name : '' );
			$row->userProfile_name = (( $row->user) ? $row->userProfile->name : '' );
			

			//get MTM names
			$milestone_list=$row->startupMilestones()->with('milestones')->take(5)->lists('milestone_name');
			$row->milestone_names =  $milestone_list->implode(', ');	
			$data['startups'][]=$row;	
		} /* end foreach row */
	

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'startups.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
		return view('startups.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 * Startup @ create
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($refId=0, $refSlug='')
	{
		$data = array();
		$startup = new Startup; //MODEL for default values and form model binding
		
		//REFERENCED RESOURCE SELECT LIST
			
			$data['locationPlaceList'] = LocationPlace::orderBy('sort_order', 'asc')->lists('geo_place_name', 'geo_place_id');
        if($this->user_is_admin){ $data['userProfileList'] = UserProfile::lists('name', 'id'); }
		else { $data['userProfileList'] = UserProfile::where('id', $this->user_id)->lists('name', 'id'); }
				
		
		
		$data['startup'] = $startup; 

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'startups.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('startups.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);	
	}


	/**
	 * Startup @ store
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($refId=0, $refSlug='')
	{

		$validator = Validator::make(Input::all(), Startup::$insertRules);

		if($validator->passes()) {
				$startup = new Startup;
				$startup->fill(Input::all());
			//FILL GUARDED OWNER ID
			if(($this->user_is_admin) && (Input::has('user_id'))){ $startup->user_id= Input::get('user_id'); } 
			else { $startup->user_id = $this->user_id; }
			
					
			//SET DEFAULT VALUES ON CREATE
			if (! $startup->sort_order) $startup->sort_order = 20;
			

			if($startup->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.startups.index')->with('message', 'Startup has been created!');				
				} elseif ($this->is_api_url ) {
				    $data['error'] =false;
					$data['message'] = 'Startup has been created!';
					$data['url'] = URL::route("api.((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '')startups.index");							

					return json_encode($data);
				}	
				return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'startups.index', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->with('message', 'Startup has been created!');					
			}

		}
		$data['error']   = true;
		$data['message']  = 'Please fill required fields below!';
		$data['errors'] = $validator->messages();

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . ((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'startups.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'startups.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
	}
	

	/**
	 * Startup @ show
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id, $refId=0, $refSlug='')
	{
		$data = array();

		//ACCESS CONTROL 
		$startup = Startup::where('startup_id', $id)->authed( $this->user_id, $this->user_is_admin, 'show' )
		   ->with('locationPlace', 'userProfile', 'startupMilestones') 
		   ->first();
	
		if (empty($startup)){ return Redirect::route('startups.index')->with('message', 'You are not logged in as the owner of this Startup.'); }
			
		$data['startup'] = $startup;

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'startups.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('startups.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 *  Startup @ edit
	 *  Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$startup = Startup::where('startup_id', $id)->authed( $this->user_id, $this->user_is_admin, 'edit' )
		   ->with('locationPlace', 'userProfile', 'startupMilestones') 
		   ->first();

		if (empty($startup)){ return Redirect::route('startups.index')->with('message', 'You are not logged in as the owner of this Startup.'); }
		
		$data['startup'] = $startup;

		//REFERENCED RESOURCE SELECT LIST
		$data['locationPlaceList'] = LocationPlace::where('geo_place_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('geo_place_name', 'geo_place_id');
		if($this->user_is_admin){ $data['userProfileList'] = UserProfile::lists('name', 'id'); }
		else { $data['userProfileList'] = UserProfile::where('id', $this->user_id)->lists('name', 'id'); }

		//MTM OPTIONS
		//add parent IDs to where clause where appropriate
		//milestones 
		$data['milestones_selected'] = $startup->startupMilestones()->where('xref_startup_milestone.is_active', '>', 0)->pluck('xref_startup_milestone.milestone_id')->toArray();
 		$data['milestones_selected_count'] = count($data['milestones_selected']);
		$data['milestone_options'] = Milestone::where('milestone_id', '>', '-1')->orderBy('sort_order' , 'ASC')->lists('milestone_name', 'milestone_id');					

  		//SET SuccessURL if manually set in url
  		$data['successUrl'] = Input::get('successUrl', '');
  	
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'startups.edit', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('startups.edit', $data);
	}

	/**
	 * Startup @ update
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		//ACCESS CONTROL - admin sees all. everyone else sees their own.
		$startup = Startup::where('startup_id', $id)->authed( $this->user_id, $this->user_is_admin, 'update' )
		   ->with('locationPlace', 'userProfile', 'startupMilestones') 
		   ->first();
		
		if (empty($startup)){ return Redirect::route('startups.index')->with('message', 'You are not logged in as the owner of this Startup.'); }
	
		$validator = Validator::make(Input::all(), Startup::$updateRules);

		if($validator->passes()) {
			$startup->fill(Input::all());

			//FILL GUARDED OWNER ID
			if(($this->user_is_admin) && (Input::has('user_id'))){ $startup->user_id = Input::get('user_id'); }
			//else { $startup->user_id = $this->user_id; }
			
			if($startup->save()) {
			
				/**
				 * MTM UPDATES 
				 * for simple xrefs delete and insert
				 * for xrefs tables with data, toggle the is_active flag and insert if doesn't exist
				 *
				 **/ 


	//UPDATE milestones - XREF DATA TABLE 
				if(  Input::has('milestones_selected_count') ) {
					$xref_add = array();
					$xref_enable = array();
					//GET OLD ACTIVE ROWS	
					                                               
					$xref_existing = $startup->startupMilestones()->select('tbl_milestone.milestone_id')->lists('milestone_id');		
					//GET SELECTIONS FROM FORM INPUT
					$xref_selected_input = Input::get('milestones_selected', array());
					
					if (is_string($xref_selected_input)) { $xref_selected_input = explode(',', $xref_selected_input); }
					if (is_array($xref_selected_input)) {
						foreach ($xref_selected_input as $that_xref_id) {
							if ($xref_existing->contains($that_xref_id)) {
								//ENABLE EXISTING ROWS
								$xref_enable[] = $that_xref_id;
							} elseif ($that_xref_id >0) {
								//CREATE NEW ROWS
								$xref_add[] = array('startup_id' => $startup->startup_id, 'milestone_id' => $that_xref_id, 'is_active' => 1);
							}
						}
					}	
	
					//TURN OFF ALL ROWS
					StartupMilestone::where('startup_id', '=', $startup->startup_id)->update(array('is_active' => 0));
					
					//TURN ON SELECTED ROWS
					StartupMilestone::where('startup_id', '=', $startup->startup_id)->whereIn('milestone_id', $xref_enable)->update(array('is_active' => 1));
							
					//ADD NEW ROWS
					if (count($xref_add) > 0){ StartupMilestone::insert($xref_add); }
				}

				//REDIRECT TO INDEX PAGE
				$success_url = Input::get('success_url', '');
								
				if ($this->is_admin_url ){
					if (!empty ($success_url)) { return Redirect::to($success_url); }
					return Redirect::route('admin.startups.index')->with('messge', 'Startup has been updated.');				
				} elseif ($this->is_api_url ) {
					$data = ['error' => false,
						'message' => 'Startup has been updated!',
						'url' => URL::route("api.startups.index")							
						];
					if (!empty ($success_url)) {$data['url'] = $success_url; }
					return json_encode($data);
				}
				if (!empty ($success_url)) { return Redirect::to($success_url); } 
				return Redirect::route('startups.show', array($id))->with('message', 'Startup has been updated!');
			}
		}

		//RETURN ERRORS
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route('admin.startups.edit', $id)
				->withInput()
				->withErrors($validator)
				->with('message', 'There were validation errors.');			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}

		return Redirect::route('startups.edit', $id)
			->withInput()
			->withErrors($validator)
			->with('message', 'There were validation errors.');

	}


	/**
	 * Startup @ destroy
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$startup = Startup::where('startup_id', $id)->authed( $this->user_id, $this->user_is_admin, 'destroy')->first();
		if (empty($startup)){ return Redirect::route('startups.index')->with('message', 'You are not logged in as the owner of this Startup.'); }
		
		  // MTM delete xref rows: 
			StartupMilestone::where('startup_id', '=', $startup->startup_id)->delete();

		/** 
		 * // CHILDREN delete  Where child model = "Jr" 
		 * 
		 * 	$startupJrs = StartupJr::where('startup_id', $id)->get();
		 *	foreach ($startupJrs as $child) {
		 *	  //recursivly delete decendants
		 *	  //$grandchildren = GrandChild::where('parent_id', $child->startup_jr_id)->delete();
		 *
		 *	  $child->delete();
		 *  }
		 */

		$startup->delete();
		
		//Delete with popup modal and json api response, because DELETE method only available via forms submital.  

		$data = ['error' => false,
				'message' => "Deleted Startup(" . $id . ")"
			];
		if ($this->is_admin_url ){
			$data['url']= URL::route("admin.startups.index");
		} elseif ($this->is_api_url ) {
			$data['url']= URL::route("api.startups.index");
		} else {
			$data['url']= URL::route("startups.index");
		}
		//return json string if the views use an ajax call to delete.
		return json_encode($data);
		//return Redirect::route($data['url'])->with('message', 'Startup  deleted');
		
	}

	
/**
 * MTM Relationships INDEX and SHOW functions
 * 
 *
 */
	

 /**
 * OTM TABLE INDEX functions (excluding parent, owner - use Parent controller;  and type tables - not usually necessary )
 *
 */
 

}