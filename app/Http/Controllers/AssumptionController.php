<?php

/** 
 * Controller for Assumption 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\Assumption;

use App\Models\Startup;

class AssumptionController extends Controller {
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * Assumption @ index
	 * Display a list of Assumptions and links for CRUD actions
	 *
	 * @param int $parentId (optional, Used by helper method for filtering by a referenced table. )
	 * @param string $parentSlug (optional, Used by helper method for filtering by a referenced table.)
	 * @return Response
	 */
	public function index($refId=0, $refSlug='')
	{
		$assumptions = Assumption::authed( $this->user_id, $this->user_is_admin, 'view' )
		   ->with('startup') 
		   ->orderBy('sort_order', 'asc') 
		   ->get();
		$data['assumptions']=array();
			
		//REFERENCED RESOURCE NAMES
		foreach ($assumptions as $row)
		{				
			//get OTM names
			$row->startup_name = (( $row->startup) ? $row->startup->startup_name : '' );
			

			//get MTM names
			$data['assumptions'][]=$row;	
		} /* end foreach row */
	

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'assumptions.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
		return view('assumptions.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 * Assumption @ create
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($refId=0, $refSlug='')
	{
		$data = array();
		$assumption = new Assumption; //MODEL for default values and form model binding
		
		//REFERENCED RESOURCE SELECT LIST
			
			$data['startupList'] = Startup::orderBy('sort_order', 'asc')->lists('startup_name', 'startup_id');
				
		
		
		$data['assumption'] = $assumption; 

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'assumptions.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('assumptions.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);	
	}


	/**
	 * Assumption @ store
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($refId=0, $refSlug='')
	{

		$validator = Validator::make(Input::all(), Assumption::$insertRules);

		if($validator->passes()) {
				$assumption = new Assumption;
				$assumption->fill(Input::all());
			
			
			//FILL GUARDED PARENT ID
			if(($this->user_is_admin) && (Input::has('startup_id'))){ $assumption->startup_id= Input::get('startup_id'); } 
			//$assumption->startup_id = $parentId;
					
			//SET DEFAULT VALUES ON CREATE
			if (! $assumption->sort_order) $assumption->sort_order = 20;
			

			if($assumption->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.assumptions.index')->with('message', 'Assumption has been created!');				
				} elseif ($this->is_api_url ) {
				    $data['error'] =false;
					$data['message'] = 'Assumption has been created!';
					$data['url'] = URL::route("api.((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '')assumptions.index");							

					return json_encode($data);
				}	
				return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'assumptions.index', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->with('message', 'Assumption has been created!');					
			}

		}
		$data['error']   = true;
		$data['message']  = 'Please fill required fields below!';
		$data['errors'] = $validator->messages();

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . ((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'assumptions.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'assumptions.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
	}
	

	/**
	 * Assumption @ show
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id, $refId=0, $refSlug='')
	{
		$data = array();

		//ACCESS CONTROL 
		$assumption = Assumption::where('assumption_id', $id)->authed( $this->user_id, $this->user_is_admin, 'show' )
		   ->with('startup') 
		   ->first();
	
		if (empty($assumption)){ return Redirect::route('assumptions.index')->with('message', 'You are not logged in as the owner of this Assumption.'); }
			
		$data['assumption'] = $assumption;

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'assumptions.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('assumptions.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 *  Assumption @ edit
	 *  Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$assumption = Assumption::where('assumption_id', $id)
		   ->with('startup') 
		   ->first();

		if (empty($assumption)){ return Redirect::route('assumptions.index')->with('message', 'You are not logged in as the owner of this Assumption.'); }
		
		$data['assumption'] = $assumption;

		//REFERENCED RESOURCE SELECT LIST
		$data['startupList'] = Startup::where('startup_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('startup_name', 'startup_id');

		//MTM OPTIONS
		//add parent IDs to where clause where appropriate

  		//SET SuccessURL if manually set in url
  		$data['successUrl'] = Input::get('successUrl', '');
  	
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'assumptions.edit', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('assumptions.edit', $data);
	}

	/**
	 * Assumption @ update
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		$assumption = Assumption::where('assumption_id', $id)
		   ->with('startup') 
		   ->first();
		
		if (empty($assumption)){ return Redirect::route('assumptions.index')->with('message', 'You are not logged in as the owner of this Assumption.'); }
	
		$validator = Validator::make(Input::all(), Assumption::$updateRules);

		if($validator->passes()) {
			$assumption->fill(Input::all());

			//FILL GUARDED PARENT ID
			if(($this->user_is_admin) && (Input::has('startup_id'))){ $assumption->startup_id = Input::get('startup_id'); }
			if($assumption->save()) {
			
				/**
				 * MTM UPDATES 
				 * for simple xrefs delete and insert
				 * for xrefs tables with data, toggle the is_active flag and insert if doesn't exist
				 *
				 **/ 


				//REDIRECT TO INDEX PAGE
				$success_url = Input::get('success_url', '');
								
				if ($this->is_admin_url ){
					if (!empty ($success_url)) { return Redirect::to($success_url); }
					return Redirect::route('admin.assumptions.index')->with('messge', 'Assumption has been updated.');				
				} elseif ($this->is_api_url ) {
					$data = ['error' => false,
						'message' => 'Assumption has been updated!',
						'url' => URL::route("api.assumptions.index")							
						];
					if (!empty ($success_url)) {$data['url'] = $success_url; }
					return json_encode($data);
				}
				if (!empty ($success_url)) { return Redirect::to($success_url); } 
				return Redirect::route('assumptions.show', array($id))->with('message', 'Assumption has been updated!');
			}
		}

		//RETURN ERRORS
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route('admin.assumptions.edit', $id)
				->withInput()
				->withErrors($validator)
				->with('message', 'There were validation errors.');			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}

		return Redirect::route('assumptions.edit', $id)
			->withInput()
			->withErrors($validator)
			->with('message', 'There were validation errors.');

	}


	/**
	 * Assumption @ destroy
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$assumption = Assumption::where('assumption_id', $id)->first();
		if (empty($assumption)){ return Redirect::route('assumptions.index')->with('message', 'You are not logged in as the owner of this Assumption.'); }
		
		  // MTM delete xref rows: 

		/** 
		 * // CHILDREN delete  Where child model = "Jr" 
		 * 
		 * 	$assumptionJrs = AssumptionJr::where('assumption_id', $id)->get();
		 *	foreach ($assumptionJrs as $child) {
		 *	  //recursivly delete decendants
		 *	  //$grandchildren = GrandChild::where('parent_id', $child->assumption_jr_id)->delete();
		 *
		 *	  $child->delete();
		 *  }
		 */

		$assumption->delete();
		
		//Delete with popup modal and json api response, because DELETE method only available via forms submital.  

		$data = ['error' => false,
				'message' => "Deleted Assumption(" . $id . ")"
			];
		if ($this->is_admin_url ){
			$data['url']= URL::route("admin.assumptions.index");
		} elseif ($this->is_api_url ) {
			$data['url']= URL::route("api.assumptions.index");
		} else {
			$data['url']= URL::route("assumptions.index");
		}
		//return json string if the views use an ajax call to delete.
		return json_encode($data);
		//return Redirect::route($data['url'])->with('message', 'Assumption  deleted');
		
	}

	

 /**
 * OTM TABLE INDEX functions (excluding parent, owner - use Parent controller;  and type tables - not usually necessary )
 *
 */
 

}