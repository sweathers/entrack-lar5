<?php

/** 
 * Controller for User Profile 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\UserProfile;


class UserProfileController extends Controller {
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * UserProfile @ index
	 * Display a list of UserProfiles and links for CRUD actions
	 *
	 * @param int $parentId (optional, Used by helper method for filtering by a referenced table. )
	 * @param string $parentSlug (optional, Used by helper method for filtering by a referenced table.)
	 * @return Response
	 */
	public function index($refId=0, $refSlug='')
	{
		$userProfiles = UserProfile::authed( $this->user_id, $this->user_is_admin, 'view' )
		    
		    
		   ->get();
		$data['userProfiles']=array();
			
		//REFERENCED RESOURCE NAMES
		foreach ($userProfiles as $row)
		{				
			//get OTM names


			//get MTM names
			$data['userProfiles'][]=$row;	
		} /* end foreach row */
	

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'user-profiles.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
		return view('user-profiles.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 * UserProfile @ create
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($refId=0, $refSlug='')
	{
		$data = array();
		$userProfile = new UserProfile; //MODEL for default values and form model binding
		
		//REFERENCED RESOURCE SELECT LIST
				
		
		
		$data['userProfile'] = $userProfile; 

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'user-profiles.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('user-profiles.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);	
	}


	/**
	 * UserProfile @ store
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($refId=0, $refSlug='')
	{

		$validator = Validator::make(Input::all(), UserProfile::$insertRules);

		if($validator->passes()) {
				$userProfile = new UserProfile;
				$userProfile->fill(Input::all());
			
					
			//SET DEFAULT VALUES ON CREATE
			if (! $userProfile->is_active) $userProfile->is_active = 1;
			

			if($userProfile->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.user-profiles.index')->with('message', 'User Profile has been created!');				
				} elseif ($this->is_api_url ) {
				    $data['error'] =false;
					$data['message'] = 'User Profile has been created!';
					$data['url'] = URL::route("api.((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '')user-profiles.index");							

					return json_encode($data);
				}	
				return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'user-profiles.index', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->with('message', 'User Profile has been created!');					
			}

		}
		$data['error']   = true;
		$data['message']  = 'Please fill required fields below!';
		$data['errors'] = $validator->messages();

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . ((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'user-profiles.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'user-profiles.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
	}
	

	/**
	 * UserProfile @ show
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id, $refId=0, $refSlug='')
	{
		$data = array();

		//ACCESS CONTROL 
		$userProfile = UserProfile::where('id', $id)->authed( $this->user_id, $this->user_is_admin, 'show' )
		    
		   ->first();
	
		if (empty($userProfile)){ return Redirect::route('user-profiles.index')->with('message', 'You are not logged in as the owner of this User Profile.'); }
			
		$data['userProfile'] = $userProfile;

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'user-profiles.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('user-profiles.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 *  UserProfile @ edit
	 *  Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$userProfile = UserProfile::where('id', $id)
		    
		   ->first();

		if (empty($userProfile)){ return Redirect::route('user-profiles.index')->with('message', 'You are not logged in as the owner of this User Profile.'); }
		
		$data['userProfile'] = $userProfile;

		//REFERENCED RESOURCE SELECT LIST

		//MTM OPTIONS
		//add parent IDs to where clause where appropriate

  		//SET SuccessURL if manually set in url
  		$data['successUrl'] = Input::get('successUrl', '');
  	
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'user-profiles.edit', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('user-profiles.edit', $data);
	}

	/**
	 * UserProfile @ update
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		$userProfile = UserProfile::where('id', $id)
		    
		   ->first();
		
		if (empty($userProfile)){ return Redirect::route('user-profiles.index')->with('message', 'You are not logged in as the owner of this User Profile.'); }
	
		$validator = Validator::make(Input::all(), UserProfile::$updateRules);

		if($validator->passes()) {
			$userProfile->fill(Input::all());

			if($userProfile->save()) {
			
				/**
				 * MTM UPDATES 
				 * for simple xrefs delete and insert
				 * for xrefs tables with data, toggle the is_active flag and insert if doesn't exist
				 *
				 **/ 


				//REDIRECT TO INDEX PAGE
				$success_url = Input::get('success_url', '');
								
				if ($this->is_admin_url ){
					if (!empty ($success_url)) { return Redirect::to($success_url); }
					return Redirect::route('admin.user-profiles.index')->with('messge', 'User Profile has been updated.');				
				} elseif ($this->is_api_url ) {
					$data = ['error' => false,
						'message' => 'User Profile has been updated!',
						'url' => URL::route("api.user-profiles.index")							
						];
					if (!empty ($success_url)) {$data['url'] = $success_url; }
					return json_encode($data);
				}
				if (!empty ($success_url)) { return Redirect::to($success_url); } 
				return Redirect::route('user-profiles.show', array($id))->with('message', 'User Profile has been updated!');
			}
		}

		//RETURN ERRORS
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route('admin.user-profiles.edit', $id)
				->withInput()
				->withErrors($validator)
				->with('message', 'There were validation errors.');			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}

		return Redirect::route('user-profiles.edit', $id)
			->withInput()
			->withErrors($validator)
			->with('message', 'There were validation errors.');

	}


	/**
	 * UserProfile @ destroy
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$userProfile = UserProfile::where('id', $id)->first();
		if (empty($userProfile)){ return Redirect::route('user-profiles.index')->with('message', 'You are not logged in as the owner of this User Profile.'); }
		
		  // MTM delete xref rows: 

		/** 
		 * // CHILDREN delete  Where child model = "Jr" 
		 * 
		 * 	$userProfileJrs = UserProfileJr::where('id', $id)->get();
		 *	foreach ($userProfileJrs as $child) {
		 *	  //recursivly delete decendants
		 *	  //$grandchildren = GrandChild::where('parent_id', $child->userProfile_jr_id)->delete();
		 *
		 *	  $child->delete();
		 *  }
		 */

		$userProfile->delete();
		
		//Delete with popup modal and json api response, because DELETE method only available via forms submital.  

		$data = ['error' => false,
				'message' => "Deleted User Profile(" . $id . ")"
			];
		if ($this->is_admin_url ){
			$data['url']= URL::route("admin.user-profiles.index");
		} elseif ($this->is_api_url ) {
			$data['url']= URL::route("api.user-profiles.index");
		} else {
			$data['url']= URL::route("user-profiles.index");
		}
		//return json string if the views use an ajax call to delete.
		return json_encode($data);
		//return Redirect::route($data['url'])->with('message', 'User Profile  deleted');
		
	}

	

 /**
 * OTM TABLE INDEX functions (excluding parent, owner - use Parent controller;  and type tables - not usually necessary )
 *
 */
 

}