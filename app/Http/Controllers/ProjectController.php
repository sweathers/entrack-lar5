<?php

/** 
 * Controller for Project 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\Project;

use App\Models\Startup;
use App\Models\ProjectStatus;
use App\Models\Assumption;
use App\Models\Milestone;
use App\Models\TeamMember;
use App\Models\ProjectTeamMember;

class ProjectController extends Controller {
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * Project @ index
	 * Display a list of Projects and links for CRUD actions
	 *
	 * @param int $parentId (optional, Used by helper method for filtering by a referenced table. )
	 * @param string $parentSlug (optional, Used by helper method for filtering by a referenced table.)
	 * @return Response
	 */
	public function index($refId=0, $refSlug='')
	{
		$projects = Project::authed( $this->user_id, $this->user_is_admin, 'view' )
		   ->with('startup', 'projectStatus', 'assumption', 'milestone', 'responsibleTeamMember', 'projectTeamMembers') 
		   ->orderBy('sort_order', 'asc') 
		   ->get();
				

		if (('assumptions' == $refSlug) && ($refId > 0)){
			// filtering for route assumptions/{ assumptions }/projects 
			$data['assumption'] = Assumption::where('assumption_id', $refId)->authed( $this->user_id, $this->user_is_admin, 'view' )->first();
			$projects = Project::where('assumption_id', $data['assumption']->assumption_id)->authed( $this->user_id, $this->user_is_admin, 'view' )
			   ->get();
		} 
				

		if (('milestones' == $refSlug) && ($refId > 0)){
			// filtering for route milestones/{ milestones }/projects 
			$data['milestone'] = Milestone::where('milestone_id', $refId)->authed( $this->user_id, $this->user_is_admin, 'view' )->first();
			$projects = Project::where('milestone_id', $data['milestone']->milestone_id)->authed( $this->user_id, $this->user_is_admin, 'view' )
			   ->get();
		} 
				

		if (('team-members' == $refSlug) && ($refId > 0)){
			// filtering for route team-members/{ teamMembers }/projects 
			$data['teamMember'] = TeamMember::where('team_member_id', $refId)->authed( $this->user_id, $this->user_is_admin, 'view' )->first();
			$projects = Project::where('responsible_team_member_id', $data['teamMember']->team_member_id)->authed( $this->user_id, $this->user_is_admin, 'view' )
			   ->get();
		} 
		$data['projects']=array();
			
		//REFERENCED RESOURCE NAMES
		foreach ($projects as $row)
		{				
			//get OTM names
			$row->startup_name = (( $row->startup) ? $row->startup->startup_name : '' );
						$row->projectStatus_name = (( $row->projectStatus) ? $row->projectStatus->project_status_name : '' );
			$row->assumption_name = (( $row->assumption) ? $row->assumption->assumption_name : '' );
			$row->milestone_name = (( $row->milestone) ? $row->milestone->milestone_name : '' );
			$row->teamMember_name = (( $row->responsibleTeamMember) ? $row->responsibleTeamMember->team_member_name : '' );


			//get MTM names
			$team_member_list=$row->projectTeamMembers()->with('teamMembers')->take(5)->lists('team_member_name');
			$row->team_member_names =  $team_member_list->implode(', ');	
			$data['projects'][]=$row;	
		} /* end foreach row */
	

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'projects.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
		return view('projects.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 * Project @ create
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($refId=0, $refSlug='')
	{
		$data = array();
		$project = new Project; //MODEL for default values and form model binding
		
		//REFERENCED RESOURCE SELECT LIST
			
			$data['startupList'] = Startup::orderBy('sort_order', 'asc')->lists('startup_name', 'startup_id');
			
			$data['projectStatusList'] = ProjectStatus::orderBy('sort_order', 'asc')->lists('project_status_name', 'project_status_id');
					
			
		if (('assumptions' == $refSlug) && ($refId > 0)){
			// handing route assumptions/{ assumptions }/projects/create
			$data['assumptionList'] = Assumption::where('assumption_id', $refId)->lists('assumption_name', 'assumption_id');
			$data['assumption'] = Assumption::where('assumption_id', $refId)->authed( $this->user_id, $this->user_is_admin, 'view' )->first();
            $data['assumption_id'] = $refId;
		} else {
			$data['assumptionList'] = Assumption::orderBy('sort_order', 'asc')->lists('assumption_name', 'assumption_id');
		}
					
			
		if (('milestones' == $refSlug) && ($refId > 0)){
			// handing route milestones/{ milestones }/projects/create
			$data['milestoneList'] = Milestone::where('milestone_id', $refId)->lists('milestone_name', 'milestone_id');
			$data['milestone'] = Milestone::where('milestone_id', $refId)->authed( $this->user_id, $this->user_is_admin, 'view' )->first();
            $data['milestone_id'] = $refId;
		} else {
			$data['milestoneList'] = Milestone::orderBy('sort_order', 'asc')->lists('milestone_name', 'milestone_id');
		}
					
			
		if (('team-members' == $refSlug) && ($refId > 0)){
			// handing route team-members/{ teamMembers }/projects/create
			$data['teamMemberList'] = TeamMember::where('team_member_id', $refId)->lists('team_member_name', 'team_member_id');
			$data['teamMember'] = TeamMember::where('team_member_id', $refId)->authed( $this->user_id, $this->user_is_admin, 'view' )->first();
            $data['responsible_team_member_id'] = $refId;
		} else {
			$data['teamMemberList'] = TeamMember::orderBy('sort_order', 'asc')->lists('team_member_name', 'team_member_id');
		}
				
		
		
		$data['project'] = $project; 

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'projects.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('projects.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);	
	}


	/**
	 * Project @ store
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($refId=0, $refSlug='')
	{

		$validator = Validator::make(Input::all(), Project::$insertRules);

		if($validator->passes()) {
				$project = new Project;
				$project->fill(Input::all());
			
			
			//FILL GUARDED PARENT ID
			if(($this->user_is_admin) && (Input::has('startup_id'))){ $project->startup_id= Input::get('startup_id'); } 
			//$project->startup_id = $parentId;
					
			//SET DEFAULT VALUES ON CREATE
			if (! $project->sort_order) $project->sort_order = 20;
			

			if($project->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.projects.index')->with('message', 'Project has been created!');				
				} elseif ($this->is_api_url ) {
				    $data['error'] =false;
					$data['message'] = 'Project has been created!';
					$data['url'] = URL::route("api.((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '')projects.index");							

					return json_encode($data);
				}	
				return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'projects.index', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->with('message', 'Project has been created!');					
			}

		}
		$data['error']   = true;
		$data['message']  = 'Please fill required fields below!';
		$data['errors'] = $validator->messages();

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . ((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'projects.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'projects.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
	}
	

	/**
	 * Project @ show
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id, $refId=0, $refSlug='')
	{
		$data = array();

		//ACCESS CONTROL 
		$project = Project::where('project_id', $id)->authed( $this->user_id, $this->user_is_admin, 'show' )
		   ->with('startup', 'projectStatus', 'assumption', 'milestone', 'responsibleTeamMember', 'projectTeamMembers') 
		   ->first();
	
		if (empty($project)){ return Redirect::route('projects.index')->with('message', 'You are not logged in as the owner of this Project.'); }
		if (('assumptions' == $refSlug) && ($refId > 0)){
			// handling route assumptions/{ assumptions }/projects/ { projects }
			$data['assumption'] = Assumption::where('assumption_id', $refId)->authed( $this->user_id, $this->user_is_admin, 'view' )
			  ->first();
		} 
		if (('milestones' == $refSlug) && ($refId > 0)){
			// handling route milestones/{ milestones }/projects/ { projects }
			$data['milestone'] = Milestone::where('milestone_id', $refId)->authed( $this->user_id, $this->user_is_admin, 'view' )
			  ->first();
		} 
		if (('team-members' == $refSlug) && ($refId > 0)){
			// handling route team-members/{ teamMembers }/projects/ { projects }
			$data['teamMember'] = TeamMember::where('team_member_id', $refId)->authed( $this->user_id, $this->user_is_admin, 'view' )
			  ->first();
		} 
			
		$data['project'] = $project;

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'projects.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('projects.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 *  Project @ edit
	 *  Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$project = Project::where('project_id', $id)
		   ->with('startup', 'projectStatus', 'assumption', 'milestone', 'responsibleTeamMember', 'projectTeamMembers') 
		   ->first();

		if (empty($project)){ return Redirect::route('projects.index')->with('message', 'You are not logged in as the owner of this Project.'); }
		
		$data['project'] = $project;

		//REFERENCED RESOURCE SELECT LIST
		$data['startupList'] = Startup::where('startup_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('startup_name', 'startup_id');
		$data['projectStatusList'] = ProjectStatus::where('project_status_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('project_status_name', 'project_status_id');
		$data['assumptionList'] = Assumption::where('startup_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('assumption_name', 'assumption_id');
		$data['milestoneList'] = Milestone::where('milestone_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('milestone_name', 'milestone_id');
		$data['teamMemberList'] = TeamMember::where('startup_id', '>', '-1')->orderBy('sort_order', 'asc')->lists('team_member_name', 'team_member_id');

		//MTM OPTIONS
		//add parent IDs to where clause where appropriate
		//team_members 
		$data['team_members_selected'] = $project->projectTeamMembers()->pluck('xref_project_team_member.team_member_id')->toArray();
 		$data['team_members_selected_count'] = count($data['team_members_selected']);
		$data['team_member_options'] = TeamMember::whereIn('startup_id',[0, $project->startup_id])->orderBy('sort_order' , 'ASC')->lists('team_member_name', 'team_member_id');
					
  		//SET SuccessURL if manually set in url
  		$data['successUrl'] = Input::get('successUrl', '');
  	
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'projects.edit', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('projects.edit', $data);
	}

	/**
	 * Project @ update
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		$project = Project::where('project_id', $id)
		   ->with('startup', 'projectStatus', 'assumption', 'milestone', 'responsibleTeamMember', 'projectTeamMembers') 
		   ->first();
		
		if (empty($project)){ return Redirect::route('projects.index')->with('message', 'You are not logged in as the owner of this Project.'); }
	
		$validator = Validator::make(Input::all(), Project::$updateRules);

		if($validator->passes()) {
			$project->fill(Input::all());

			//FILL GUARDED PARENT ID
			if(($this->user_is_admin) && (Input::has('startup_id'))){ $project->startup_id = Input::get('startup_id'); }
			if($project->save()) {
			
				/**
				 * MTM UPDATES 
				 * for simple xrefs delete and insert
				 * for xrefs tables with data, toggle the is_active flag and insert if doesn't exist
				 *
				 **/ 


	//UPDATE XREF DATA TABLE - teamMembers 
				if(  Input::has('team_members_selected_count') ) {
					$team_members=array();
				 	//DELETE DATA IN simple xref table
				 		ProjectTeamMember::where('project_id', '=', $project->project_id)->delete();
				 	//INSERT DATA to xref table
				 		$team_members_selected_input = Input::get('team_members_selected', array());
				 		if (is_string($team_members_selected_input)) {
				 			$team_members_selected_input = explode(',', $team_members_selected_input);
				  		}
				  		foreach ($team_members_selected_input as $that_xref_id) {
				 			$team_members[] = array('project_id' => $project->project_id, 'team_member_id' => $that_xref_id);
				  		}
				  	//return json_encode($team_members);
				  	ProjectTeamMember::insert($team_members);
				}

				//REDIRECT TO INDEX PAGE
				$success_url = Input::get('success_url', '');
								
				if ($this->is_admin_url ){
					if (!empty ($success_url)) { return Redirect::to($success_url); }
					return Redirect::route('admin.projects.index')->with('messge', 'Project has been updated.');				
				} elseif ($this->is_api_url ) {
					$data = ['error' => false,
						'message' => 'Project has been updated!',
						'url' => URL::route("api.projects.index")							
						];
					if (!empty ($success_url)) {$data['url'] = $success_url; }
					return json_encode($data);
				}
				if (!empty ($success_url)) { return Redirect::to($success_url); } 
				return Redirect::route('projects.show', array($id))->with('message', 'Project has been updated!');
			}
		}

		//RETURN ERRORS
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route('admin.projects.edit', $id)
				->withInput()
				->withErrors($validator)
				->with('message', 'There were validation errors.');			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}

		return Redirect::route('projects.edit', $id)
			->withInput()
			->withErrors($validator)
			->with('message', 'There were validation errors.');

	}


	/**
	 * Project @ destroy
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$project = Project::where('project_id', $id)->first();
		if (empty($project)){ return Redirect::route('projects.index')->with('message', 'You are not logged in as the owner of this Project.'); }
		
		  // MTM delete xref rows: 
			ProjectTeamMember::where('project_id', '=', $project->project_id)->delete();

		/** 
		 * // CHILDREN delete  Where child model = "Jr" 
		 * 
		 * 	$projectJrs = ProjectJr::where('project_id', $id)->get();
		 *	foreach ($projectJrs as $child) {
		 *	  //recursivly delete decendants
		 *	  //$grandchildren = GrandChild::where('parent_id', $child->project_jr_id)->delete();
		 *
		 *	  $child->delete();
		 *  }
		 */

		$project->delete();
		
		//Delete with popup modal and json api response, because DELETE method only available via forms submital.  

		$data = ['error' => false,
				'message' => "Deleted Project(" . $id . ")"
			];
		if ($this->is_admin_url ){
			$data['url']= URL::route("admin.projects.index");
		} elseif ($this->is_api_url ) {
			$data['url']= URL::route("api.projects.index");
		} else {
			$data['url']= URL::route("projects.index");
		}
		//return json string if the views use an ajax call to delete.
		return json_encode($data);
		//return Redirect::route($data['url'])->with('message', 'Project  deleted');
		
	}

	
/**
 * MTM Relationships INDEX and SHOW functions
 * 
 *
 */
	
	public function teamMembersIndex($parentId)
	{
		$project = Project::where('project_id', $parentId)->authed( $this->user_id, $this->user_is_admin, 'show' )->first();

		$teamMembers = $project->projectTeamMembers()
			->get();

		$data=array();
		$data['project'] = $project;
		$data['teamMembers'] = $teamMembers;
		
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'team-members.index-for-projects', $data);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
	}
	return view('team-members.index-for-projects', $data);
	}

	public function teamMembersShow($parentId, $id)
	{
		//ACCESS CONTROL
		$project = Project::where('project_id', $parentId)->authed( $this->user_id, $this->user_is_admin, 'show')->first(); 
		
		//$teamMember = TeamMember::where('team_member_id', $id)->where('project_id',  $project->project_id )->first();
		$teamMember= $project->projectTeamMembers()->where('tbl_team_member.team_member_id', $id)->first();
		
		if (empty($teamMember)){ return Redirect::back()->with('message', 'You are not authorized to see this record for Team Member.'); }
		
		$data['project'] = $project;
		$data['teamMember'] = $teamMember;
		

		if ($this->is_admin_url ){ 
		return view(self::$admin_dir . '.' . 'team-members.view-for-projects', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('team-members.view-for-projects', $data);
	}
	

 /**
 * OTM TABLE INDEX functions (excluding parent, owner - use Parent controller;  and type tables - not usually necessary )
 *
 */
 
			
	public function assumptionIndex($parentId)  // assumptions 
	{  return $this->index($parentId, 'assumptions'); }

	public function assumptionCreate($parentId)  // assumptions 
	{  return $this->create($parentId, 'assumptions'); }
		
	public function assumptionStore($parentId)  // assumptions 
	{  return $this->store($parentId, 'assumptions'); }

	public function assumptionShow($parentId, $id)  // assumptions 
	{  return $this->show($id, $parentId, 'assumptions'); }
	
			
	public function milestoneIndex($parentId)  // milestones 
	{  return $this->index($parentId, 'milestones'); }

	public function milestoneCreate($parentId)  // milestones 
	{  return $this->create($parentId, 'milestones'); }
		
	public function milestoneStore($parentId)  // milestones 
	{  return $this->store($parentId, 'milestones'); }

	public function milestoneShow($parentId, $id)  // milestones 
	{  return $this->show($id, $parentId, 'milestones'); }
	
			
	public function responsibleTeamMemberIndex($parentId)  // teamMembers 
	{  return $this->index($parentId, 'team-members'); }

	public function responsibleTeamMemberCreate($parentId)  // teamMembers 
	{  return $this->create($parentId, 'team-members'); }
		
	public function responsibleTeamMemberStore($parentId)  // teamMembers 
	{  return $this->store($parentId, 'team-members'); }

	public function responsibleTeamMemberShow($parentId, $id)  // teamMembers 
	{  return $this->show($id, $parentId, 'team-members'); }
	

}