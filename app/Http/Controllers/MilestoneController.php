<?php

/** 
 * Controller for Milestone 
 *
 * @package entrack 
 * @author Steve Weathers 
 * @copyright Copyright(c)2016 Steve Weathers.  All Right Reserved.  

 * Portions from CodeWarrior Laravel Template -  Copyright (c)2016 MyProto Technology.  Used by permission.
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Models\Milestone;

use App\Models\StartupMilestone;
use App\Models\Startup;

class MilestoneController extends Controller {
	protected $user_id;
	protected $user_is_admin;
	static $admin_dir = 'admin';
	static $api_dir = 'api';
	var $is_admin_url;
	var $is_api_url;

	public function __construct()
	{
		//IF TABLE HAS OWNER OR PARENT HAS OWNER, REQUIRE USER AUTH
		$this->middleware('auth');
		$this->user_id 			=  Auth::user()->id; //or user_id
		$this->user_is_admin 	= (Auth::user()->role_id == 1) ? true : false;
		$this->is_admin_url     = ((self::$admin_dir == Request::segment(2)) || (self::$admin_dir == Request::segment(1)) ) ? true : false;
		$this->is_api_url     = ((self::$api_dir == Request::segment(2)) || (self::$api_dir == Request::segment(1)) ) ? true : false;
	}


	/**
	 * Milestone @ index
	 * Display a list of Milestones and links for CRUD actions
	 *
	 * @param int $parentId (optional, Used by helper method for filtering by a referenced table. )
	 * @param string $parentSlug (optional, Used by helper method for filtering by a referenced table.)
	 * @return Response
	 */
	public function index($refId=0, $refSlug='')
	{
		$milestones = Milestone::authed( $this->user_id, $this->user_is_admin, 'view' )
		   ->with('startupMilestones') 
		   ->orderBy('sort_order', 'asc') 
		   ->get();
		$data['milestones']=array();
			
		//REFERENCED RESOURCE NAMES
		foreach ($milestones as $row)
		{				
			//get OTM names


			//get MTM names
			$startup_list=$row->startupMilestones()->with('startups')->take(5)->lists('startup_name');
			$row->startup_names =  $startup_list->implode(', ');	
			$data['milestones'][]=$row;	
		} /* end foreach row */
	

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'milestones.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
		} elseif ($this->is_api_url ) { 
			return json_encode($data);
		}
		return view('milestones.index'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 * Milestone @ create
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($refId=0, $refSlug='')
	{
		$data = array();
		$milestone = new Milestone; //MODEL for default values and form model binding
		
		//REFERENCED RESOURCE SELECT LIST
				
		
		
		$data['milestone'] = $milestone; 

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'milestones.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('milestones.create'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);	
	}


	/**
	 * Milestone @ store
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($refId=0, $refSlug='')
	{

		$validator = Validator::make(Input::all(), Milestone::$insertRules);

		if($validator->passes()) {
				$milestone = new Milestone;
				$milestone->fill(Input::all());
			
					
			//SET DEFAULT VALUES ON CREATE
			if (! $milestone->sort_order) $milestone->sort_order = 20;
			

			if($milestone->save()) {

				//SAVE MTM xref relationships (usually not done on create) 

				//REDIRECT TO INDEX PAGE
				
				if ($this->is_admin_url ){
					return Redirect::route('admin.milestones.index')->with('message', 'Milestone has been created!');				
				} elseif ($this->is_api_url ) {
				    $data['error'] =false;
					$data['message'] = 'Milestone has been created!';
					$data['url'] = URL::route("api.((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '')milestones.index");							

					return json_encode($data);
				}	
				return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'milestones.index', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->with('message', 'Milestone has been created!');					
			}

		}
		$data['error']   = true;
		$data['message']  = 'Please fill required fields below!';
		$data['errors'] = $validator->messages();

		if ($this->is_admin_url ){
			return Redirect::route(self::$admin_dir . '.' . ((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'milestones.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return Redirect::route(((('' != $refSlug) && ($refId > 0)) ?  $refSlug . '.' : '').'milestones.create', ((('' != $refSlug) && ($refId > 0)) ? array($refId) : array()))->withInput(Input::all())->withErrors($validator);
	}
	

	/**
	 * Milestone @ show
	 * Display the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function show($id, $refId=0, $refSlug='')
	{
		$data = array();

		//ACCESS CONTROL 
		$milestone = Milestone::where('milestone_id', $id)->authed( $this->user_id, $this->user_is_admin, 'show' )
		   ->with('startupMilestones') 
		   ->first();
	
		if (empty($milestone)){ return Redirect::route('milestones.index')->with('message', 'You are not logged in as the owner of this Milestone.'); }
			
		$data['milestone'] = $milestone;

		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'milestones.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('milestones.view'. ((('' != $refSlug) && ($refId > 0)) ?  '-for-'.$refSlug : ''), $data);
	}

	/**
	 *  Milestone @ edit
	 *  Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function edit($id)
	{
		$milestone = Milestone::where('milestone_id', $id)
		   ->with('startupMilestones') 
		   ->first();

		if (empty($milestone)){ return Redirect::route('milestones.index')->with('message', 'You are not logged in as the owner of this Milestone.'); }
		
		$data['milestone'] = $milestone;

		//REFERENCED RESOURCE SELECT LIST

		//MTM OPTIONS
		//add parent IDs to where clause where appropriate
		//startups 
		$data['startups_selected'] = $milestone->startupMilestones()->where('xref_startup_milestone.is_active', '>', 0)->pluck('xref_startup_milestone.startup_id')->toArray();
 		$data['startups_selected_count'] = count($data['startups_selected']);
		$data['startup_options'] = Startup::where('startup_id', '>', '-1')->orderBy('sort_order' , 'ASC')->lists('startup_name', 'startup_id');					

  		//SET SuccessURL if manually set in url
  		$data['successUrl'] = Input::get('successUrl', '');
  	
		if ($this->is_admin_url ){
			return view(self::$admin_dir . '.' . 'milestones.edit', $data);			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}
		return view('milestones.edit', $data);
	}

	/**
	 * Milestone @ update
	 * Update the specified resource in storage.
	 *
	 * @param int $id
	 * @return Response
	 */
	public function update($id)
	{
		$milestone = Milestone::where('milestone_id', $id)
		   ->with('startupMilestones') 
		   ->first();
		
		if (empty($milestone)){ return Redirect::route('milestones.index')->with('message', 'You are not logged in as the owner of this Milestone.'); }
	
		$validator = Validator::make(Input::all(), Milestone::$updateRules);

		if($validator->passes()) {
			$milestone->fill(Input::all());

			if($milestone->save()) {
			
				/**
				 * MTM UPDATES 
				 * for simple xrefs delete and insert
				 * for xrefs tables with data, toggle the is_active flag and insert if doesn't exist
				 *
				 **/ 


	//UPDATE startups - XREF DATA TABLE 
				if(  Input::has('startups_selected_count') ) {
					$xref_add = array();
					$xref_enable = array();
					//GET OLD ACTIVE ROWS	
					                                               
					$xref_existing = $milestone->startupMilestones()->select('tbl_startup.startup_id')->lists('startup_id');		
					//GET SELECTIONS FROM FORM INPUT
					$xref_selected_input = Input::get('startups_selected', array());
					
					if (is_string($xref_selected_input)) { $xref_selected_input = explode(',', $xref_selected_input); }
					if (is_array($xref_selected_input)) {
						foreach ($xref_selected_input as $that_xref_id) {
							if ($xref_existing->contains($that_xref_id)) {
								//ENABLE EXISTING ROWS
								$xref_enable[] = $that_xref_id;
							} elseif ($that_xref_id >0) {
								//CREATE NEW ROWS
								$xref_add[] = array('milestone_id' => $milestone->milestone_id, 'startup_id' => $that_xref_id, 'is_active' => 1);
							}
						}
					}	
	
					//TURN OFF ALL ROWS
					StartupMilestone::where('milestone_id', '=', $milestone->milestone_id)->update(array('is_active' => 0));
					
					//TURN ON SELECTED ROWS
					StartupMilestone::where('milestone_id', '=', $milestone->milestone_id)->whereIn('startup_id', $xref_enable)->update(array('is_active' => 1));
							
					//ADD NEW ROWS
					if (count($xref_add) > 0){ StartupMilestone::insert($xref_add); }
				}

				//REDIRECT TO INDEX PAGE
				$success_url = Input::get('success_url', '');
								
				if ($this->is_admin_url ){
					if (!empty ($success_url)) { return Redirect::to($success_url); }
					return Redirect::route('admin.milestones.index')->with('messge', 'Milestone has been updated.');				
				} elseif ($this->is_api_url ) {
					$data = ['error' => false,
						'message' => 'Milestone has been updated!',
						'url' => URL::route("api.milestones.index")							
						];
					if (!empty ($success_url)) {$data['url'] = $success_url; }
					return json_encode($data);
				}
				if (!empty ($success_url)) { return Redirect::to($success_url); } 
				return Redirect::route('milestones.show', array($id))->with('message', 'Milestone has been updated!');
			}
		}

		//RETURN ERRORS
		$data = ['error'   => true,
			'message' => 'Please fill required fields below!',
			'errors' => $validator->messages()
			];

		if ($this->is_admin_url ){
			return Redirect::route('admin.milestones.edit', $id)
				->withInput()
				->withErrors($validator)
				->with('message', 'There were validation errors.');			
		} elseif ($this->is_api_url ) {
			return json_encode($data);
		}

		return Redirect::route('milestones.edit', $id)
			->withInput()
			->withErrors($validator)
			->with('message', 'There were validation errors.');

	}


	/**
	 * Milestone @ destroy
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		
		$milestone = Milestone::where('milestone_id', $id)->first();
		if (empty($milestone)){ return Redirect::route('milestones.index')->with('message', 'You are not logged in as the owner of this Milestone.'); }
		
		  // MTM delete xref rows: 
			StartupMilestone::where('milestone_id', '=', $milestone->milestone_id)->delete();

		/** 
		 * // CHILDREN delete  Where child model = "Jr" 
		 * 
		 * 	$milestoneJrs = MilestoneJr::where('milestone_id', $id)->get();
		 *	foreach ($milestoneJrs as $child) {
		 *	  //recursivly delete decendants
		 *	  //$grandchildren = GrandChild::where('parent_id', $child->milestone_jr_id)->delete();
		 *
		 *	  $child->delete();
		 *  }
		 */

		$milestone->delete();
		
		//Delete with popup modal and json api response, because DELETE method only available via forms submital.  

		$data = ['error' => false,
				'message' => "Deleted Milestone(" . $id . ")"
			];
		if ($this->is_admin_url ){
			$data['url']= URL::route("admin.milestones.index");
		} elseif ($this->is_api_url ) {
			$data['url']= URL::route("api.milestones.index");
		} else {
			$data['url']= URL::route("milestones.index");
		}
		//return json string if the views use an ajax call to delete.
		return json_encode($data);
		//return Redirect::route($data['url'])->with('message', 'Milestone  deleted');
		
	}

	
/**
 * MTM Relationships INDEX and SHOW functions
 * 
 *
 */
	

 /**
 * OTM TABLE INDEX functions (excluding parent, owner - use Parent controller;  and type tables - not usually necessary )
 *
 */
 

}