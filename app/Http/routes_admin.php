<?php
/** 
 * Routes for REST MVC CRUD Controllers
 * 
 * @package  entrack
 * @author  Steve Weathers
 * @copyright  Copyright(c)2016 Steve Weathers.  All Right Reserved. 
 * 
 */
 
 		Route::resource('startups', 'StartupController');
 		Route::resource('team-members', 'TeamMemberController');
 		//Route::get('/team-members/{team-members}/projects', array('as' => 'team-members.projects', 'uses' => 'TeamMemberController@projectsIndex'));
 		//Route::get('/team-members/{team-members}/projects/{projects}', array('as' => 'team-members.projects.show', 'uses' => 'TeamMemberController@projectsShow'));
 		Route::resource('startups.team-members', 'StartupTeamMemberController');
 		Route::resource('milestones', 'MilestoneController');
 		Route::resource('assumptions', 'AssumptionController');
 		Route::resource('startups.assumptions', 'StartupAssumptionController');
 		Route::resource('projects', 'ProjectController');
 		//Route::get('/projects/{projects}/team-members', array('as' => 'projects.team-members', 'uses' => 'ProjectController@teamMembersIndex'));
 		//Route::get('/projects/{projects}/team-members/{team-members}', array('as' => 'projects.team-members.show', 'uses' => 'ProjectController@teamMembersShow'));
 		   //Route::get ('/assumptions/{assumptions}/projects', array('as' => 'assumptions.projects.index',  'uses' => 'ProjectController@assumptionIndex'));
 		   //Route::get ('/assumptions/{assumptions}/projects/create', array('as' => 'assumptions.projects.create', 'uses' => 'ProjectController@assumptionCreate'));
 		   //Route::post('/assumptions/{assumptions}/projects/store', array('as' => 'assumptions.projects.store',  'uses' => 'ProjectController@assumptionStore'));
 		   //Route::get ('/assumptions/{assumptions}/projects/{projects}', array('as' => 'assumptions.projects.show', 'uses' => 'ProjectController@assumptionShow'));
 		   //Route::get ('/milestones/{milestones}/projects', array('as' => 'milestones.projects.index',  'uses' => 'ProjectController@milestoneIndex'));
 		   //Route::get ('/milestones/{milestones}/projects/create', array('as' => 'milestones.projects.create', 'uses' => 'ProjectController@milestoneCreate'));
 		   //Route::post('/milestones/{milestones}/projects/store', array('as' => 'milestones.projects.store',  'uses' => 'ProjectController@milestoneStore'));
 		   //Route::get ('/milestones/{milestones}/projects/{projects}', array('as' => 'milestones.projects.show', 'uses' => 'ProjectController@milestoneShow'));
 		   //Route::get ('/responsible-team-members/{teamMembers}/projects', array('as' => 'team-members.projects.index',  'uses' => 'ProjectController@responsibleTeamMemberIndex'));
 		   //Route::get ('/responsible-team-members/{teamMembers}/projects/create', array('as' => 'team-members.projects.create', 'uses' => 'ProjectController@responsibleTeamMemberCreate'));
 		   //Route::post('/responsible-team-members/{teamMembers}/projects/store', array('as' => 'team-members.projects.store',  'uses' => 'ProjectController@responsibleTeamMemberStore'));
 		   //Route::get ('/responsible-team-members/{teamMembers}/projects/{projects}', array('as' => 'team-members.projects.show', 'uses' => 'ProjectController@responsibleTeamMemberShow'));
 		Route::resource('startups.projects', 'StartupProjectController');
 		Route::resource('team-relationships', 'TeamRelationshipController');
 		Route::resource('team-roles', 'TeamRoleController');
 		Route::resource('project-statuses', 'ProjectStatusController');
 		Route::resource('startup-milestones', 'StartupMilestoneController');
 		Route::resource('startups.startup-milestones', 'StartupStartupMilestoneController');
 		Route::resource('milestones.startup-milestones', 'MilestoneStartupMilestoneController');
 		Route::resource('user-profiles', 'UserProfileController');
