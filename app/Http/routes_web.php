<?php
/** 
 * Routes for REST MVC CRUD Controllers
 * 
 * @package  entrack
 * @author  Steve Weathers
 * @copyright  Copyright(c)2016 Steve Weathers.  All Right Reserved. 
 * 
 */
	
	Route::resource('/startups', 'StartupController');
	Route::resource('/milestones', 'MilestoneController');
	Route::resource('/assumptions', 'AssumptionController');
	Route::resource('/projects', 'ProjectController');
	Route::resource('/startups.team-members', 'StartupTeamMemberController');
	Route::resource('/startups.assumptions', 'StartupAssumptionController');
	Route::resource('/startups.projects', 'StartupProjectController');
	Route::resource('/team-roles', 'TeamRoleController');
	Route::resource('/team-members', 'TeamMemberController');
	Route::resource('/startup-milestones', 'StartupMilestoneController');
	Route::resource('startups.startup-milestones', 'StartupStartupMilestoneController');
	Route::resource('project-statuses', 'ProjectStatusController');
	Route::resource('user-profiles', 'UserProfileController');
	Route::auth();
    	


