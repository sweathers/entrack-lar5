<?php
/** 
 * Model for Assumption
 * 
 * @package  entrack
 * @author  Steve Weathers
 * @copyright  Copyright(c)2016 Steve Weathers.  All Right Reserved. 
 * 
 * Portions from CodeWarrior Laravel Template -  Copyright (c)2015 Steve Weathers Technology.  Used by permission.  Used by permission.  
 * 
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Assumption extends Model {

	/**
	 * Database table used by this model
	 * @var  string
	 */
	protected $table = 'tbl_assumption';

	/**
	 * primary key of the table
	 * @var  string
	 */
	protected $primaryKey = 'assumption_id';
    
	protected $nameField = 'assumption_name';

	protected $sortField = 'sort_order';    
	
	/**
	 * Disable Auto Timestamps
	 */
	public $timestamps = false;
		
	/**
	 * Fillable DB Fields
	 * Secure fields like ID, owner, parent should be manually set by the controller
	 */

	protected $fillable = [
   		
		//'assumption_id', //ID
    			
		'assumption_name', //NAME
     		
		//'startup_id', //PARENT
    			
		'assumption_description', //TEXTBLOCK
  			
		'test', //TEXTBLOCK
  			
		'result', //TEXTBLOCK
  			
		'pivot_description', //TEXTBLOCK
    			
		'is_validated', //BOOLEAN
  			
		'cause_pivot', //BOOLEAN
    			
		'sort_order', //SORT
  	];

	/**
	 * Attributes defines the default values to be set in new objects
	 * 
	 */
	
	protected $attributes = array(
 		'sort_order' => '20' 
);

	/**
	 * Validation Rules for create
	 * @var  array
	 */
	public static $insertRules = [
		// 'assumption_id'		=> 'required|integer',	//ID
   
	
		'assumption_name'		=> 'required|max:64',	 
   		// 'startup_id'		=> 'required|integer',	//PARENT
            	];


	/**
	 * Validation Rules for edit
	 * @var  array
	 */
	public static $updateRules = [
		//'assumption_id'	=> 'required|integer',   //ID
    		//'startup_id'	=> 'required|integer',   //PARENT
  		'assumption_description'		=> 'max:65565',
 		'test'		=> 'max:65565',
 		'result'		=> 'max:65565',
 		'pivot_description'		=> 'max:65565',
  		'is_validated'		=> 'integer',
 		'cause_pivot'		=> 'integer',
  		'sort_order'		=> 'integer',	
   	
	];

		/**
	 * Search for entities where the name field or a searchable text fields matches the search string
	 * prefixing the name with "scope" makes this function available for query chains 
	 *
	 * usage: Assumption::select('assumption_id','assumption_name','sort_order')->assumptionName( $search_query )->get(); 		     
	 */
    public function scopeAssumptionName($query, $search)
    {
        return $query->where('assumption_name', 'like', '%'.$search.'%')
            		;

    }
    
        
    /**
	 * ->authed()
	 * Filter the query by only the resources the user is authorized to view, edit or delete
	 * prefixing the name with "scope" makes this function available for query chains 
	 * 
	 * usage: $assumption = Assumption::select(..)->authed( $this->user_id, $this->user_is_admin, 'edit')->get(); 		     
	 * 
	 */
	public function scopeAuthed($query, $user_id, $user_is_admin=false, $action='w')
	{
		/** Admin users always have all permissions **/
		
		if ($user_is_admin){ return $query;  }
		switch ($action) {
			/** Execute / Assign / Delete actions are generally restricted to the owner **/
			case 'x':
			case 'execute':
			case 'delete':
			case 'destroy':
			case 'assign':
				return $query
					;
				break;				
		
			/** Write / Edit / Update / Create Children are restricted to owner and managers **/
			case 'w':
			case 'write':
			case 'edit':
			case 'update':
			case 'read-private':
				return $query
					//Table Managers Join
					//->join('xref_employer_manager', 'xref_employer_manager.employer_id', '=', 'employer.id')
					//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					;
				break;
			
			/** Read / View are sometimes open to the world, other times they are restricted to a specific audience.  **/
			case 'r':  
			case 'read': 
			case 'view':
			case 'show':
			case 'view-public':
				return $query
					//Table Subscribers Join
					//->join('xref_employer_subscriber', 'xref_employer_subscriber.employer_id', '=', 'employer.id')
						//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					//Public - no filtering
				; 
				break;
				
		}
		return $query;
	}
    
	 
		
    /**
	 * Filter the query to show only the children of the designated parent
	 * prefixing the name with "scope" makes this function available for query chains 
	 * usage: $assumption = Assumption::select(..)->childrenOf( $parent_id)->get(); 		     
	 */
	public function scopeChildrenOf($parent_id)
	{
		return $query->where($startup_id, $parent_id );
	}
	     
    /** 
     * FIND ASSOCIATED OTM TABLES
     * Return the specific one-to-many object referenced by this object
     * usage: foreach ($query_results_collection as $row) {... $row->'that_type_name' => @$row->thatType->that_type_name, ... } 
     * 
     */
   
    
    
  	
	public function startup()
    {
    	return $this->belongsTo('App\Models\Startup', 'startup_id' );
    }
      
    
    
 	 




	 /** 
	  * FIND OTHER REFERENCES 
	  */
 
	 public function projectAssumptions()
	{
	 	return $this->hasMany('App\Models\Project', 'assumption_id', 'assumption_id');
	 } 
	 
 	

   
}