<?php
/** 
 * Model for Team Role
 * 
 * @package  entrack
 * @author  Steve Weathers
 * @copyright  Copyright(c)2016 Steve Weathers.  All Right Reserved. 
 * 
 * Portions from CodeWarrior Laravel Template -  Copyright (c)2015 Steve Weathers Technology.  Used by permission.  Used by permission.  
 * 
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TeamRole extends Model {

	/**
	 * Database table used by this model
	 * @var  string
	 */
	protected $table = 'tbl_team_role_type';

	/**
	 * primary key of the table
	 * @var  string
	 */
	protected $primaryKey = 'team_role_id';
    
	protected $nameField = 'team_role_name';

	protected $sortField = 'sort_order';    
	
	/**
	 * Disable Auto Timestamps
	 */
	public $timestamps = false;
		
	/**
	 * Fillable DB Fields
	 * Secure fields like ID, owner, parent should be manually set by the controller
	 */

	protected $fillable = [
   		
		//'team_role_id', //ID
    			
		'team_role_name', //NAME
    			
		'team_role_code', //CODE
    			
		'is_default', //BOOLEAN
    			
		'score', //INT
    			
		'sort_order', //SORT
  	];

	/**
	 * Attributes defines the default values to be set in new objects
	 * 
	 */
	
	protected $attributes = array(
 		'sort_order' => '20' 
);

	/**
	 * Validation Rules for create
	 * @var  array
	 */
	public static $insertRules = [
		// 'team_role_id'		=> 'required|integer',	//ID
   
	
		'team_role_name'		=> 'required|max:64',	 
   		// 'team_role_code'		=> 'required|integer',	//CODE
        	];


	/**
	 * Validation Rules for edit
	 * @var  array
	 */
	public static $updateRules = [
		//'team_role_id'	=> 'required|integer',   //ID
    		'team_role_code'		=> 'required|max:16',	
  		'is_default'		=> 'integer',
  		'score'		=> 'integer',	
   		'sort_order'		=> 'integer',	
   	
	];

		/**
	 * Search for entities where the name field or a searchable text fields matches the search string
	 * prefixing the name with "scope" makes this function available for query chains 
	 *
	 * usage: TeamRole::select('team_role_id','team_role_name','sort_order')->teamRoleName( $search_query )->get(); 		     
	 */
    public function scopeTeamRoleName($query, $search)
    {
        return $query->where('team_role_name', 'like', '%'.$search.'%')
      
                  	->orWhere('team_role_code', 'like', '%'.$search.'%') 
         		;

    }
    
        
    /**
	 * ->authed()
	 * Filter the query by only the resources the user is authorized to view, edit or delete
	 * prefixing the name with "scope" makes this function available for query chains 
	 * 
	 * usage: $teamRole = TeamRole::select(..)->authed( $this->user_id, $this->user_is_admin, 'edit')->get(); 		     
	 * 
	 */
	public function scopeAuthed($query, $user_id, $user_is_admin=false, $action='w')
	{
		/** Admin users always have all permissions **/
		
		if ($user_is_admin){ return $query;  }
		switch ($action) {
			/** Execute / Assign / Delete actions are generally restricted to the owner **/
			case 'x':
			case 'execute':
			case 'delete':
			case 'destroy':
			case 'assign':
				return $query
					;
				break;				
		
			/** Write / Edit / Update / Create Children are restricted to owner and managers **/
			case 'w':
			case 'write':
			case 'edit':
			case 'update':
			case 'read-private':
				return $query
					//Table Managers Join
					//->join('xref_employer_manager', 'xref_employer_manager.employer_id', '=', 'employer.id')
					//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					;
				break;
			
			/** Read / View are sometimes open to the world, other times they are restricted to a specific audience.  **/
			case 'r':  
			case 'read': 
			case 'view':
			case 'show':
			case 'view-public':
				return $query
					//Table Subscribers Join
					//->join('xref_employer_subscriber', 'xref_employer_subscriber.employer_id', '=', 'employer.id')
						//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					//Public - no filtering
				; 
				break;
				
		}
		return $query;
	}
    
	 
	    
    /** 
     * FIND ASSOCIATED OTM TABLES
     * Return the specific one-to-many object referenced by this object
     * usage: foreach ($query_results_collection as $row) {... $row->'that_type_name' => @$row->thatType->that_type_name, ... } 
     * 
     */
   
    
    
    
    
    
 	 




	

    /**
     * getTypeCodes 
     * returns an array that matches type ids to type codes and type codes to type ids
     * this enables us to keep auto-increment ids out of the source code, but keep the db normalized and efficient. 
     * for type tables only.
     *
     */

    public static function getTypeCodes()
    {
    	$typeList = TeamRole::select('team_role_id as id', 'team_role_code as code')->get();
    
    	$typeCodes = array();
    	foreach ($typeList as $type){
    		$typeCodes[($type['id'])] = $type['code'];
    		$typeCodes[($type['code'])] = $type['id'];
    	}
    	return $typeCodes;
    }
    
    /**
     * getTypesById
     * returns a list of all types accessible by id
     *
     */
    
    public static function getTypesById()
    {  	
    	$typeList = TeamRole::all(); 
    	$typeById = array();
    	foreach ($typeList as $type){
    		$typeById[ ($type['team_role_id']) ] = $type;
    	}
    	return $typeById;
    }
    
   
}