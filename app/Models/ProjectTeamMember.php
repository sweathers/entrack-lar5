<?php
/** 
 * Model for Project Team Member
 * 
 * @package  entrack
 * @author  Steve Weathers
 * @copyright  Copyright(c)2016 Steve Weathers.  All Right Reserved. 
 * 
 * Portions from CodeWarrior Laravel Template -  Copyright (c)2015 Steve Weathers Technology.  Used by permission.  Used by permission.  
 * 
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectTeamMember extends Model {

	/**
	 * Database table used by this model
	 * @var  string
	 */
	protected $table = 'xref_project_team_member';

	/**
	 * primary key of the table
	 * @var  string
	 */
	protected $primaryKey = '';
    
	protected $nameField = '';

	protected $sortField = '';    
	
	/**
	 * Disable Auto Timestamps
	 */
	public $timestamps = false;
		
	/**
	 * Fillable DB Fields
	 * Secure fields like ID, owner, parent should be manually set by the controller
	 */

	protected $fillable = [
  			
		'project_id', //XREFID
  			
		'team_member_id', //XREFID
  	];

	/**
	 * Attributes defines the default values to be set in new objects
	 * 
	 */
	
	protected $attributes = array(
 		 
);

	/**
	 * Validation Rules for create
	 * @var  array
	 */
	public static $insertRules = [
 
		'project_id'		=> 'required|integer',	
   
		'team_member_id'		=> 'required|integer',	
   	];


	/**
	 * Validation Rules for edit
	 * @var  array
	 */
	public static $updateRules = [
		//'project_id'	=> 'required|integer',   //XREFID
 		//'team_member_id'	=> 'required|integer',   //XREFID
  	
	];

	    
    /**
	 * ->authed()
	 * Filter the query by only the resources the user is authorized to view, edit or delete
	 * prefixing the name with "scope" makes this function available for query chains 
	 * 
	 * usage: $projectTeamMember = ProjectTeamMember::select(..)->authed( $this->user_id, $this->user_is_admin, 'edit')->get(); 		     
	 * 
	 */
	public function scopeAuthed($query, $user_id, $user_is_admin=false, $action='w')
	{
		/** Admin users always have all permissions **/
		
		if ($user_is_admin){ return $query;  }
		switch ($action) {
			/** Execute / Assign / Delete actions are generally restricted to the owner **/
			case 'x':
			case 'execute':
			case 'delete':
			case 'destroy':
			case 'assign':
				return $query
					;
				break;				
		
			/** Write / Edit / Update / Create Children are restricted to owner and managers **/
			case 'w':
			case 'write':
			case 'edit':
			case 'update':
			case 'read-private':
				return $query
					//Table Managers Join
					//->join('xref_employer_manager', 'xref_employer_manager.employer_id', '=', 'employer.id')
					//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					;
				break;
			
			/** Read / View are sometimes open to the world, other times they are restricted to a specific audience.  **/
			case 'r':  
			case 'read': 
			case 'view':
			case 'show':
			case 'view-public':
				return $query
					//Table Subscribers Join
					//->join('xref_employer_subscriber', 'xref_employer_subscriber.employer_id', '=', 'employer.id')
						//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					//Public - no filtering
				; 
				break;
				
		}
		return $query;
	}
    
	 
	    
    /** 
     * FIND ASSOCIATED OTM TABLES
     * Return the specific one-to-many object referenced by this object
     * usage: foreach ($query_results_collection as $row) {... $row->'that_type_name' => @$row->thatType->that_type_name, ... } 
     * 
     */
   
  	
	public function project()
    {
    	return $this->belongsTo('App\Models\Project', 'project_id' );
    }
  	
	public function teamMember()
    {
    	return $this->belongsTo('App\Models\TeamMember', 'team_member_id' );
    }
   	 




	

   
}