<?php
/** 
 * Model for Startup
 * 
 * @package  entrack
 * @author  Steve Weathers
 * @copyright  Copyright(c)2016 Steve Weathers.  All Right Reserved. 
 * 
 * Portions from CodeWarrior Laravel Template -  Copyright (c)2015 Steve Weathers Technology.  Used by permission.  Used by permission.  
 * 
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Startup extends Model {

	/**
	 * Database table used by this model
	 * @var  string
	 */
	protected $table = 'tbl_startup';

	/**
	 * primary key of the table
	 * @var  string
	 */
	protected $primaryKey = 'startup_id';
    
	protected $nameField = 'startup_name';

	protected $sortField = 'sort_order';    
	
	/**
	 * Disable Auto Timestamps
	 */
	public $timestamps = false;
		
	/**
	 * Fillable DB Fields
	 * Secure fields like ID, owner, parent should be manually set by the controller
	 */

	protected $fillable = [
   		
		//'startup_id', //ID
    			
		'startup_name', //NAME
    			
		'location_place_id', //TYPETABLE
     		
		//'user_id', //OWNER
    			
		'date_started', //DATETIME
    			
		'about', //TEXTBLOCK
    			
		'target_industry', //TEXT_255
    			
		'capital_raised', //DECIMAL
    			
		'sort_order', //SORT
  	];

	/**
	 * Attributes defines the default values to be set in new objects
	 * 
	 */
	
	protected $attributes = array(
 		'sort_order' => '20' 
);

	/**
	 * Validation Rules for create
	 * @var  array
	 */
	public static $insertRules = [
		// 'startup_id'		=> 'required|integer',	//ID
   
	
		'startup_name'		=> 'required|max:64',	 
     		// 'user_id'		=> 'required|integer',	//OWNER
            	];


	/**
	 * Validation Rules for edit
	 * @var  array
	 */
	public static $updateRules = [
		//'startup_id'	=> 'required|integer',   //ID
    		'location_place_id'		=> 'integer',	
   		//'user_id'	=> 'required|integer',   //OWNER
    		'about'		=> 'max:65565',
  		'target_industry'		=> 'max:255',
  		'capital_raised'		=> 'numeric',	
   		'sort_order'		=> 'integer',	
   	
	];

		/**
	 * Search for entities where the name field or a searchable text fields matches the search string
	 * prefixing the name with "scope" makes this function available for query chains 
	 *
	 * usage: Startup::select('startup_id','startup_name','sort_order')->startupName( $search_query )->get(); 		     
	 */
    public function scopeStartupName($query, $search)
    {
        return $query->where('startup_name', 'like', '%'.$search.'%')
                  		;

    }
    
        
    /**
	 * ->authed()
	 * Filter the query by only the resources the user is authorized to view, edit or delete
	 * prefixing the name with "scope" makes this function available for query chains 
	 * 
	 * usage: $startup = Startup::select(..)->authed( $this->user_id, $this->user_is_admin, 'edit')->get(); 		     
	 * 
	 */
	public function scopeAuthed($query, $user_id, $user_is_admin=false, $action='w')
	{
		/** Admin users always have all permissions **/
		
		if ($user_is_admin){ return $query;  }
		switch ($action) {
			/** Execute / Assign / Delete actions are generally restricted to the owner **/
			case 'x':
			case 'execute':
			case 'delete':
			case 'destroy':
			case 'assign':
				return $query
	
					//Table Owner Where
					->where('tbl_startup.' . 'user_id', $user_id)
					;
				break;				
		
			/** Write / Edit / Update / Create Children are restricted to owner and managers **/
			case 'w':
			case 'write':
			case 'edit':
			case 'update':
			case 'read-private':
				return $query
					//Table Managers Join
					//->join('xref_employer_manager', 'xref_employer_manager.employer_id', '=', 'employer.id')
	
					//Table Owner Where
					->where('tbl_startup.' . 'user_id', $user_id )
					//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					;
				break;
			
			/** Read / View are sometimes open to the world, other times they are restricted to a specific audience.  **/
			case 'r':  
			case 'read': 
			case 'view':
			case 'show':
			case 'view-public':
				return $query
					//Table Subscribers Join
					//->join('xref_employer_subscriber', 'xref_employer_subscriber.employer_id', '=', 'employer.id')
		
					//Table Owner Where must be added to subsriber list
					//->where('tbl_startup.' . 'user_id', $user_id )
						//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					//Public - no filtering
				; 
				break;
				
		}
		
		return $query->where('user_id', $user_id );
	}
    
	 
	    
    /** 
     * FIND ASSOCIATED OTM TABLES
     * Return the specific one-to-many object referenced by this object
     * usage: foreach ($query_results_collection as $row) {... $row->'that_type_name' => @$row->thatType->that_type_name, ... } 
     * 
     */
   
    
    
  
	public function locationPlace()  // locationPlaces
    {
    	return $this->belongsTo('App\Models\LocationPlace', 'location_place_id' );
    }
      
	public function user()
    {
    	return $this->belongsTo('App\Models\UserProfile', 'user_id' );
    }	
	
	public function userProfile()
    {
    	return $this->belongsTo('App\Models\UserProfile', 'user_id' );
    }
    
    
    
    
    
    
    
 	 

	    
    /** 
     * FIND ASSOCIATED MTM TABLES
     * Return a collction of objects that are referenced through an xref table
     * usage:  $widget_list=@$row->widgetAttributes()->select('tbl_attribute.attribute_name')->lists('attribute_name');
     * example assumes this model=Widget and that object = Attribute
     * 
     **/
	 
	 
	 public function startupMilestones()
	 {
 	 	 return $this->belongsToMany('App\Models\Milestone', 'xref_startup_milestone', 'startup_id', 'milestone_id');
	 }	
 


	 /** 
	  * FIND CHILDREN 
	  */
 
	 public function startupTeamMembers()
	 {
	 	return $this->hasMany('App\Models\TeamMember', 'startup_id', 'startup_id');
	 } 
	 
  
	 public function startupAssumptions()
	 {
	 	return $this->hasMany('App\Models\Assumption', 'startup_id', 'startup_id');
	 } 
	 
  
	 public function startupProjects()
	 {
	 	return $this->hasMany('App\Models\Project', 'startup_id', 'startup_id');
	 } 
	 
 
	

   
}