<?php
/** 
 * Model for Milestone
 * 
 * @package  entrack
 * @author  Steve Weathers
 * @copyright  Copyright(c)2016 Steve Weathers.  All Right Reserved. 
 * 
 * Portions from CodeWarrior Laravel Template -  Copyright (c)2015 Steve Weathers Technology.  Used by permission.  Used by permission.  
 * 
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Milestone extends Model {

	/**
	 * Database table used by this model
	 * @var  string
	 */
	protected $table = 'tbl_milestone';

	/**
	 * primary key of the table
	 * @var  string
	 */
	protected $primaryKey = 'milestone_id';
    
	protected $nameField = 'milestone_name';

	protected $sortField = 'sort_order';    
	
	/**
	 * Disable Auto Timestamps
	 */
	public $timestamps = false;
		
	/**
	 * Fillable DB Fields
	 * Secure fields like ID, owner, parent should be manually set by the controller
	 */

	protected $fillable = [
   		
		//'milestone_id', //ID
    			
		'milestone_name', //NAME
    			
		'description', //TEXTBLOCK
  			
		'helpful_information', //TEXTBLOCK
  			
		'helpful_person', //TEXTBLOCK
  			
		'complete_descr', //TEXTBLOCK
    			
		'score', //INT
    			
		'sort_order', //SORT
  	];

	/**
	 * Attributes defines the default values to be set in new objects
	 * 
	 */
	
	protected $attributes = array(
 		'sort_order' => '20' 
);

	/**
	 * Validation Rules for create
	 * @var  array
	 */
	public static $insertRules = [
		// 'milestone_id'		=> 'required|integer',	//ID
   
	
		'milestone_name'		=> 'required|max:64',	 
            	];


	/**
	 * Validation Rules for edit
	 * @var  array
	 */
	public static $updateRules = [
		//'milestone_id'	=> 'required|integer',   //ID
    		'description'		=> 'max:65565',
 		'helpful_information'		=> 'max:65565',
 		'helpful_person'		=> 'max:65565',
 		'complete_descr'		=> 'max:65565',
  		'score'		=> 'integer',	
   		'sort_order'		=> 'integer',	
   	
	];

		/**
	 * Search for entities where the name field or a searchable text fields matches the search string
	 * prefixing the name with "scope" makes this function available for query chains 
	 *
	 * usage: Milestone::select('milestone_id','milestone_name','sort_order')->milestoneName( $search_query )->get(); 		     
	 */
    public function scopeMilestoneName($query, $search)
    {
        return $query->where('milestone_name', 'like', '%'.$search.'%')
          		;

    }
    
        
    /**
	 * ->authed()
	 * Filter the query by only the resources the user is authorized to view, edit or delete
	 * prefixing the name with "scope" makes this function available for query chains 
	 * 
	 * usage: $milestone = Milestone::select(..)->authed( $this->user_id, $this->user_is_admin, 'edit')->get(); 		     
	 * 
	 */
	public function scopeAuthed($query, $user_id, $user_is_admin=false, $action='w')
	{
		/** Admin users always have all permissions **/
		
		if ($user_is_admin){ return $query;  }
		switch ($action) {
			/** Execute / Assign / Delete actions are generally restricted to the owner **/
			case 'x':
			case 'execute':
			case 'delete':
			case 'destroy':
			case 'assign':
				return $query
					;
				break;				
		
			/** Write / Edit / Update / Create Children are restricted to owner and managers **/
			case 'w':
			case 'write':
			case 'edit':
			case 'update':
			case 'read-private':
				return $query
					//Table Managers Join
					//->join('xref_employer_manager', 'xref_employer_manager.employer_id', '=', 'employer.id')
					//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					;
				break;
			
			/** Read / View are sometimes open to the world, other times they are restricted to a specific audience.  **/
			case 'r':  
			case 'read': 
			case 'view':
			case 'show':
			case 'view-public':
				return $query
					//Table Subscribers Join
					//->join('xref_employer_subscriber', 'xref_employer_subscriber.employer_id', '=', 'employer.id')
						//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					//Public - no filtering
				; 
				break;
				
		}
		return $query;
	}
    
	 
	    
    /** 
     * FIND ASSOCIATED OTM TABLES
     * Return the specific one-to-many object referenced by this object
     * usage: foreach ($query_results_collection as $row) {... $row->'that_type_name' => @$row->thatType->that_type_name, ... } 
     * 
     */
   
    
    
    
    
 	 

	    
    /** 
     * FIND ASSOCIATED MTM TABLES
     * Return a collction of objects that are referenced through an xref table
     * usage:  $widget_list=@$row->widgetAttributes()->select('tbl_attribute.attribute_name')->lists('attribute_name');
     * example assumes this model=Widget and that object = Attribute
     * 
     **/
	 
	 
	 public function startupMilestones()
	 {
 	 	 return $this->belongsToMany('App\Models\Startup', 'xref_startup_milestone', 'milestone_id', 'startup_id');
	 }	
 



	 /** 
	  * FIND OTHER REFERENCES 
	  */
 
	 public function projectMilestones()
	{
	 	return $this->hasMany('App\Models\Project', 'milestone_id', 'milestone_id');
	 } 
	 
 	

   
}