<?php
/** 
 * Model for Startup Milestone
 * 
 * @package  entrack
 * @author  Steve Weathers
 * @copyright  Copyright(c)2016 Steve Weathers.  All Right Reserved. 
 * 
 * Portions from CodeWarrior Laravel Template -  Copyright (c)2015 Steve Weathers Technology.  Used by permission.  Used by permission.  
 * 
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class StartupMilestone extends Model {

	/**
	 * Database table used by this model
	 * @var  string
	 */
	protected $table = 'xref_startup_milestone';

	/**
	 * primary key of the table
	 * @var  string
	 */
	protected $primaryKey = 'xref_s_m_id';
    
	protected $nameField = '';

	protected $sortField = 'xref_s_m_id';    
	
	/**
	 * Disable Auto Timestamps
	 */
	public $timestamps = false;
		
	/**
	 * Fillable DB Fields
	 * Secure fields like ID, owner, parent should be manually set by the controller
	 */

	protected $fillable = [
   		
		//'xref_s_m_id', //ID
    			
		'startup_id', //XREFID
  			
		'milestone_id', //XREFID
    			
		'date_completed', //DATETIME
    			
		'completed_descr', //TEXTBLOCK
    			
		'is_active', //ISACTIVE
  	];

	/**
	 * Attributes defines the default values to be set in new objects
	 * 
	 */
	
	protected $attributes = array(
 		'is_active' => '1' 
);

	/**
	 * Validation Rules for create
	 * @var  array
	 */
	public static $insertRules = [
		// 'xref_s_m_id'		=> 'required|integer',	//ID
   
		'startup_id'		=> 'required|integer',	
   
		'milestone_id'		=> 'required|integer',	
         	];


	/**
	 * Validation Rules for edit
	 * @var  array
	 */
	public static $updateRules = [
		//'xref_s_m_id'	=> 'required|integer',   //ID
  		//'startup_id'	=> 'required|integer',   //XREFID
 		//'milestone_id'	=> 'required|integer',   //XREFID
    		'completed_descr'		=> 'max:65565',
  		'is_active'		=> 'integer',
  	
	];

		/**
	 * Search for entities where the name field or a searchable text fields matches the search string
	 * prefixing the name with "scope" makes this function available for query chains 
	 *
	 * usage: StartupMilestone::select('xref_s_m_id','xref_s_m_name','sort_order')->startupMilestoneName( $search_query )->get(); 		     
	 */
    public function scopeStartupMilestoneName($query, $search)
    {
        return $query->where('', 'like', '%'.$search.'%')
          		;

    }
    
        
    /**
	 * ->authed()
	 * Filter the query by only the resources the user is authorized to view, edit or delete
	 * prefixing the name with "scope" makes this function available for query chains 
	 * 
	 * usage: $startupMilestone = StartupMilestone::select(..)->authed( $this->user_id, $this->user_is_admin, 'edit')->get(); 		     
	 * 
	 */
	public function scopeAuthed($query, $user_id, $user_is_admin=false, $action='w')
	{
		/** Admin users always have all permissions **/
		
		if ($user_is_admin){ return $query;  }
		switch ($action) {
			/** Execute / Assign / Delete actions are generally restricted to the owner **/
			case 'x':
			case 'execute':
			case 'delete':
			case 'destroy':
			case 'assign':
				return $query
					;
				break;				
		
			/** Write / Edit / Update / Create Children are restricted to owner and managers **/
			case 'w':
			case 'write':
			case 'edit':
			case 'update':
			case 'read-private':
				return $query
					//Table Managers Join
					//->join('xref_employer_manager', 'xref_employer_manager.employer_id', '=', 'employer.id')
					//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					;
				break;
			
			/** Read / View are sometimes open to the world, other times they are restricted to a specific audience.  **/
			case 'r':  
			case 'read': 
			case 'view':
			case 'show':
			case 'view-public':
				return $query
					//Table Subscribers Join
					//->join('xref_employer_subscriber', 'xref_employer_subscriber.employer_id', '=', 'employer.id')
						//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					//Public - no filtering
				; 
				break;
				
		}
		return $query;
	}
    
	 
	    
    /** 
     * FIND ASSOCIATED OTM TABLES
     * Return the specific one-to-many object referenced by this object
     * usage: foreach ($query_results_collection as $row) {... $row->'that_type_name' => @$row->thatType->that_type_name, ... } 
     * 
     */
   
    
  	
	public function startup()
    {
    	return $this->belongsTo('App\Models\Startup', 'startup_id' );
    }
  	
	public function milestone()
    {
    	return $this->belongsTo('App\Models\Milestone', 'milestone_id' );
    }
      
    
    
 	 




	

   
}