<?php
/** 
 * Model for Team Member
 * 
 * @package  entrack
 * @author  Steve Weathers
 * @copyright  Copyright(c)2016 Steve Weathers.  All Right Reserved. 
 * 
 * Portions from CodeWarrior Laravel Template -  Copyright (c)2015 Steve Weathers Technology.  Used by permission.  Used by permission.  
 * 
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model {

	/**
	 * Database table used by this model
	 * @var  string
	 */
	protected $table = 'tbl_team_member';

	/**
	 * primary key of the table
	 * @var  string
	 */
	protected $primaryKey = 'team_member_id';
    
	protected $nameField = 'team_member_name';

	protected $sortField = 'sort_order';    
	
	/**
	 * Disable Auto Timestamps
	 */
	public $timestamps = false;
		
	/**
	 * Fillable DB Fields
	 * Secure fields like ID, owner, parent should be manually set by the controller
	 */

	protected $fillable = [
   		
		//'team_member_id', //ID
    			
		'team_member_name', //NAME
     		
		//'startup_id', //PARENT
    			
		'team_role_id', //TYPETABLE
  			
		'team_relationship_id', //TYPETABLE
    			
		'title', //TEXT_64
    			
		'contact_email', //TEXT_128
    			
		'contact_phone', //TEXT_32
    			
		'hours_per_month', //DECIMAL
    			
		'has_equity', //BOOLEAN
    			
		'sort_order', //SORT
  	];

	/**
	 * Attributes defines the default values to be set in new objects
	 * 
	 */
	
	protected $attributes = array(
 		'sort_order' => '20' 
);

	/**
	 * Validation Rules for create
	 * @var  array
	 */
	public static $insertRules = [
		// 'team_member_id'		=> 'required|integer',	//ID
   
	
		'team_member_name'		=> 'required|max:64',	 
   		// 'startup_id'		=> 'required|integer',	//PARENT
                 	];


	/**
	 * Validation Rules for edit
	 * @var  array
	 */
	public static $updateRules = [
		//'team_member_id'	=> 'required|integer',   //ID
    		//'startup_id'	=> 'required|integer',   //PARENT
  		'team_role_id'		=> 'integer',	
  		'team_relationship_id'		=> 'integer',	
   		'title'		=> 'max:64',
  		'contact_email'		=> 'max:128',
  		'contact_phone'		=> 'max:32',
  		'hours_per_month'		=> 'numeric',	
   		'has_equity'		=> 'integer',
  		'sort_order'		=> 'integer',	
   	
	];

		/**
	 * Search for entities where the name field or a searchable text fields matches the search string
	 * prefixing the name with "scope" makes this function available for query chains 
	 *
	 * usage: TeamMember::select('team_member_id','team_member_name','sort_order')->teamMemberName( $search_query )->get(); 		     
	 */
    public function scopeTeamMemberName($query, $search)
    {
        return $query->where('team_member_name', 'like', '%'.$search.'%')
                    		;

    }
    
        
    /**
	 * ->authed()
	 * Filter the query by only the resources the user is authorized to view, edit or delete
	 * prefixing the name with "scope" makes this function available for query chains 
	 * 
	 * usage: $teamMember = TeamMember::select(..)->authed( $this->user_id, $this->user_is_admin, 'edit')->get(); 		     
	 * 
	 */
	public function scopeAuthed($query, $user_id, $user_is_admin=false, $action='w')
	{
		/** Admin users always have all permissions **/
		
		if ($user_is_admin){ return $query;  }
		switch ($action) {
			/** Execute / Assign / Delete actions are generally restricted to the owner **/
			case 'x':
			case 'execute':
			case 'delete':
			case 'destroy':
			case 'assign':
				return $query
					;
				break;				
		
			/** Write / Edit / Update / Create Children are restricted to owner and managers **/
			case 'w':
			case 'write':
			case 'edit':
			case 'update':
			case 'read-private':
				return $query
					//Table Managers Join
					//->join('xref_employer_manager', 'xref_employer_manager.employer_id', '=', 'employer.id')
					//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					;
				break;
			
			/** Read / View are sometimes open to the world, other times they are restricted to a specific audience.  **/
			case 'r':  
			case 'read': 
			case 'view':
			case 'show':
			case 'view-public':
				return $query
					//Table Subscribers Join
					//->join('xref_employer_subscriber', 'xref_employer_subscriber.employer_id', '=', 'employer.id')
						//Table Managers Where
					//->orWhere('xref_employer_manager.user_id', '=', $user_id)
					//Public - no filtering
				; 
				break;
				
		}
		return $query;
	}
    
	 
		
    /**
	 * Filter the query to show only the children of the designated parent
	 * prefixing the name with "scope" makes this function available for query chains 
	 * usage: $teamMember = TeamMember::select(..)->childrenOf( $parent_id)->get(); 		     
	 */
	public function scopeChildrenOf($parent_id)
	{
		return $query->where($startup_id, $parent_id );
	}
	     
    /** 
     * FIND ASSOCIATED OTM TABLES
     * Return the specific one-to-many object referenced by this object
     * usage: foreach ($query_results_collection as $row) {... $row->'that_type_name' => @$row->thatType->that_type_name, ... } 
     * 
     */
   
    
    
  	
	public function startup()
    {
    	return $this->belongsTo('App\Models\Startup', 'startup_id' );
    }
      
  
	public function teamRole()  // teamRoles
    {
    	return $this->belongsTo('App\Models\TeamRole', 'team_role_id' );
    }
  
	public function teamRelationship()  // teamRelationships
    {
    	return $this->belongsTo('App\Models\TeamRelationship', 'team_relationship_id' );
    }
      
    
    
    
    
    
 	 

	    
    /** 
     * FIND ASSOCIATED MTM TABLES
     * Return a collction of objects that are referenced through an xref table
     * usage:  $widget_list=@$row->widgetAttributes()->select('tbl_attribute.attribute_name')->lists('attribute_name');
     * example assumes this model=Widget and that object = Attribute
     * 
     **/
	 
	 
	 public function projectTeamMembers()
	 {
 	 	 return $this->belongsToMany('App\Models\Project', 'xref_project_team_member', 'team_member_id', 'project_id');
	 }	
 



	 /** 
	  * FIND OTHER REFERENCES 
	  */
 
	 public function projectResponsibleTeamMembers()
	{
	 	return $this->hasMany('App\Models\Project', 'responsible_team_member_id', 'team_member_id');
	 } 
	 
 	

   
}